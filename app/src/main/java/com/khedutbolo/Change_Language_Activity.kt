package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Filter_Adapter
import com.khedutbolo.Adapter.Filter_Adapter1
import com.khedutbolo.Model.Filter_Model
import com.khedutbolo.Model.Filter_Model1
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*

class Change_Language_Activity : AppCompatActivity() {

    private var rb_english : RadioButton? = null
    private var rb_hindi : RadioButton? = null
    private var rb_gujarati : RadioButton? = null
    lateinit var btn_default : Button
    private val sharedPrefFile = "language"
    private var str_language = ""
    private var select_language = ""
    lateinit var hud: KProgressHUD
    private var str_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change__language_)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.changelan)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")

        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        str_language = sharedPreferences.getString("lan_name", "")!!

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        rb_english = findViewById(R.id.r_english) as RadioButton
        rb_hindi = findViewById(R.id.r_hindi) as RadioButton
        rb_gujarati = findViewById(R.id.r_gujarati) as RadioButton
        btn_default = findViewById(R.id.btn_default) as Button

        rb_english!!.typeface = typeface_medium
        rb_hindi!!.typeface = typeface_medium
        rb_gujarati!!.typeface = typeface_medium
        btn_default.typeface = typeface_medium

        if (str_language.equals("hi")){
            rb_hindi!!.isChecked = true

            rb_english!!.setTextColor(Color.BLACK)
            rb_hindi!!.setTextColor(Color.WHITE)
            rb_gujarati!!.setTextColor(Color.BLACK)

            rb_english!!.setBackgroundResource(R.drawable.change_language_back)
            rb_hindi!!.setBackgroundResource(R.drawable.change_language_back_green)
            rb_gujarati!!.setBackgroundResource(R.drawable.change_language_back)

        }else if (str_language.equals("gu")){
            rb_gujarati!!.isChecked = true
            rb_english!!.setTextColor(Color.BLACK)
            rb_hindi!!.setTextColor(Color.BLACK)
            rb_gujarati!!.setTextColor(Color.WHITE)

            rb_english!!.setBackgroundResource(R.drawable.change_language_back)
            rb_hindi!!.setBackgroundResource(R.drawable.change_language_back)
            rb_gujarati!!.setBackgroundResource(R.drawable.change_language_back_green)
        }else if (str_language.equals("en")){
            rb_english!!.isChecked = true
            rb_english!!.setTextColor(Color.WHITE)
            rb_hindi!!.setTextColor(Color.BLACK)
            rb_gujarati!!.setTextColor(Color.BLACK)

            rb_english!!.setBackgroundResource(R.drawable.change_language_back_green)
            rb_hindi!!.setBackgroundResource(R.drawable.change_language_back)
            rb_gujarati!!.setBackgroundResource(R.drawable.change_language_back)
        }else{
            rb_english!!.isChecked = true
            rb_english!!.setTextColor(Color.WHITE)
            rb_hindi!!.setTextColor(Color.BLACK)
            rb_gujarati!!.setTextColor(Color.BLACK)

            rb_english!!.setBackgroundResource(R.drawable.change_language_back_green)
            rb_hindi!!.setBackgroundResource(R.drawable.change_language_back)
            rb_gujarati!!.setBackgroundResource(R.drawable.change_language_back)
        }

        btn_default.setOnClickListener {
            if (select_language.equals("")){

            }else{

                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    hud.show()
                    val json = JSONObject()
                    LanguageChange({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@LanguageChange
                        } else {
                            languagechangeResponse(it)
                        }

                    }).execute("GET", AppConfig.CHANGE_LANGUAGE+str_id+"&lang="+select_language, json.toString())
                }
            }
        }

        val radioGroup = findViewById(R.id.r_group) as RadioGroup
        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                // checkedId is the RadioButton selected
                val radio: RadioButton = findViewById(checkedId)
                if (radio.text.equals("ENGLISH")){
                    rb_english!!.setTextColor(Color.WHITE)
                    rb_hindi!!.setTextColor(Color.BLACK)
                    rb_gujarati!!.setTextColor(Color.BLACK)

                    rb_english!!.setBackgroundResource(R.drawable.change_language_back_green)
                    rb_hindi!!.setBackgroundResource(R.drawable.change_language_back)
                    rb_gujarati!!.setBackgroundResource(R.drawable.change_language_back)

                    select_language = "en";


                }else if (radio.text.equals("हिंदी")){
                    rb_english!!.setTextColor(Color.BLACK)
                    rb_hindi!!.setTextColor(Color.WHITE)
                    rb_gujarati!!.setTextColor(Color.BLACK)

                    rb_english!!.setBackgroundResource(R.drawable.change_language_back)
                    rb_hindi!!.setBackgroundResource(R.drawable.change_language_back_green)
                    rb_gujarati!!.setBackgroundResource(R.drawable.change_language_back)

                    select_language = "hi";


                }else if (radio.text.equals("ગુજરાતી")){
                    rb_english!!.setTextColor(Color.BLACK)
                    rb_hindi!!.setTextColor(Color.BLACK)
                    rb_gujarati!!.setTextColor(Color.WHITE)

                    rb_english!!.setBackgroundResource(R.drawable.change_language_back)
                    rb_hindi!!.setBackgroundResource(R.drawable.change_language_back)
                    rb_gujarati!!.setBackgroundResource(R.drawable.change_language_back_green)

                    select_language = "gu";

                }
            }
        })
    }

    private fun setLocale(lang: String) {
        val locale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = locale
        res.updateConfiguration(conf, dm)
        val refresh = Intent(this, DashBoardActivity::class.java)
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        this.overridePendingTransition(0, 0)
        startActivity(refresh)

    }

    override fun onSupportNavigateUp(): Boolean {
        val refresh = Intent(this, DashBoardActivity::class.java)
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        this.overridePendingTransition(0, 0)
        startActivity(refresh)
        return true
    }

    override fun onBackPressed() {
        val refresh = Intent(this, DashBoardActivity::class.java)
        refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        this.overridePendingTransition(0, 0)
        startActivity(refresh)
        super.onBackPressed()
    }

    class LanguageChange(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun languagechangeResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (status.equals("200")){
            val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor =  sharedPreferences.edit()
            editor.putString("lan_name",select_language)
            editor.apply()
            setLocale(select_language)

        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}

