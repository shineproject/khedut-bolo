package com.khedutbolo

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.*
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.AsyncTask
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.widget.*
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import javax.net.ssl.HttpsURLConnection
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.kaopiz.kprogresshud.KProgressHUD
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.view.KeyEvent
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import java.lang.ref.WeakReference

class OtpActivity : AppCompatActivity() {

    var otp = ""
    lateinit var response: String
    lateinit var ed_phone_number: EditText
    lateinit var ed_phone_code: EditText
    lateinit var btn_get_otp: Button
    lateinit var txt_title_mobile: TextView
    lateinit var hud: KProgressHUD
    private var register_check = ""
    private var str_id = ""
    private var str_lan = ""

    companion object {
        var token: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        val policy = StrictMode.ThreadPolicy.Builder()
            .permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.mobileverification)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_semibold = Typeface.createFromAsset(assets, "poppinssemiBold.otf")

        val i = intent
        str_lan = i.getStringExtra("language")

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(ContentValues.TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                token = task.result?.token.toString()

                val msg = getString(R.string.msg_token_fmt, token)
                Log.d(ContentValues.TAG, msg)
            })

        ed_phone_number = findViewById(R.id.ed_number) as EditText
        ed_phone_code = findViewById(R.id.ed_code) as EditText
        btn_get_otp = findViewById(R.id.btn_get) as Button
        txt_title_mobile = findViewById(R.id.txt_title_number) as TextView

        ed_phone_number.setFocusableInTouchMode(true);
        ed_phone_number.setFocusable(true);
        ed_phone_number.requestFocus();

        ed_phone_number.showKeyboard()

        ed_phone_code.isLongClickable = false

        txt_title_mobile.typeface = typeface_semibold
        ed_phone_number.typeface = typeface_medium
        ed_phone_code.typeface = typeface_medium
        btn_get_otp.typeface = typeface_medium

        btn_get_otp.setOnClickListener {
            if (ed_phone_number.text.toString().trim().equals("")) {
                Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
            } else if (ed_phone_number.text.toString().trim().length < 10) {
                Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
            } else {
                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()

                } else {
                    hud.show()
                    val json = JSONObject()
                    json.put("mobile", ed_phone_number.text.toString().trim())
                    HttpTask({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@HttpTask
                        } else {
                            otpResponse(it)
                        }

                    }).execute("POST", AppConfig.SENDOTP, json.toString())
                }

            }
        }

        ed_phone_number.setOnEditorActionListener() { v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_DONE){
                hideKeyboard()
                if (ed_phone_number.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
                } else if (ed_phone_number.text.toString().trim().length < 10) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
                } else {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()

                    } else {
                        hud.show()
                        val json = JSONObject()
                        json.put("mobile", ed_phone_number.text.toString().trim())
                        HttpTask({
                            hud.dismiss();

                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                ).show()
                                return@HttpTask
                            } else {
                                otpResponse(it)
                            }

                        }).execute("POST", AppConfig.SENDOTP, json.toString())
                    }

                }
                true
            } else {
                false
            }
        }
    }

    class HttpTask(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    //post Api
    fun sendPostRequest(mobile: String) {
        hud.show()
        var reqParam = URLEncoder.encode("mobile", "UTF-8") + "=" + URLEncoder.encode(mobile, "UTF-8")
        val mURL = URL(AppConfig.SENDOTP)

        with(mURL.openConnection() as HttpURLConnection) {
            // optional default is GET
            requestMethod = "POST"

            val wr = OutputStreamWriter(getOutputStream());
            wr.write(reqParam);
            wr.flush();

            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()

                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
                it.close()
                hud.dismiss()
                otpResponse(response.toString())
            }
        }
    }

    fun otpResponse(response: String?) {
        ed_phone_number.clearFocus();

        hideKeyboard()

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")){
            if (data.length > 0) {
                val jsonData = JSONObject(data)
                val is_register = jsonData.getString("is_register")
                val mobile = jsonData.getString("mobile")
                otp = jsonData.getString("otp")

                register_check = is_register
                if (register_check.equals("true")) {
                    str_id = jsonData.getString("id")
                    val name = jsonData.getString("name")
                    val address = jsonData.getString("address")
                    val language = jsonData.getString("language")
                    val image = jsonData.getString("image")
                    val state_id = jsonData.getString("state_id")
                    val city_id = jsonData.getString("city_id")
                    val tehsil_id = jsonData.getString("tehsil_id")
                    val village_id = jsonData.getString("village_id")
                    val state = jsonData.getString("state")
                    val city = jsonData.getString("city")
                    val tehsil = jsonData.getString("tehsil")
                    val village = jsonData.getString("village")

                    val sharedProfile = this.getSharedPreferences("profile", 0)
                    val editor: SharedPreferences.Editor = sharedProfile.edit()
                    editor.putString("id", str_id)
                    editor.putString("name", name)
                    editor.putString("mobile", mobile)
                    editor.putString("address", address)
                    editor.putString("language", language)
                    editor.putString("image", image)
                    editor.putString("sid", state_id)
                    editor.putString("cid", city_id)
                    editor.putString("tid", tehsil_id)
                    editor.putString("vid", village_id)
                    editor.putString("state_name", state)
                    editor.putString("city_name", city)
                    editor.putString("tehsil_name", tehsil)
                    editor.putString("village_name", village)
                    editor.apply()

                    val sharedPreferences: SharedPreferences = this.getSharedPreferences("language", Context.MODE_PRIVATE)
                    val editor1: SharedPreferences.Editor =  sharedPreferences.edit()
                    editor1.putString("lan_name",language)
                    editor1.apply()

                    hideKeyboard()
                    val intent = Intent(applicationContext,Check_OTP_Activity::class.java)
                    intent.putExtra("otp",otp)
                    intent.putExtra("mobile",mobile)
                    intent.putExtra("register_check",register_check)
                    intent.putExtra("str_id",str_id)
                    intent.putExtra("language",str_lan)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    overridePendingTransition(0,0)
                    finish()
                }else{
                    hideKeyboard()
                    val intent = Intent(applicationContext,Check_OTP_Activity::class.java)
                    intent.putExtra("otp",otp)
                    intent.putExtra("mobile",mobile)
                    intent.putExtra("register_check",register_check)
                    intent.putExtra("str_id",str_id)
                    intent.putExtra("language",str_lan)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    overridePendingTransition(0,0)
                    finish()
                }

            } else {
                Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
        }

    }

    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    override fun onSupportNavigateUp(): Boolean {
        hideKeyboard()
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.requestFocus()
        imm.showSoftInput(this, 0)
    }

}


