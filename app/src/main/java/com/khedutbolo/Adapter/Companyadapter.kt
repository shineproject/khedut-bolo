package com.khedutbolo.Adapter

import com.khedutbolo.Model.CompanyModel
import com.khedutbolo.Model.ProductModel
import com.khedutbolo.Model.TrandingModel
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.khedutbolo.OfferActivity
import com.khedutbolo.OtpActivity
import com.khedutbolo.R
import kotlinx.android.synthetic.main.first_recycler.view.*
import androidx.core.content.ContextCompat.startActivity
import androidx.core.content.ContextCompat.startActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.util.AppConfig

class Companyadapter(val userList: ArrayList<CompanyModel>) : RecyclerView.Adapter<Companyadapter.ViewHolder>() {

    private val context: Context? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.company_recycler_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: CompanyModel) {
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            val textViewName = itemView.findViewById(R.id.textview) as TextView
            val image_crop = itemView.findViewById(R.id.imageview) as ImageView
            textViewName.text = user.name

            textViewName.typeface = typeface_medium
            Glide.with(itemView.context)
                .load(user.image)
                .error(R.mipmap.companyicon)
                .into(image_crop)

        }
    }
}