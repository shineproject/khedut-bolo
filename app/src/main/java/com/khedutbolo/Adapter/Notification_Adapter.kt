package com.khedutbolo.Adapter

import com.khedutbolo.Model.Cancel_Order_Model
import com.khedutbolo.Model.Current_Order_Model
import com.khedutbolo.Model.Notification_Model
import com.khedutbolo.Model.ProductModel
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.khedutbolo.R
import kotlinx.android.synthetic.main.first_recycler.view.*
import java.text.SimpleDateFormat

class Notification_Adapter(val userList: ArrayList<Notification_Model>) : RecyclerView.Adapter<Notification_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.notification_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: Notification_Model) {
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            val textViewName = itemView.findViewById(R.id.txt_message) as TextView
            val textViewDate = itemView.findViewById(R.id.txt_date) as TextView
            val textViewTime = itemView.findViewById(R.id.txt_time) as TextView

            textViewName.text = user.noti_detail

            val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val outputFormat = SimpleDateFormat("dd-MM-yyyy")
            val inputDateStr = user.date
            val date = inputFormat.parse(inputDateStr)
            val outputDateStr = outputFormat.format(date)
            textViewDate.text = outputDateStr

            val inputFormat1 = SimpleDateFormat("hh:mm:ss")
            val outputFormat1 = SimpleDateFormat("hh:mm")
            val inputDateStr1 = user.time
            val time = inputFormat1.parse(inputDateStr1)
            val outputDateStr1 = outputFormat1.format(time)
            textViewTime.text = outputDateStr1

            textViewName.typeface = typeface_medium
            textViewDate.typeface = typeface_medium
            textViewTime.typeface = typeface_medium

        }
    }
}