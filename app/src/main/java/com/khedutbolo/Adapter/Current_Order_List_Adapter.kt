package com.khedutbolo.Adapter

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.khedutbolo.Current_order_detail
import com.khedutbolo.Model.Current_Order_List_Model
import com.khedutbolo.Model.Current_Order_Model
import com.khedutbolo.R
import com.khedutbolo.Shopping_Cart_Activity
import com.khedutbolo.Trending_Product_Activity
import java.text.SimpleDateFormat

class Current_Order_List_Adapter(internal var context: Context, val userList: ArrayList<Current_Order_List_Model>) : RecyclerView.Adapter<Current_Order_List_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.current_order_id, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: Current_Order_List_Model) {
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            val textViewOrderid = itemView.findViewById(R.id.txt_orderid) as TextView
            val textViewDatetime = itemView.findViewById(R.id.txt_datetime) as TextView
            val textViewDatet = itemView.findViewById(R.id.txt_datet) as TextView
            val textViewTotal = itemView.findViewById(R.id.txt_total) as TextView
            val textViewTotalprice = itemView.findViewById(R.id.txt_totalprice) as TextView
            val btn_view = itemView.findViewById(R.id.btn_view) as Button

            textViewOrderid.text = itemView.context.resources.getString(R.string.orderid)+user.id
            textViewTotalprice.text = "₹ "+user.total

            val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val outputFormat = SimpleDateFormat("dd-MM-yyyy hh:mm")
            if (user.createdat.equals("null")){

            }else{
                val inputDateStr = user.createdat
                val date = inputFormat.parse(inputDateStr)
                val outputDateStr = outputFormat.format(date)
                textViewDatet.text = outputDateStr
            }

            textViewOrderid.typeface = typeface_medium
            textViewDatetime.typeface = typeface_medium
            textViewDatet.typeface = typeface_medium
            textViewTotal.typeface = typeface_medium
            textViewTotalprice.typeface = typeface_medium
            btn_view.typeface = typeface_medium

            btn_view.setOnClickListener{
                val intent = Intent(itemView.context, Current_order_detail::class.java)
                intent.putExtra("order_id",user.id)
                itemView.context.startActivity(intent)
            }
        }
    }
}