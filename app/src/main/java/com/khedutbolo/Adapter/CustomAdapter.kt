package com.khedutbolo.Adapter

import android.content.Context
import com.khedutbolo.Model.ProductModel
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.DashBoardActivity
import com.khedutbolo.R
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.first_recycler.view.*
import java.util.*
import kotlin.collections.ArrayList

class CustomAdapter(ctx: Context, val userList: ArrayList<ProductModel>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    private val inflater: LayoutInflater
    private val arraylist: ArrayList<ProductModel>

    init {

        inflater = LayoutInflater.from(ctx)
        this.arraylist = ArrayList<ProductModel>()
        this.arraylist.addAll(DashBoardActivity.productdata)
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.first_recycler, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: ProductModel) {
            val textViewName = itemView.findViewById(R.id.textview) as TextView
            val image_crop = itemView.findViewById(R.id.imageview) as ImageView
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            textViewName.typeface = typeface_medium
            textViewName.text = user.name
            Glide.with(itemView.context)
                .load(user.image)
                .error(R.mipmap.test_product)
                .into(image_crop)

        }
    }

    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())

//        if (DashBoardActivity.productdata.isEmpty()){
//        }else{
            DashBoardActivity.productdata.clear()
            if (charText.length == 0) {
                DashBoardActivity.productdata.addAll(arraylist)
            } else {
                for (wp in arraylist) {
                    if (wp.name.toLowerCase(Locale.getDefault()).contains(charText)) {
                        DashBoardActivity.productdata.add(wp)
                    }else{

                    }
                }
            }
            notifyDataSetChanged()
//        }

    }
}