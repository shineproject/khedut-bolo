package com.khedutbolo.Adapter

import android.graphics.Paint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.Model.Company_Detail_Model
import com.khedutbolo.R
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.first_recycler.view.*

class Company_Detail_Adapter(val userList: ArrayList<Company_Detail_Model>) : RecyclerView.Adapter<Company_Detail_Adapter.ViewHolder>() {

    var company_category: String = ""

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.company_detail_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val typeface_medium = Typeface.createFromAsset(holder.itemView.context.getAssets(), "poppinsmedium.otf")
        val typeface_regular = Typeface.createFromAsset(holder.itemView.context.getAssets(), "poppinsregular.otf")

        if (userList[position].s.equals("1")) {
            val layoutParams = holder.itemView.getLayoutParams() as StaggeredGridLayoutManager.LayoutParams
            layoutParams.isFullSpan = true
            holder.l_image.visibility = View.GONE
            if (position == 0){
                holder.view_line.visibility = View.GONE
            }else{
                holder.view_line.visibility = View.VISIBLE
            }
            holder.textViewHeader.visibility = View.VISIBLE
        }else{
            val layoutParams = holder.itemView.getLayoutParams() as StaggeredGridLayoutManager.LayoutParams
            layoutParams.isFullSpan = false
            holder.l_image.visibility = View.VISIBLE
            holder.view_line.visibility = View.GONE
            holder.textViewHeader.visibility = View.GONE
        }

        holder.textViewHeader.text = userList[position].category_name
        holder.textViewHeader.typeface = typeface_medium

        holder.textViewName.text = userList[position].name
        holder.textViewCompany.text = holder.itemView.context.resources.getString(R.string.by)+userList[position].company_name
        holder.textViewPrice.text = "₹ "+userList[position].sale_price
        holder.textViewActualPrice.text = userList[position].price + holder.itemView.context.resources.getString(R.string.perset)
        holder.textViewActualPrice.paintFlags = holder.textViewActualPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        Glide.with(holder.itemView.context)
            .load(userList[position].cover)
            .error(R.mipmap.test_product)
            .into(holder.image)

        holder.textViewName.typeface = typeface_medium
        holder.textViewPrice.typeface = typeface_medium
        holder.textViewActualPrice.typeface = typeface_medium
        holder.textViewCompany.typeface = typeface_regular

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName = itemView.findViewById(R.id.txt_about) as TextView
        val textViewHeader = itemView.findViewById(R.id.txt_titleheader) as TextView
        val textViewPrice = itemView.findViewById(R.id.txt_price) as TextView
        val textViewActualPrice = itemView.findViewById(R.id.txt_actualprice) as TextView
        val textViewCompany = itemView.findViewById(R.id.txt_company_name) as TextView
        val l_image = itemView.findViewById(R.id.l_image) as LinearLayout
        val view_line = itemView.findViewById(R.id.view_line) as View
        val image = itemView.findViewById(R.id.imageview) as ImageView
    }
}