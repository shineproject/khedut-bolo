package com.khedutbolo.Adapter

import android.graphics.Paint
import com.khedutbolo.Model.Cancel_Order_Model
import com.khedutbolo.Model.Current_Order_Model
import com.khedutbolo.Model.ProductModel
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.R
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.first_recycler.view.*
import java.text.SimpleDateFormat

class Cancel_Order_Adapter(val userList: ArrayList<Cancel_Order_Model>) : RecyclerView.Adapter<Cancel_Order_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.cancel_order_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: Cancel_Order_Model) {
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            val textViewName = itemView.findViewById(R.id.Product_name) as TextView
            val textViewPrice = itemView.findViewById(R.id.txt_price) as TextView
            val textViewqty = itemView.findViewById(R.id.txt_qty) as TextView
            val textViewSpent = itemView.findViewById(R.id.txt_spent) as TextView
            val textViewDate = itemView.findViewById(R.id.txt_date) as TextView
            val image_current = itemView.findViewById(R.id.set_image) as ImageView

            textViewName.text = user.name
            textViewPrice.text = user.total
            textViewqty.text = itemView.context.resources.getString(R.string.qtyy)+user.qty
            textViewSpent.text = user.price+ itemView.context.resources.getString(R.string.perset)
            textViewSpent.paintFlags = textViewSpent.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val outputFormat = SimpleDateFormat("dd-MM-yyyy")
            if (user.date.equals("null")){

            }else{
                val inputDateStr = user.date
                val date = inputFormat.parse(inputDateStr)
                val outputDateStr = outputFormat.format(date)
                textViewDate.text = itemView.context.resources.getString(R.string.dateoforder)+outputDateStr
            }


            Glide.with(itemView.context)
                .load(user.image)
                .error(R.mipmap.test_product)
                .into(image_current)

            textViewName.typeface = typeface_medium
            textViewPrice.typeface = typeface_medium
            textViewqty.typeface = typeface_medium
            textViewSpent.typeface = typeface_medium
            textViewDate.typeface = typeface_medium
        }
    }
}