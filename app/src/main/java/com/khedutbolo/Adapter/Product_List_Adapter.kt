package com.khedutbolo.Adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.khedutbolo.DashBoardActivity
import com.khedutbolo.Model.ProductModel
import com.khedutbolo.Model.Product_Model_Search
import com.khedutbolo.R
import com.khedutbolo.Searchactivity
import java.util.*
import kotlin.collections.ArrayList

class Product_List_Adapter(ctx: Context, val userList: ArrayList<Product_Model_Search>) : RecyclerView.Adapter<Product_List_Adapter.ViewHolder>() {

    private val inflater: LayoutInflater
    private val arraylist: ArrayList<Product_Model_Search>

    init {

        inflater = LayoutInflater.from(ctx)
        this.arraylist = ArrayList<Product_Model_Search>()
        this.arraylist.addAll(Searchactivity.product_list)
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.first_recycler, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: Product_Model_Search) {
            val textViewName = itemView.findViewById(R.id.textview) as TextView
            val image_crop = itemView.findViewById(R.id.imageview) as ImageView
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            textViewName.typeface = typeface_medium
            textViewName.text = user.name
            Glide.with(itemView.context)
                .load(user.image)
                .error(R.mipmap.test_product)
                .into(image_crop)

        }
    }

    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())

//        if (DashBoardActivity.productdata.isEmpty()){
//        }else{
        Searchactivity.product_list.clear()
        if (charText.length == 0) {
            Searchactivity.product_list.addAll(arraylist)
        } else {
            for (wp in arraylist) {
                if (wp.name.toLowerCase(Locale.getDefault()).contains(charText)) {
                    Searchactivity.product_list.add(wp)
                }else{

                }
            }
        }
        notifyDataSetChanged()
//        }

    }
}