package com.khedutbolo.Adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.khedutbolo.Model.About_Product_Model
import com.khedutbolo.R
import kotlinx.android.synthetic.main.first_recycler.view.*

class About_Product_Adapter(val userList: ArrayList<About_Product_Model>) : RecyclerView.Adapter<About_Product_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.about_product_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: About_Product_Model) {
            val textViewName = itemView.findViewById(R.id.txt_about) as TextView
            val textViewValue = itemView.findViewById(R.id.txt_value) as TextView
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            textViewName.typeface = typeface_medium
            textViewValue.typeface = typeface_medium
            textViewName.text = user.key
            textViewValue.text = user.value

        }
    }
}