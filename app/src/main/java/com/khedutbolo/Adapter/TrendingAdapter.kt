package com.khedutbolo.Adapter

import android.graphics.Paint
import com.khedutbolo.Model.ProductModel
import com.khedutbolo.Model.TrandingModel
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.R
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.first_recycler.view.*

class TrendingAdapter(val userList: ArrayList<TrandingModel>) : RecyclerView.Adapter<TrendingAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.trending_product_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: TrandingModel) {
            val textViewName = itemView.findViewById(R.id.txt_about) as TextView
            val textViewPrice = itemView.findViewById(R.id.txt_price) as TextView
            val textViewDealer = itemView.findViewById(R.id.txt_dealer) as TextView
            val textViewActualPrice = itemView.findViewById(R.id.txt_actualprice) as TextView
            val image_crop = itemView.findViewById(R.id.imageview) as ImageView

            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")

            textViewName.typeface = typeface_medium
            textViewPrice.typeface = typeface_medium
            textViewDealer.typeface = typeface_medium
            textViewActualPrice.typeface = typeface_medium

            textViewActualPrice.paintFlags = textViewActualPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            textViewName.text = user.name
            textViewPrice.text = "₹ "+user.sale_price
            textViewDealer.text = itemView.context.resources.getString(R.string.by)+user.company_name
            textViewActualPrice.text = user.price + itemView.context.resources.getString(R.string.perset)
            Glide.with(itemView.context)
                .load(user.image)
                .apply(
                    RequestOptions()
                        .placeholder(R.mipmap.test_product)
                )
                .into(image_crop)

        }
    }
}