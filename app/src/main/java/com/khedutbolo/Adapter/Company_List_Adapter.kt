package com.khedutbolo.Adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.Model.Company_List_Model
import com.khedutbolo.Model.ProductModel
import com.khedutbolo.R
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.first_recycler.view.*

class Company_List_Adapter(val userList: ArrayList<Company_List_Model>) : RecyclerView.Adapter<Company_List_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.company_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(user: Company_List_Model) {
            val textViewName = itemView.findViewById(R.id.txt_about) as TextView
            val image_crop = itemView.findViewById(R.id.imageview) as ImageView
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            textViewName.typeface = typeface_medium
            textViewName.text = user.name
            Glide.with(itemView.context)
                .load(user.image)
                .error(R.mipmap.test_product)
                .into(image_crop)

        }
    }
}