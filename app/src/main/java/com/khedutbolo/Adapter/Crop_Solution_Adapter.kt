package com.khedutbolo.Adapter

import com.khedutbolo.Model.Crop_Model
import com.khedutbolo.Model.Crop_Solution_Model
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.khedutbolo.R
import kotlinx.android.synthetic.main.first_recycler.view.*

class Crop_Solution_Adapter(val userList: ArrayList<Crop_Solution_Model>) : RecyclerView.Adapter<Crop_Solution_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.crop_solution_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: Crop_Solution_Model) {
            val textViewName = itemView.findViewById(R.id.textview) as TextView
            val typeface_medium = Typeface.createFromAsset(itemView.context.getAssets(), "poppinsmedium.otf")
            textViewName.typeface = typeface_medium
            textViewName.text = user.name
            itemView.imageview.setImageResource(user.image)

        }
    }
}