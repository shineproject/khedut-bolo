package com.khedutbolo.Adapter

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.khedutbolo.Model.Filter_Model
import com.khedutbolo.Model.Filter_Model1
import com.khedutbolo.R
import com.khedutbolo.Select_Solution_Activity

class Filter_Adapter1(val userList: ArrayList<Filter_Model1>) : RecyclerView.Adapter<Filter_Adapter1.ViewHolder>() {

    var item = 0
    var company = 0

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.filter_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val typeface_medium = Typeface.createFromAsset(holder.itemView.context.getAssets(), "poppinsmedium.otf")
        val typeface_regular = Typeface.createFromAsset(holder.itemView.context.getAssets(), "poppinsregular.otf")
        holder.textViewName.text = userList[position].name
        holder.textViewName.typeface = typeface_regular
        holder.textViewName.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                Select_Solution_Activity.filter_technical.add(userList[position].id)
            }
            else {
                Select_Solution_Activity.filter_technical.remove(userList[position].id)
            }
        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName = itemView.findViewById(R.id.check_txtname) as CheckBox
        val textViewheader = itemView.findViewById(R.id.txt_headername) as TextView
        val l_header = itemView.findViewById(R.id.l_header) as LinearLayout

    }
}