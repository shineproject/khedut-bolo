package com.khedutbolo.Adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.khedutbolo.Model.Wallet_Model
import com.khedutbolo.R
import com.khedutbolo.WalletActivity
import java.text.SimpleDateFormat

class Wallet_Adapter(val userList: ArrayList<Wallet_Model>) : RecyclerView.Adapter<Wallet_Adapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Wallet_Adapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.wallet_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: Wallet_Adapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(user: Wallet_Model) {
            val textViewName = itemView.findViewById(R.id.txt_add_reason) as TextView
            val textViewpoint = itemView.findViewById(R.id.txt_point) as TextView
            val textViewdate = itemView.findViewById(R.id.txt_date) as TextView


            if (user.creditdebit.equals("credit")){
                textViewName.text = itemView.resources.getString(R.string.addwallet)
            }else{
                textViewName.text = itemView.resources.getString(R.string.paidwallet)
                textViewpoint.setTextColor(Color.RED)
            }
            textViewpoint.text = user.points

            val inputFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val outputFormat = SimpleDateFormat("dd-MM-yyyy hh:mm")
            if (user.created_at.equals("null")){

            }else{
                val inputDateStr = user.created_at
                val date = inputFormat.parse(inputDateStr)
                val outputDateStr = outputFormat.format(date)
                textViewdate.text = outputDateStr
            }

        }
    }
}