package com.khedutbolo.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import com.khedutbolo.Model.City_Model
import com.khedutbolo.Model.Product_Size_Model
import com.khedutbolo.R
import kotlinx.android.synthetic.main.spinner_item.view.*

class Product_Size_Adapter(ctx: Context,
                           moods: List<Product_Size_Model>) :
    ArrayAdapter<Product_Size_Model>(ctx, 0, moods) {
    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }
    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }
    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val mood = getItem(position)
        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.spinner_item,
            parent,
            false
        )
        if (mood != null) {
            view.text_spinner.text = mood.value + " " + mood.key
        }
        return view
    }
}