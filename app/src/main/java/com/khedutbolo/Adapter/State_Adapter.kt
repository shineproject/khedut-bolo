package com.khedutbolo.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.khedutbolo.Model.State_Model
import com.khedutbolo.R
import kotlinx.android.synthetic.main.spinner_item.view.*

class State_Adapter(ctx: Context,
                    moods: List<State_Model>) :
    ArrayAdapter<State_Model>(ctx, 0, moods) {
    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }
    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }
    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val mood = getItem(position)
        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.spinner_state,
            parent,
            false
        )
        if (mood != null) {
            view.text_spinner.text = mood.state
        }
        return view
    }
}