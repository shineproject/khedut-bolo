package com.khedutbolo.Adapter

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.Model.Solution_Model
import com.khedutbolo.Model.Sub_Technical_Model
import com.khedutbolo.R
import com.khedutbolo.Select_Solution_Activity
import com.khedutbolo.util.AppConfig
import java.util.*
import kotlin.collections.ArrayList

class Sub_Technical_Adapter(ctx: Context, val userList: ArrayList<Sub_Technical_Model>) : RecyclerView.Adapter<Sub_Technical_Adapter.ViewHolder>() {

    private val inflater: LayoutInflater
    private val arraylist: ArrayList<Sub_Technical_Model>

    init {

        inflater = LayoutInflater.from(ctx)
        this.arraylist = ArrayList<Sub_Technical_Model>()
//        this.arraylist.addAll(Select_Solution_Activity.solution_product)
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.sub_technical, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val typeface_medium = Typeface.createFromAsset(holder.itemView.context.getAssets(), "poppinsmedium.otf")
        val typeface_regular = Typeface.createFromAsset(holder.itemView.context.getAssets(), "poppinsregular.otf")

        if (userList[position].s.equals("1")) {
            val layoutParams = holder.itemView.getLayoutParams() as StaggeredGridLayoutManager.LayoutParams
            layoutParams.isFullSpan = true
            holder.l_image.visibility = View.GONE
            if (position == 0){
                holder.view_line.visibility = View.GONE
            }else{
                holder.view_line.visibility = View.VISIBLE
            }
            holder.textViewHeader.visibility = View.VISIBLE
        }else{
            val layoutParams = holder.itemView.getLayoutParams() as StaggeredGridLayoutManager.LayoutParams
            layoutParams.isFullSpan = false
            holder.l_image.visibility = View.VISIBLE
            holder.view_line.visibility = View.GONE
            holder.textViewHeader.visibility = View.GONE
        }

        holder.textViewHeader.text = userList[position].technical_name
        holder.textViewHeader.typeface = typeface_medium
        holder.textViewName.text = userList[position].name
        holder.textViewPrice.text = "₹ "+userList[position].sale_price
        holder.textViewActualPrice.text = "₹ "+userList[position].price+ holder.itemView.context.resources.getString(R.string.perset)
        holder.textViewSeller.text = holder.itemView.context.resources.getString(R.string.by)+userList[position].dealer_name
        Glide.with(holder.itemView.context)
            .load(userList[position].cover)
            .error(R.mipmap.test_product)
            .into(holder.solution_image)

        holder.textViewName.typeface = typeface_medium
        holder.textViewPrice.typeface = typeface_medium
        holder.textViewActualPrice.typeface = typeface_medium
        holder.textViewSeller.typeface = typeface_regular

        holder.textViewActualPrice.paintFlags = holder.textViewActualPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName = itemView.findViewById(R.id.txt_about) as TextView
        val textViewHeader = itemView.findViewById(R.id.txt_titleheader) as TextView
        val textViewPrice = itemView.findViewById(R.id.txt_price) as TextView
        val textViewSeller = itemView.findViewById(R.id.txt_seller_name) as TextView
        val l_image = itemView.findViewById(R.id.l_image) as LinearLayout
        val view_line = itemView.findViewById(R.id.view_line) as View
        val textViewActualPrice = itemView.findViewById(R.id.txt_actualprice) as TextView
        val solution_image = itemView.findViewById(R.id.imageview) as ImageView

    }

//    fun filter(charText: String) {
//        var charText = charText
//        charText = charText.toLowerCase(Locale.getDefault())
//        Select_Solution_Activity.solution_product.clear()
//        if (charText.length == 0) {
//            Select_Solution_Activity.solution_product.addAll(arraylist)
//        } else {
//            for (wp in arraylist) {
//                if (wp.name.toLowerCase(Locale.getDefault()).contains(charText)) {
//                    Select_Solution_Activity.solution_product.add(wp)
//                }
//            }
//        }
//        notifyDataSetChanged()
//    }
}