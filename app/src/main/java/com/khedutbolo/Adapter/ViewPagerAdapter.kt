package com.khedutbolo.Adapter

import com.khedutbolo.Model.SliderItem
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.khedutbolo.OfferActivity
import com.khedutbolo.R
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.first_recycler.view.*

class ViewPagerAdapter(internal var context: Context, internal var itemList: List<SliderItem>) : PagerAdapter() {
    internal var mLayoutInflater: LayoutInflater
    private var pos = 0

    init {
        mLayoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getCount(): Int {
        return itemList.size
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val holder = ViewHolder()
        val itemView = mLayoutInflater.inflate(R.layout.viewpager_activity, container, false)
        holder.itemImage = itemView.findViewById(R.id.imageview) as ImageView

//        if (pos > this.itemList.size - 1)
//            pos = 0

        holder.sliderItem = this.itemList[pos]
        Glide.with(itemView.context)
            .load(itemList[position].image)
            .error(R.mipmap.test_product)
            .into(holder.itemImage)

        container.addView(itemView,0)

        holder.itemImage.scaleType = ImageView.ScaleType.FIT_XY

        holder.itemImage.setOnClickListener{
            var gotoLevel= Intent(context,OfferActivity::class.java)
            gotoLevel.putExtra("offer_id", itemList[position].id)
            context.startActivity(gotoLevel)
        }

//        pos++
        return itemView
    }

    internal class ViewHolder {
        lateinit var sliderItem: SliderItem
        lateinit var itemImage: ImageView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as CardView)
    }

    override fun isViewFromObject(arg0: View, arg1: Any): Boolean {
        return arg0 === arg1 as View
    }

    override fun destroyItem(arg0: View, arg1: Int, arg2: Any) {
        (arg0 as ViewPager).removeView(arg2 as View)
    }
}