package com.khedutbolo

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Build
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import java.util.*


class MainActivity : Activity() {

    private val mHandler = Handler();
    private val sharedPrefFile = "language"
    private var str_language = ""
    private var str_id = ""
    private var check_otp = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        str_language = sharedPreferences.getString("lan_name", "")!!

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!
        check_otp = sharedProfile.getString("check_otp", "")!!

        val r = Runnable {

            if (str_language.equals("hi")){
                setLocale("hi")
            }else if (str_language.equals("gu")){
                setLocale("gu")
            }else if (str_language.equals("en")){
                setLocale("en")
            }else{
                setLocale("en")
            }
        }


        val h = Handler()
        h.postDelayed(r, 2500) // will be delayed for 1.5 seconds
    }

    private fun setLocale(lang: String) {
        val locale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = locale
        res.updateConfiguration(conf, dm)
        if (check_otp.equals("1")){
            startActivity(Intent(this@MainActivity, DashBoardActivity::class.java))
            overridePendingTransition(0,0)
            finish()
        }else{
                startActivity(Intent(this@MainActivity, Select_Language_Activity::class.java))
                overridePendingTransition(0,0)
                finish()
        }
    }

}
