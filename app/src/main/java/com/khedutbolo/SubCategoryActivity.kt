package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.*
import com.khedutbolo.Model.*
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class SubCategoryActivity : AppCompatActivity() {

    lateinit var rv_sub_category : RecyclerView
    lateinit var rv_sub_technical : RecyclerView
    lateinit var category_id: String
    var category_name: String = ""
    lateinit var txt_nodata : LinearLayout
    val subcategory_list = ArrayList<Subcategory_Model>()
    internal var adapter_subcategory: CustomAdapter1? = null
    var solution_check: String = ""
    internal var adapter_sub_technical: Sub_Technical_Adapter? = null
    val sub_tech_product = ArrayList<Sub_Technical_Model>()
    lateinit var hud: KProgressHUD
    private var str_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_category)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val i = intent
        category_id = i.getStringExtra("category_id")
        category_name = i.getStringExtra("category_name")

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        if (category_name.equals("")){
            val actionbar = supportActionBar
            actionbar!!.title = resources.getString(R.string.selectedcategory)
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }else{
            val actionbar = supportActionBar
            actionbar!!.title = category_name
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }

        rv_sub_category = findViewById(R.id.rv_sub_category) as RecyclerView
        rv_sub_technical = findViewById(R.id.rv_sub_technical) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_sub_category.layoutManager = GridLayoutManager(this, 2)
        rv_sub_technical.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            subcategory({
                hud.dismiss();
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@subcategory
                } else {
                    subcategoryResponse(it)
                }

            }).execute("GET", AppConfig.SUB_CATEGORY+category_id+"&user_id="+str_id, json.toString())
        }

        rv_sub_category.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_sub_category,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext,Category_Activity::class.java)
                        intent.putExtra("category_id", subcategory_list.get(position).id)
                        intent.putExtra("category_name", subcategory_list.get(position).name)
                        overridePendingTransition(0,0)
                        startActivity(intent)
                    }
                })
        )

        rv_sub_technical.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_sub_category,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(applicationContext,AddtocartActivity::class.java)
                        intent.putExtra("product_id",sub_tech_product.get(position).id)
                        intent.putExtra("technical_id",sub_tech_product.get(position).technical_id)
                        overridePendingTransition(0,0)
                        startActivity(intent)
                    }
                })
        )
    }

    class subcategory(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun subcategoryResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val subcategory_status = jsonObj.getString("subcategory_status")

        if (subcategory_status.equals("0")){
            val jsonArrayData = jsonObj.getJSONArray("data")

            if (jsonArrayData.length() > 0){
                rv_sub_category.visibility = View.VISIBLE
                rv_sub_technical.visibility = View.GONE
                txt_nodata.visibility = View.GONE
                var jsonObject: JSONObject? = null

                for (i in 0 until jsonArrayData.length()) {
                    jsonObject = jsonArrayData.getJSONObject(i)
                    subcategory_list.add(
                        Subcategory_Model(jsonObject!!.getString("id"),jsonObject.getString("name")
                            , jsonObject.getString("cover"))
                    )
                }
                adapter_subcategory = CustomAdapter1(this, subcategory_list)
                rv_sub_category.adapter = adapter_subcategory
            }
            else{
                rv_sub_category.visibility = View.GONE
                rv_sub_technical.visibility = View.GONE
                txt_nodata.visibility = View.VISIBLE
            }
        }
        else if (subcategory_status.equals("1")){
            val jsonArrayData = jsonObj.getJSONArray("data")

            if (jsonArrayData.length() > 0) {
                rv_sub_category.visibility = View.GONE
                rv_sub_technical.visibility = View.VISIBLE
                txt_nodata.visibility = View.GONE
                var jsonObject: JSONObject? = null

                for (i in 0 until jsonArrayData.length()) {

                    jsonObject = jsonArrayData.getJSONObject(i)

                    if (i == 0) {
                        solution_check = jsonObject.getString("technical_name")
                        sub_tech_product.add(
                            Sub_Technical_Model(
                                jsonObject.getString("id"),
                                jsonObject.getString("name"),
                                jsonObject.getString("technical_name"),
                                jsonObject.getString("price"),
                                jsonObject.getString("sale_price"),
                                jsonObject.getString("cover"),
                                jsonObject.getString("additional_info"),
                                jsonObject.getString("dealer_name"),
                                jsonObject.getString("mobile")
                                ,
                                jsonObject.getString("category_name"),
                                "1",
                                jsonObject.getString("technical_id")
                            )
                        )

                        sub_tech_product.add(
                            Sub_Technical_Model(
                                jsonObject.getString("id"),
                                jsonObject.getString("name"),
                                jsonObject.getString("technical_name"),
                                jsonObject.getString("price"),
                                jsonObject.getString("sale_price"),
                                jsonObject.getString("cover"),
                                jsonObject.getString("additional_info"),
                                jsonObject.getString("dealer_name"),
                                jsonObject.getString("mobile")
                                ,
                                jsonObject.getString("category_name"),
                                "0",
                                jsonObject.getString("technical_id")
                            )
                        )

                    } else if (jsonObject.getString("technical_name").equals(solution_check)) {
                        solution_check = jsonObject.getString("technical_name")
                        sub_tech_product.add(
                            Sub_Technical_Model(
                                jsonObject.getString("id"),
                                jsonObject.getString("name"),
                                jsonObject.getString("technical_name"),
                                jsonObject.getString("price"),
                                jsonObject.getString("sale_price"),
                                jsonObject.getString("cover"),
                                jsonObject.getString("additional_info"),
                                jsonObject.getString("dealer_name"),
                                jsonObject.getString("mobile")
                                ,
                                jsonObject.getString("category_name"),
                                "0",
                                jsonObject.getString("technical_id")
                            )
                        )
                    } else {
                        solution_check = jsonObject.getString("technical_name")
                        sub_tech_product.add(
                            Sub_Technical_Model(
                                jsonObject.getString("id"),
                                jsonObject.getString("name"),
                                jsonObject.getString("technical_name"),
                                jsonObject.getString("price"),
                                jsonObject.getString("sale_price"),
                                jsonObject.getString("cover"),
                                jsonObject.getString("additional_info"),
                                jsonObject.getString("dealer_name"),
                                jsonObject.getString("mobile")
                                ,
                                jsonObject.getString("category_name"),
                                "1",
                                jsonObject.getString("technical_id")
                            )
                        )

                        sub_tech_product.add(
                            Sub_Technical_Model(
                                jsonObject.getString("id"),
                                jsonObject.getString("name"),
                                jsonObject.getString("technical_name"),
                                jsonObject.getString("price"),
                                jsonObject.getString("sale_price"),
                                jsonObject.getString("cover"),
                                jsonObject.getString("additional_info"),
                                jsonObject.getString("dealer_name"),
                                jsonObject.getString("mobile")
                                ,
                                jsonObject.getString("category_name"),
                                "0",
                                jsonObject.getString("technical_id")
                            )
                        )
                    }
                }

                adapter_sub_technical = Sub_Technical_Adapter(this, sub_tech_product)
                rv_sub_technical.adapter = adapter_sub_technical
            } else {
                rv_sub_category.visibility = View.GONE
                rv_sub_technical.visibility = View.GONE
                txt_nodata.visibility = View.VISIBLE
            }
        }
        else{
            rv_sub_category.visibility = View.GONE
            rv_sub_technical.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }

    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
