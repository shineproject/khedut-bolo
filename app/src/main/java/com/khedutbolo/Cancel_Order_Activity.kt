package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Cancel_Order_Adapter
import com.khedutbolo.Adapter.Cancel_Order_List_Adapter
import com.khedutbolo.Adapter.Complete_Order_List_Adapter
import com.khedutbolo.Model.Cancel_Order_Model
import com.khedutbolo.Model.cancel_Order_List_Model
import com.khedutbolo.Model.complete_Order_List_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Cancel_Order_Activity : AppCompatActivity() {

    lateinit var rv_cancel_list : RecyclerView
    private var str_id = ""
    lateinit var hud: KProgressHUD
    lateinit var txt_nodata : LinearLayout
    val cancel_order_list = ArrayList<cancel_Order_List_Model>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cancel__order_)

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = resources.getString(R.string.menu_cancel_order)
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        rv_cancel_list = findViewById(R.id.rv_cancel_list) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_cancel_list.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            cancelorderlist({
                hud.dismiss();
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@cancelorderlist
                } else {
                    cancelorderlistResponse(it)
                }

            }).execute("GET", AppConfig.CURRENT_LIST+str_id+"&flag=2", "")
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class cancelorderlist(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cancelorderlistResponse(response: String?) {

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0){
            rv_cancel_list.visibility = View.VISIBLE
            txt_nodata.visibility = View.GONE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {
                jsonObject = jsonArrayData.getJSONObject(i)

                cancel_order_list.add(cancel_Order_List_Model(jsonObject.getString("id"),
                    jsonObject.getString("created_at"), jsonObject.getString("total")))
            }

            val adapter_current = Cancel_Order_List_Adapter(this, cancel_order_list)
            rv_cancel_list.adapter = adapter_current
        }
        else{
            rv_cancel_list.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }

}
