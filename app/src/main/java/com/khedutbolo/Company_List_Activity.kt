package com.khedutbolo

import com.khedutbolo.Adapter.Company_List_Adapter
import com.khedutbolo.Adapter.Trending_Product_Adapter
import com.khedutbolo.Model.Company_List_Model
import com.khedutbolo.Model.Trending_Product_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.json.JSONArray
import org.json.JSONObject

class Company_List_Activity : AppCompatActivity() {

    lateinit var rv_company_list : RecyclerView
    val company_list = ArrayList<Company_List_Model>()
    lateinit var txt_nodata : LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company__list_)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.companies)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        rv_company_list = findViewById(R.id.rv_company_list) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_company_list.layoutManager = GridLayoutManager(this, 2)

        val jsonArray = JSONArray(DashBoardActivity.array_company)
        if (jsonArray.length() > 0){
            rv_company_list.visibility = View.VISIBLE
            txt_nodata.visibility = View.GONE
            val list = ArrayList<String>()
            var jsonObject: JSONObject? = null
            for (i in 0 until jsonArray.length()) {

                jsonObject = jsonArray.getJSONObject(i)

                company_list.add(Company_List_Model(jsonObject.getString("id"),jsonObject.getString("name"),jsonObject.getString("cover")))
            }

            val adapter_company_list = Company_List_Adapter(company_list)
            rv_company_list.adapter = adapter_company_list
        }else{
            rv_company_list.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }

        rv_company_list.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_company_list,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext,Company_Detail_Activity::class.java)
                        intent.putExtra("company_id",company_list.get(position).id)
                        intent.putExtra("company_name",company_list.get(position).name)
                        overridePendingTransition(0,0)
                        startActivity(intent)
                    }
                })
        )
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
