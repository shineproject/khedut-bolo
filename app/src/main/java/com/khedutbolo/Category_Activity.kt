package com.khedutbolo

import android.content.Context
import com.khedutbolo.Adapter.Category_Adapter
import com.khedutbolo.Model.Category_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Category_Activity : AppCompatActivity() {

    lateinit var rv_category : RecyclerView
    lateinit var hud: KProgressHUD
    lateinit var category_id: String
    var category_name: String = ""
    val category_product = ArrayList<Category_Model>()
    var solution_check: String = ""
    lateinit var txt_nodata : LinearLayout
    private var str_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_)

        val i = intent
        category_id = i.getStringExtra("category_id")
        category_name = i.getStringExtra("category_name")

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        if (category_name.equals("")){
            val actionbar = supportActionBar
            //set actionbar title
            actionbar!!.title = resources.getString(R.string.selectedcategory)
            //set back button
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }else{
            val actionbar = supportActionBar
            actionbar!!.title = category_name
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        rv_category = findViewById(R.id.rv_category) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_category.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            json.put("id", category_id)
            json.put("user_id", str_id)
            categoryproduct({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                    return@categoryproduct
                } else {
                    categoryResponse(it)
                }

            }).execute("POST", AppConfig.SELECT_CATEGORY, json.toString())
        }
        rv_category.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_category,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(applicationContext,AddtocartActivity::class.java)
                        intent.putExtra("product_id",category_product.get(position).id)
                        intent.putExtra("technical_id","")
                        overridePendingTransition(0,0)
                        startActivity(intent)
                    }
                })
        )
    }

    class categoryproduct(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun categoryResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0){

            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {
                rv_category.visibility = View.VISIBLE
                txt_nodata.visibility = View.GONE
                jsonObject = jsonArrayData.getJSONObject(i)

                if (i == 0){
                    solution_check = jsonObject.getString("technical_name")
                    category_product.add(Category_Model(jsonObject.getString("id"), jsonObject.getString("name"),
                        jsonObject.getString("price"), jsonObject.getString("cover"),jsonObject.getString("additional_info"),
                        jsonObject.getString("dealer_name"), jsonObject.getString("mobile"), jsonObject.getString("category_name"),
                        "1", jsonObject.getString("sale_price"), jsonObject.getString("technical_name")))

                    category_product.add(Category_Model(jsonObject.getString("id"), jsonObject.getString("name"),
                        jsonObject.getString("price"), jsonObject.getString("cover"),jsonObject.getString("additional_info"),
                        jsonObject.getString("dealer_name"), jsonObject.getString("mobile"), jsonObject.getString("category_name"),
                        "0", jsonObject.getString("sale_price"), jsonObject.getString("technical_name")))

                }else if (jsonObject.getString("technical_name").equals(solution_check)){
                    solution_check = jsonObject.getString("technical_name")
                    category_product.add(Category_Model(jsonObject.getString("id"), jsonObject.getString("name"),
                        jsonObject.getString("price"), jsonObject.getString("cover"),jsonObject.getString("additional_info"),
                        jsonObject.getString("dealer_name"), jsonObject.getString("mobile"), jsonObject.getString("category_name"),
                        "0", jsonObject.getString("sale_price"), jsonObject.getString("technical_name")))
                }else {
                    solution_check = jsonObject.getString("technical_name")
                    category_product.add(Category_Model(jsonObject.getString("id"), jsonObject.getString("name"),
                        jsonObject.getString("price"), jsonObject.getString("cover"),jsonObject.getString("additional_info"),
                        jsonObject.getString("dealer_name"), jsonObject.getString("mobile"), jsonObject.getString("category_name"),
                        "1", jsonObject.getString("sale_price"), jsonObject.getString("technical_name")))

                    category_product.add(Category_Model(jsonObject.getString("id"), jsonObject.getString("name"),
                        jsonObject.getString("price"), jsonObject.getString("cover"),jsonObject.getString("additional_info"),
                        jsonObject.getString("dealer_name"), jsonObject.getString("mobile"), jsonObject.getString("category_name"),
                        "0", jsonObject.getString("sale_price"), jsonObject.getString("technical_name")))
                }
            }

            val adapter_category = Category_Adapter(category_product)
            rv_category.adapter = adapter_category
        }
        else{
            rv_category.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
