package com.khedutbolo

import android.content.Context
import android.content.DialogInterface
import com.khedutbolo.Model.Shopping_Cart_Model
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Color.parseColor
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.math.RoundingMode
import java.net.HttpURLConnection
import java.net.URL
import java.text.DecimalFormat

class Shopping_Cart_Activity : AppCompatActivity() {

    lateinit var rv_shopping_cart: RecyclerView
    lateinit var btn_proceed: Button
    lateinit var btn_continue: Button

    private var total_qty = 0
    private var check_procuct: String = ""
    private var product_id: String = ""
    private lateinit var deleteIcon: Drawable
    private var colorDrawableBackground = ColorDrawable(parseColor("#00653A"))
    lateinit var hud: KProgressHUD

    companion object {
        private var select_item = 0
        internal var adapter_shopping_cart: Shopping_Cart_Adapter? = null
        val shopping_cart = ArrayList<Shopping_Cart_Model>()
        lateinit var l_cart: LinearLayout
        lateinit var txt_nodata: LinearLayout
        private var str_id = ""
        private var total_payment = ""
        private var str_total_point = ""
        private var str_total_amount = ""
        private var gst_price = ""
        private var delete_gst_price: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping__cart_)

        deleteIcon = ContextCompat.getDrawable(this, R.drawable.icon_delete)!!
        val typeface_regular = Typeface.createFromAsset(assets, "poppinsregular.otf")

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.shoppingcart)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val i = intent
        check_procuct = i.getStringExtra("check_product")
        product_id = i.getStringExtra("product_id")

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        rv_shopping_cart = findViewById(R.id.rv_cart_list) as RecyclerView
        btn_proceed = findViewById(R.id.btn_proceed) as Button
        btn_continue = findViewById(R.id.btn_continue) as Button
        l_cart = findViewById(R.id.linear_cart) as LinearLayout
        txt_nodata = findViewById(R.id.txt_nodata) as LinearLayout

        btn_proceed.typeface = typeface_regular
        btn_continue.typeface = typeface_regular

        rv_shopping_cart.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        if (shopping_cart.isEmpty()) {

        } else {
            shopping_cart.clear()
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            ShowCart({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@ShowCart
                } else {
                    showcartResponse(it)
                }

            }).execute("GET", AppConfig.SHOW_CART + str_id + "&user_id="+ str_id, json.toString())
        }

        rv_shopping_cart.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_shopping_cart,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, AddtocartActivity::class.java)
                        intent.putExtra("product_id", shopping_cart.get(position).id)
                        intent.putExtra("technical_id","")
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        finish()
                    }
                })
        )

        val itemTouchHelperCallback =
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    viewHolder2: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDirection: Int) {
                            (adapter_shopping_cart as Shopping_Cart_Adapter).removeItem(viewHolder.adapterPosition)
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {
                    val itemView = viewHolder.itemView
                    val iconMarginVertical = (viewHolder.itemView.height - deleteIcon.intrinsicHeight) / 2

                    if (dX > 0) {
                        colorDrawableBackground.setBounds(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                        deleteIcon.setBounds(
                            itemView.left + iconMarginVertical,
                            itemView.top + iconMarginVertical,
                            itemView.left + iconMarginVertical + deleteIcon.intrinsicWidth,
                            itemView.bottom - iconMarginVertical
                        )
                    } else {
                        colorDrawableBackground.setBounds(
                            itemView.right + dX.toInt(),
                            itemView.top,
                            itemView.right,
                            itemView.bottom
                        )
                        deleteIcon.setBounds(
                            itemView.right - iconMarginVertical - deleteIcon.intrinsicWidth,
                            itemView.top + iconMarginVertical,
                            itemView.right - iconMarginVertical,
                            itemView.bottom - iconMarginVertical
                        )
                        deleteIcon.level = 0
                    }

                    colorDrawableBackground.draw(c)

                    c.save()

                    if (dX > 0)
                        c.clipRect(itemView.left, itemView.top, dX.toInt(), itemView.bottom)
                    else
                        c.clipRect(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)

                    deleteIcon.draw(c)

                    c.restore()

                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                }
            }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(rv_shopping_cart)

        btn_proceed.setOnClickListener {
            val intent = Intent(applicationContext, Checkout_Activity::class.java)
            intent.putExtra("total_amount", total_payment)
            intent.putExtra("total_point", str_total_point)
            intent.putExtra("total_wallet_amount", str_total_amount)
            intent.putExtra("gst_price", gst_price)
            overridePendingTransition(0, 0)
            startActivity(intent)
        }

        btn_continue.setOnClickListener {
            val intent = Intent(applicationContext, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            overridePendingTransition(0, 0)
            startActivity(intent)
            finish()

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        if (check_procuct.equals("1")) {
            val intent = Intent(applicationContext, AddtocartActivity::class.java)
            intent.putExtra("product_id", product_id)
            intent.putExtra("technical_id","")
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        } else {
            super.onBackPressed()
        }

    }

    class ShowCart(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun showcartResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (data.equals("[]")) {
            txt_nodata.visibility = View.VISIBLE
            l_cart.visibility = View.GONE
        } else if (message.equals("Please enter user id.")) {
            txt_nodata.visibility = View.VISIBLE
            l_cart.visibility = View.GONE
        } else {
            val jsonData = JSONObject(data)
            var total = jsonData.getString("total")
            str_total_point = jsonData.getString("total_point")
            str_total_amount = jsonData.getString("total_wallet")
            total_payment = jsonData.getString("total_order_price")
            gst_price = jsonData.getString("gst_price")
            val jsonArrayData = jsonData.getJSONArray("carts")

            shopping_cart.clear()

            if (jsonArrayData.length() > 0) {
                l_cart.visibility = View.VISIBLE
                txt_nodata.visibility = View.GONE
                var jsonObject: JSONObject? = null

                for (i in 0 until jsonArrayData.length()) {

                    jsonObject = jsonArrayData.getJSONObject(i)

                    shopping_cart.add(
                        Shopping_Cart_Model(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("quantity"),
                            jsonObject.getString("product_size"),
                            jsonObject.getString("attribute_id"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("total_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("gst"),
                            jsonObject.getString("gst_price")
                        )
                    )
                }

                adapter_shopping_cart = Shopping_Cart_Adapter(shopping_cart)
                rv_shopping_cart.adapter = adapter_shopping_cart

            } else {
                txt_nodata.visibility = View.VISIBLE
                l_cart.visibility = View.GONE
            }
        }

    }


    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class Shopping_Cart_Adapter(val userList: ArrayList<Shopping_Cart_Model>) :
        RecyclerView.Adapter<Shopping_Cart_Adapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.shopping_cart_list, parent, false)
            return ViewHolder(v)
        }

        //this method is binding the data on the list
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val typeface_medium = Typeface.createFromAsset(holder.itemView.context.getAssets(), "poppinsmedium.otf")

            holder.textViewName.text = userList.get(position).name
            holder.textViewqty.text = holder.itemView.context.resources.getString(R.string.qtyy) + userList.get(position).quantity
            holder.textViewPrice.text = "₹ " + userList.get(position).sale_price
            holder.textViewSpent.text = "₹ " + userList.get(position).price + holder.itemView.context.resources.getString(R.string.perset)
            holder.textViewcompany.text = holder.itemView.context.resources.getString(R.string.by) + userList.get(position).dealer_name

            Glide.with(holder.itemView.context)
                .load(userList.get(position).cover)
                .error(R.mipmap.test_product)
                .into(holder.cart_image)

            holder.textViewName.typeface = typeface_medium
            holder.textViewPrice.typeface = typeface_medium
            holder.textViewSpent.typeface = typeface_medium
            holder.textViewqty.typeface = typeface_medium
            holder.textViewcompany.typeface = typeface_medium
            holder.textViewSeller.typeface = typeface_medium

            holder.textViewSpent.paintFlags = holder.textViewSpent.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


            holder.img_close.setOnClickListener {
                lateinit var hud: KProgressHUD

                val imageView = ImageView(holder.itemView.context)
                imageView.setBackgroundResource(R.drawable.load)

                Glide.with(holder.itemView.context)
                    .load(R.drawable.load)
                    .into(imageView)

                hud = KProgressHUD.create(holder.itemView.context)
                    .setCustomView(imageView)
                    .setBackgroundColor(Color.TRANSPARENT)
                    .setDimAmount(0.5f)
                hud.show()

            }
        }

        fun deleteResponse1(response: String?) {
            val jsonObj = JSONObject(response)
            val status = jsonObj.getString("status")
            val message = jsonObj.getString("message")
            val data = jsonObj.getString("data")

            val jsondata = JSONObject(data)

            total_payment = jsondata.getString("total_order_price")
            str_total_point = jsondata.getString("total_point")
            str_total_amount = jsondata.getString("total_wallet")
            val count = jsondata.getString("count")

            if (status.equals("200")) {
                adapter_shopping_cart?.notifyItemChanged(select_item);
                if (shopping_cart.isEmpty()) {
                    txt_nodata.visibility = View.VISIBLE
                    l_cart.visibility = View.GONE
                }

                gst_price = gst_price.replace(",","")
                delete_gst_price = delete_gst_price.replace(",","")

                val gst_price1 = (gst_price.toFloat() - delete_gst_price.toFloat())
                val df = DecimalFormat("#.##")
                df.roundingMode = RoundingMode.CEILING

                gst_price = df.format(gst_price1).toString()

                if (count.equals("0")) {
                    DashBoardActivity.tv.setText("0")
                    if (DashBoardActivity.add_cart.equals("")) {

                    } else {
                        AddtocartActivity.tv.setText("0")
                    }

                    if (DashBoardActivity.select_solution.equals("")) {

                    } else {
                        Select_Solution_Activity.tv.setText("0")
                    }

                    if (DashBoardActivity.trending.equals("")) {

                    } else {
                        Trending_Product_Activity.tv.setText("0")
                    }
//
                } else {
                    if (count.toInt() > 99) {
                        var data1 = "99+"
                        DashBoardActivity.tv.setText(data1)
                        if (DashBoardActivity.add_cart.equals("")) {

                        } else {
                            AddtocartActivity.tv.setText(data1)
                        }

                        if (DashBoardActivity.select_solution.equals("")) {

                        } else {
                            Select_Solution_Activity.tv.setText(data1)
                        }

                        if (DashBoardActivity.trending.equals("")) {

                        } else {
                            Trending_Product_Activity.tv.setText(data1)
                        }
                    } else {

                        DashBoardActivity.tv.setText(data)
                        if (DashBoardActivity.add_cart.equals("")) {

                        } else {
                            AddtocartActivity.tv.setText(count)
                        }
                        if (DashBoardActivity.select_solution.equals("")) {

                        } else {
                            Select_Solution_Activity.tv.setText(count)
                        }

                        if (DashBoardActivity.trending.equals("")) {

                        } else {
                            Trending_Product_Activity.tv.setText(count)
                        }
                    }

                }

            }
        }

        override fun getItemCount(): Int {
            return userList.size
        }

        fun removeItem(position: Int) {
            delete_gst_price = userList.get(position).gst_price
            val json = JSONObject()
            json.put("product_id", userList.get(position).id)
            json.put("customer_id", str_id)
            json.put("attribute_id", userList.get(position).attribute_id)
            json.put("delete_price", userList.get(position).total_price)
            json.put("total_price", total_payment)
            json.put("user_id", str_id)
            ViewHolder.Delete1({

                if (it == null) {
                    return@Delete1
                } else {
                    userList.removeAt(position)
                    notifyItemRemoved(position)
                    deleteResponse1(it)
                }

            }).execute("POST", AppConfig.DELETE, json.toString())

        }

        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val textViewName = itemView.findViewById(R.id.Product_name) as TextView
            val textViewPrice = itemView.findViewById(R.id.txt_price) as TextView
            val textViewSpent = itemView.findViewById(R.id.txt_spent) as TextView
            val textViewqty = itemView.findViewById(R.id.txt_qty) as TextView
            val textViewcompany = itemView.findViewById(R.id.companyname) as TextView
            val textViewSeller = itemView.findViewById(R.id.seller_name) as TextView
            val cart_image = itemView.findViewById(R.id.set_image) as ImageView
            val img_close = itemView.findViewById(R.id.img_close) as ImageView
            val l_card = itemView.findViewById(R.id.l_card) as LinearLayout

            class Delete1(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

                var callback = callback

                override fun doInBackground(vararg params: String): String? {
                    val url = URL(params[1])
                    val httpClient = url.openConnection() as HttpURLConnection
                    httpClient.setReadTimeout(10000)
                    httpClient.setConnectTimeout(10000)
                    httpClient.requestMethod = params[0]

                    if (params[0] == "POST") {
                        httpClient.instanceFollowRedirects = false
                        httpClient.doOutput = true
                        httpClient.doInput = true
                        httpClient.useCaches = false
                        httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
                    }
                    try {
                        if (params[0] == "POST") {
                            httpClient.connect()
                            val os = httpClient.getOutputStream()
                            val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                            writer.write(params[2])
                            writer.flush()
                            writer.close()
                            os.close()
                        }
                        if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                            val stream = BufferedInputStream(httpClient.inputStream)
                            val data: String = readStream(inputStream = stream)
                            return data
                        } else {
                            println("ERROR ${httpClient.responseCode}")
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    } finally {
                        httpClient.disconnect()
                    }

                    return null
                }

                fun readStream(inputStream: BufferedInputStream): String {
                    val bufferedReader = BufferedReader(InputStreamReader(inputStream))
                    val stringBuilder = StringBuilder()
                    bufferedReader.forEachLine { stringBuilder.append(it) }
                    return stringBuilder.toString()
                }

                override fun onPostExecute(result: String?) {
                    super.onPostExecute(result)
                    callback(result)
                }
            }
        }
    }
}
