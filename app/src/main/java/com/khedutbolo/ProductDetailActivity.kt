package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Cancel_Order_List_Adapter
import com.khedutbolo.Model.cancel_Order_List_Model
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class ProductDetailActivity : AppCompatActivity() {

    lateinit var img_product: ImageView
    lateinit var txt_product_title: TextView
    lateinit var txt_dealer: TextView
    lateinit var txt_seller: TextView
    lateinit var txt_product_size: TextView
    lateinit var txt_quantity: TextView
    lateinit var txt_value: TextView
    lateinit var txt_size: TextView
    lateinit var txt_selling_price: TextView
    lateinit var txt_total_price: TextView
    lateinit var txt_shipping_price: TextView
    lateinit var txt_gst: TextView
    lateinit var sell_price: TextView
    lateinit var total_price: TextView
    lateinit var shipping_price: TextView
    lateinit var gst: TextView
    private var name = ""
    private var image = ""
    private var sale_price = ""
    private var price = ""
    private var qty = ""
    private var size = ""
    private var shipping_price_text = ""
    private var dealer = ""
    private var total = ""
    private var str_gst = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.productdetail)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_regular = Typeface.createFromAsset(assets, "poppinsregular.otf")

        val i = intent
        name = i.getStringExtra("product_name")
        image = i.getStringExtra("image")
        sale_price = i.getStringExtra("sale_price")
        price = i.getStringExtra("price")
        qty = i.getStringExtra("qty")
        size = i.getStringExtra("size")
        shipping_price_text = i.getStringExtra("shipping_price")
        dealer = i.getStringExtra("dealer")
        total = i.getStringExtra("total")
        str_gst = i.getStringExtra("gst")

        img_product = findViewById(R.id.img_product) as ImageView
        txt_product_title = findViewById(R.id.txt_product) as TextView
        txt_dealer = findViewById(R.id.txt_dealer) as TextView
        txt_seller = findViewById(R.id.txt_seller) as TextView
        txt_product_size = findViewById(R.id.txt_product_size) as TextView
        txt_quantity = findViewById(R.id.txt_quantity) as TextView
        txt_value = findViewById(R.id.txtvalue) as TextView
        txt_size = findViewById(R.id.txtsize) as TextView
        txt_selling_price = findViewById(R.id.txt_selling_price) as TextView
        txt_total_price = findViewById(R.id.txt_total_price) as TextView
        txt_shipping_price = findViewById(R.id.txt_shipping_price) as TextView
        txt_gst = findViewById(R.id.txt_gst) as TextView
        sell_price = findViewById(R.id.sell_price) as TextView
        total_price = findViewById(R.id.total_price) as TextView
        shipping_price = findViewById(R.id.shipping_price) as TextView
        gst = findViewById(R.id.tax) as TextView

        txt_product_title.typeface = typeface_medium
        txt_dealer.typeface = typeface_regular
        txt_seller.typeface = typeface_regular
        txt_product_size.typeface = typeface_medium
        txt_quantity.typeface = typeface_medium
        txt_value.typeface = typeface_medium
        txt_size.typeface = typeface_medium
        txt_selling_price.typeface = typeface_medium
        txt_total_price.typeface = typeface_medium
        txt_shipping_price.typeface = typeface_medium
        txt_gst.typeface = typeface_medium
        sell_price.typeface = typeface_medium
        total_price.typeface = typeface_medium
        shipping_price.typeface = typeface_medium
        gst.typeface = typeface_medium

        Glide.with(this)
            .load(image)
            .error(R.mipmap.test_product)
            .into(img_product)

        txt_product_title.setText(name)
        txt_value.setText(qty)
        sell_price.setText("₹ "+sale_price)
        txt_size.setText(size)
        shipping_price.setText("₹ "+shipping_price_text)
        txt_dealer.setText(resources.getString(R.string.by)+dealer)
        gst.setText("₹ "+str_gst)
        total_price.setText("₹ "+total)


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


}
