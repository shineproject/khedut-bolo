package com.khedutbolo

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri
import android.util.Log
import android.view.View
import android.webkit.PermissionRequest
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.util.*


class Select_Language_Activity : Activity() {

    private var myButton : Button? = null
    private var rb_english : RadioButton? = null
    private var rb_hindi : RadioButton? = null
    private var rb_gujarati : RadioButton? = null
    private var txt_title : TextView? = null
    private val sharedPrefFile = "language"
    private var str_language = "en"
    var count = 0
    val REQUEST_ID_MULTIPLE_PERMISSIONS = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select__language_)


        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_semibold = Typeface.createFromAsset(assets, "poppinssemiBold.otf")

        checkAndRequestPermissions()

        val sharedPreferences: SharedPreferences = this.getSharedPreferences(sharedPrefFile, Context.MODE_PRIVATE)
        str_language = sharedPreferences.getString("lan_name", "")!!

        myButton = findViewById(R.id.btn_next) as Button
        rb_english = findViewById(R.id.r_english) as RadioButton
        rb_hindi = findViewById(R.id.r_hindi) as RadioButton
        rb_gujarati = findViewById(R.id.r_gujarati) as RadioButton
        txt_title = findViewById(R.id.txt_select_title) as TextView

        txt_title!!.typeface = typeface_semibold
        rb_english!!.typeface = typeface_medium
        rb_hindi!!.typeface = typeface_medium
        rb_gujarati!!.typeface = typeface_medium
        myButton!!.typeface = typeface_medium

        if (str_language.equals("hi")){
            rb_hindi!!.isChecked = true
        }else if (str_language.equals("gu")){
            rb_gujarati!!.isChecked = true
        }else if (str_language.equals("en")){
            rb_english!!.isChecked = true
        }else{
            rb_english!!.isChecked = true
        }

        val radioGroup = findViewById(R.id.r_group) as RadioGroup
        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                // checkedId is the RadioButton selected
                val radio: RadioButton = findViewById(checkedId)
                if (radio.text.equals("ENGLISH")){
                    str_language = "en"
                    setLocale("en")
                }else if (radio.text.equals("हिंदी")){
                    str_language = "hi"
                    setLocale("hi")
                }else if (radio.text.equals("ગુજરાતી")){
                    str_language = "gu"
                    setLocale("gu")
                }
            }
        })

        myButton!!.setOnClickListener {
            val intent = Intent(this, OtpActivity::class.java)
            intent.putExtra("language",str_language)
            startActivity(intent)
            overridePendingTransition(0,0)
            finish()

        }
    }

    private fun checkAndRequestPermissions(): Boolean {
        val camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val permissionLocation = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        val permissionRecordAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
        val permissionRecordtelephone = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)


        val listPermissionsNeeded = ArrayList<String>()

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA)
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (permissionRecordAudio != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO)
        }
        if (permissionRecordtelephone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE)
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
            return false
        }
        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        Log.d(TAG, "Permission callback called-------")
        when (requestCode) {
            REQUEST_ID_MULTIPLE_PERMISSIONS -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.CAMERA] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.ACCESS_FINE_LOCATION] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.RECORD_AUDIO] = PackageManager.PERMISSION_GRANTED
                perms[Manifest.permission.READ_PHONE_STATE] = PackageManager.PERMISSION_GRANTED
                if (grantResults.size > 0) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    if (perms[Manifest.permission.CAMERA] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.WRITE_EXTERNAL_STORAGE] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.ACCESS_FINE_LOCATION] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.RECORD_AUDIO] == PackageManager.PERMISSION_GRANTED
                        && perms[Manifest.permission.READ_PHONE_STATE] == PackageManager.PERMISSION_GRANTED){
                        Log.d(TAG, "sms & location services permission granted")
                        val intent = Intent(this, OtpActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(0,0)
                        finish()
                    } else {
                        Log.d(TAG, "Some permissions are not granted ask again ")
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)
                            || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)){
                            checkAndRequestPermissions()
                        } else {

                        }
                    }
                }
            }
        }

    }

    private fun setLocale(lang: String) {
        val locale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = locale
        res.updateConfiguration(conf, dm)

    }

    private fun setLocale1(lang: String) {
        val locale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = locale
        res.updateConfiguration(conf, dm)

            val refresh = Intent(this, Select_Language_Activity::class.java)
            refresh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            this.overridePendingTransition(0, 0)
            startActivity(refresh)
    }


}
