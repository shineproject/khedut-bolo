package com.khedutbolo

import android.content.Context
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import com.khedutbolo.Adapter.*
import com.khedutbolo.Model.*
import org.json.JSONObject
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import org.json.JSONArray
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.util.AppConfig
import java.io.*
import java.net.HttpURLConnection
import java.net.URL


class Trending_Product_Activity : AppCompatActivity() {

    lateinit var rv_trending_list : RecyclerView
    lateinit var rv_filter : RecyclerView
    lateinit var btn_clear : Button
    lateinit var btn_apply : Button
    private lateinit var searchView: SearchView
    internal var adapter_trending_product: Trending_Product_Adapter? = null
    lateinit var txt_nodata : LinearLayout
    private var str_id = ""
    lateinit var hud: KProgressHUD

    companion object{
        val trending_product = ArrayList<Trending_Product_Model>()
        lateinit var tv  : TextView
        lateinit var item  : MenuItem
        lateinit var actionView   : View
        lateinit var m_relative   : RelativeLayout
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trending__product_)

        DashBoardActivity.trending = "1"

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.trendingproduct)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        rv_trending_list = findViewById(R.id.rv_trending_list) as RecyclerView
        rv_filter = findViewById(R.id.rv_filter) as RecyclerView
        btn_clear = findViewById(R.id.btn_clear) as Button
        btn_apply = findViewById(R.id.btn_apply) as Button
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        btn_clear.typeface = typeface_medium
        btn_apply.typeface = typeface_medium

        rv_trending_list.layoutManager = GridLayoutManager(this, 2)
        rv_filter.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)

        try {
            val jsonArray = JSONArray(DashBoardActivity.array_trending)
            if (trending_product.isEmpty()){

            }else{
                trending_product.clear()
            }
            if (jsonArray.length() > 0){
                rv_trending_list.visibility = View.VISIBLE
                txt_nodata.visibility = View.GONE
                val list = ArrayList<String>()
                var jsonObject: JSONObject? = null

                for (i in 0 until jsonArray.length()) {

                    jsonObject = jsonArray.getJSONObject(i)

                    trending_product.add(Trending_Product_Model(jsonObject.getString("id"),jsonObject.getString("name")
                        , jsonObject.getString("cover"), jsonObject.getString("price"), jsonObject.getString("sale_price")
                        , jsonObject.getString("company_name")))
                }

                adapter_trending_product = Trending_Product_Adapter(this, trending_product)
                rv_trending_list.adapter = adapter_trending_product
            }else{
                rv_trending_list.visibility = View.GONE
                txt_nodata.visibility = View.VISIBLE
            }
        }catch (e: Exception) {
            e.printStackTrace()
        }


        rv_trending_list.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_trending_list,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext,AddtocartActivity::class.java)
                        intent.putExtra("product_id",trending_product.get(position).id)
                        intent.putExtra("technical_id","")
                        overridePendingTransition(0,0)
                        startActivity(intent)
                    }
                })
        )

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            Cartitem({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Cartitem
                } else {
                    cartitemResponse(it)
                }

            }).execute("GET", AppConfig.CART_COUNT + str_id+"&user_id="+str_id, json.toString())
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        val inflater = menuInflater
        inflater.inflate(R.menu.trending_product_menu, menu)

        item = menu!!.findItem(R.id.action_cart)
        actionView = MenuItemCompat.getActionView(item);
        tv = actionView.findViewById(R.id.txtCount);
        tv.setText("0");

        actionView.setOnClickListener{
            val intent = Intent(this, Shopping_Cart_Activity::class.java)
            intent.putExtra("check_product","")
            intent.putExtra("product_id","")
            overridePendingTransition(0, 0)
            startActivity(intent)
        }

        val searchItem = menu!!.findItem(R.id.action_search)
        searchView = searchItem.actionView as SearchView
        searchView.setQueryHint(resources.getString(R.string.search))
        searchView.setOnQueryTextListener(object :  SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                adapter_trending_product!!.filter(newText)
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                // task HERE
                //on submit send entire query
                return false
            }

        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
                return true
            }
            R.id.action_cart -> {
                val intent = Intent(this, Shopping_Cart_Activity::class.java)
                intent.putExtra("check_product","")
                intent.putExtra("product_id","")
                overridePendingTransition(0, 0)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class Cartitem(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cartitemResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")){
            if (data.equals("[]")){
                tv.setText("0")
            }else{
                if (data.toInt() > 99){
                    var data1 = "99+"
                    tv.setText(data1)
                }else{
                    tv.setText(data)
                }

            }
        }
    }
}
