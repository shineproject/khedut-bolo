package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Search_Cart_Adapter
import com.khedutbolo.Model.Product_Model_Search
import com.khedutbolo.Model.SearchModel
import com.khedutbolo.Model.Shopping_Cart_Model
import com.khedutbolo.Model.TrandingModel
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Searchactivity : AppCompatActivity() {

    lateinit var rv_product_list : RecyclerView
    lateinit var search : EditText
    lateinit var img_close : ImageView
    lateinit var txt_nodata: LinearLayout
    lateinit var progress: ProgressBar
    val searchlist = ArrayList<SearchModel>()
    private var str_id = ""

    companion object{
        val product_list = ArrayList<Product_Model_Search>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchactivity)

        val toolbar = findViewById(R.id.main_toolbar) as Toolbar?
        setSupportActionBar(toolbar)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        // These lines are needed to display the top-left hamburger button
        rv_product_list = findViewById(R.id.rv_product_list) as RecyclerView
        search = toolbar?.findViewById(R.id.search) as EditText
        img_close = toolbar?.findViewById(R.id.img_close) as ImageView
        txt_nodata = findViewById(R.id.txt_nodata) as LinearLayout
        progress = findViewById(R.id.progressbar) as ProgressBar

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.title = ""

        img_close.setOnClickListener{
            onBackPressed()
        }

        rv_product_list.layoutManager = GridLayoutManager(this, 2)

        rv_product_list.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_product_list,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, AddtocartActivity::class.java)
                        intent.putExtra("product_id", searchlist.get(position).id)
                        intent.putExtra("technical_id","")
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )

        search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    progress.isIndeterminate = true
                    progress.visibility = View.VISIBLE
                    val json = JSONObject()
                    searchitem({
                        progress.isIndeterminate = false
                        progress.visibility = View.GONE
                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            )
                                .show()
                            return@searchitem
                        } else {
                            searchitemResponse(it)
                        }

                    }).execute("GET", AppConfig.SEARCH+p0+"&user_id="+str_id, json.toString())
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class searchitem(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {

            super.onPostExecute(result)
            callback(result)
        }

    }

    fun searchitemResponse(response: String?) {

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (data.equals("[]")) {
            txt_nodata.visibility = View.VISIBLE
            rv_product_list.visibility = View.GONE
        }  else {
            if (searchlist.isEmpty()) {

            }else{
                searchlist.clear()
            }
            if (jsonArrayData.length() > 0) {
                rv_product_list.visibility = View.VISIBLE
                txt_nodata.visibility = View.GONE
                var jsonObject: JSONObject? = null

                for (i in 0 until jsonArrayData.length()) {

                    jsonObject = jsonArrayData.getJSONObject(i)

                    searchlist.add(
                        SearchModel(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("dealer_name")
                        )
                    )
                }

                val adapter_search_cart = Search_Cart_Adapter(searchlist)
                rv_product_list.adapter = adapter_search_cart

            } else {
                txt_nodata.visibility = View.VISIBLE
                rv_product_list.visibility = View.GONE
            }
        }

    }
}
