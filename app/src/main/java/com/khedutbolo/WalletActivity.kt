package com.khedutbolo

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Complete_Order_Adapter
import com.khedutbolo.Adapter.CustomAdapter1
import com.khedutbolo.Adapter.Sub_Technical_Adapter
import com.khedutbolo.Adapter.Wallet_Adapter
import com.khedutbolo.Model.Shopping_Cart_Model
import com.khedutbolo.Model.Sub_Technical_Model
import com.khedutbolo.Model.Subcategory_Model
import com.khedutbolo.Model.Wallet_Model
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class WalletActivity : AppCompatActivity() {

    private var str_id = ""
    private var str_total_point = ""
    private var str_total_amount = ""
    lateinit var rv_wallet : RecyclerView
    lateinit var l_recycle : LinearLayout
    lateinit var hud: KProgressHUD
    lateinit var txt_nodata : LinearLayout
    val wallet = ArrayList<Wallet_Model>()
    internal var adapter_wallet: Wallet_Adapter? = null
    lateinit var txt_point : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = resources.getString(R.string.wallet)
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        rv_wallet = findViewById(R.id.rv_wallet_list) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout
        l_recycle = findViewById(R.id.l_recycle)as LinearLayout
        txt_point = findViewById(R.id.walletpoint)as TextView

        rv_wallet.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)

        val horizontalDecoration2 = DividerItemDecoration(
            rv_wallet.getContext(),
            DividerItemDecoration.VERTICAL
        )
        val horizontalDivider2: Drawable =
            ContextCompat.getDrawable(this, R.drawable.horizontal_divider)!!
        horizontalDecoration2.setDrawable(horizontalDivider2)
        rv_wallet.addItemDecoration(horizontalDecoration2)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            walletstatus({
                hud.dismiss();
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@walletstatus
                } else {
                    walletstatusResponse(it)
                }

            }).execute("GET", AppConfig.WALLET_TRANSACTION+str_id+"&user_id="+str_id, json.toString())
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class walletstatus(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun walletstatusResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")
        str_total_point = jsonObj.getString("totalpoint")
        str_total_amount = jsonObj.getString("totalamount")

        txt_point.setText(str_total_point)

        if (data.equals("[]")){
            txt_nodata.visibility = View.VISIBLE
            l_recycle.visibility = View.GONE
        }else{
            val jsonArrayData = jsonObj.getJSONArray("data")


            if (jsonArrayData.length() > 0) {
                var jsonObject: JSONObject? = null

                for (i in 0 until jsonArrayData.length()) {

                    jsonObject = jsonArrayData.getJSONObject(i)

                    wallet.add(
                        Wallet_Model(
                            jsonObject.getString("id"),
                            jsonObject.getString("customer_id"),
                            jsonObject.getString("order_id"),
                            jsonObject.getString("amount"),
                            jsonObject.getString("points"),
                            jsonObject.getString("credit_or_debit"),
                            jsonObject.getString("created_at"),
                            jsonObject.getString("title")
                        )
                    )
                }

                adapter_wallet = Wallet_Adapter(wallet)
                rv_wallet.adapter = adapter_wallet

            } else {
                txt_nodata.visibility = View.VISIBLE
                l_recycle.visibility = View.GONE
            }
        }
    }

}
