package com.khedutbolo

import android.app.Activity
import android.app.AlertDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.media.MediaScannerConnection
import android.net.ConnectivityManager
import android.net.Uri
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.iid.FirebaseInstanceId
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.City_adapter
import com.khedutbolo.Adapter.State_Adapter
import com.khedutbolo.Adapter.Tehsil_Adapter
import com.khedutbolo.Adapter.Village_Adapter
import com.khedutbolo.Model.City_Model
import com.khedutbolo.Model.State_Model
import com.khedutbolo.Model.Tehsil_Model
import com.khedutbolo.Model.Village_Model
import com.khedutbolo.Service.AsyncTaskCompleteListener
import com.khedutbolo.Service.MultiPartRequester
import com.khedutbolo.util.AppConfig
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.HashMap

class RegistrationActivity : Activity(), AsyncTaskCompleteListener {

    lateinit var btn_submit: Button
    lateinit var ed_name: EditText
    lateinit var ed_phone_number: EditText
    lateinit var ed_address: EditText
    lateinit var img_click_add: CircleImageView
    lateinit var img_profile: CircleImageView
    lateinit var txt_title_register: TextView
    lateinit var txt_select: TextView
    private var rb_english: RadioButton? = null
    private var rb_hindi: RadioButton? = null
    private var rb_gujarati: RadioButton? = null
    private val GALLERY = 1
    private val CAMERA = 2
    private var ALL = 3
    var path: String = ""
    lateinit var hud: KProgressHUD
    var mobile_no: String = ""
    var str_language: String = "en"
    lateinit var spinner_state: Spinner
    lateinit var img_spinnerstate: ImageView
    lateinit var spinner_city: Spinner
    lateinit var img_spinnercity: ImageView
    lateinit var spinner_tehsil: Spinner
    lateinit var img_spinnertehsil: ImageView
    lateinit var spinner_village: Spinner
    lateinit var img_spinnervillage: ImageView
    val state_ = ArrayList<State_Model>()
    val city_ = ArrayList<City_Model>()
    val tehsil_ = ArrayList<Tehsil_Model>()
    val village_ = ArrayList<Village_Model>()
    private var str_state = ""
    private var str_city = ""
    private var str_tehsil = ""
    private var str_village = ""
    private var str_state_id = ""
    private var str_city_id = ""
    private var str_tehsil_id = ""
    private var str_village_id = ""
    private var check_keypad = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        val policy = StrictMode.ThreadPolicy.Builder()
            .permitAll().build()
        StrictMode.setThreadPolicy(policy)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = getString(R.string.default_notification_channel_id)
            val channelName = getString(R.string.default_notification_channel_name)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(
                NotificationChannel(
                    channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW
                )
            )
        }

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure()
        }

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val i = intent
        mobile_no = i.getStringExtra("mobile_no")
        str_language = i.getStringExtra("language")

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_semibold = Typeface.createFromAsset(assets, "poppinssemiBold.otf")

        btn_submit = findViewById(R.id.btn_submit) as Button
        ed_name = findViewById(R.id.ed_name) as TextInputEditText
        ed_phone_number = findViewById(R.id.ed_phone) as TextInputEditText
        ed_address = findViewById(R.id.ed_address) as TextInputEditText
        img_click_add = findViewById(R.id.img_profile_add) as CircleImageView
        img_profile = findViewById(R.id.img_profile) as CircleImageView
        txt_title_register = findViewById(R.id.txt_title_register) as TextView
        txt_select = findViewById(R.id.txt_select) as TextView
        rb_english = findViewById(R.id.r_english) as RadioButton
        rb_hindi = findViewById(R.id.r_hindi) as RadioButton
        rb_gujarati = findViewById(R.id.r_gujarati) as RadioButton
        spinner_state = findViewById(R.id.spinnerstate) as Spinner
        img_spinnerstate = findViewById(R.id.img_spinnerstate) as ImageView
        spinner_city = findViewById(R.id.spinnercity) as Spinner
        img_spinnercity = findViewById(R.id.img_spinnercity) as ImageView
        spinner_tehsil = findViewById(R.id.spinnertehsil) as Spinner
        img_spinnertehsil = findViewById(R.id.img_spinnertehsil) as ImageView
        spinner_village = findViewById(R.id.spinnervillage) as Spinner
        img_spinnervillage = findViewById(R.id.img_spinnervillage) as ImageView

        txt_title_register.typeface = typeface_semibold
        btn_submit.typeface = typeface_medium
        ed_name.typeface = typeface_medium
        ed_phone_number.typeface = typeface_medium
        ed_address.typeface = typeface_medium
        txt_select.typeface = typeface_medium
        rb_english!!.typeface = typeface_medium
        rb_hindi!!.typeface = typeface_medium
        rb_gujarati!!.typeface = typeface_medium

        ed_phone_number.setText(mobile_no)

        val radioGroup = findViewById(R.id.r_group) as RadioGroup
        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                // checkedId is the RadioButton selected
                val radio: RadioButton = findViewById(checkedId)
                if (radio.text.equals("ENGLISH")){
                    str_language = "en"
                }else if (radio.text.equals("हिंदी")){
                    str_language = "hi"
                }else if (radio.text.equals("ગુજરાતી")){
                    str_language = "gu"
                }
            }
        })

        if (str_language.equals("hi")){
            rb_hindi!!.isChecked = true
            rb_english!!.isChecked = false
            rb_gujarati!!.isChecked = false
        }else if (str_language.equals("gu")){
            rb_gujarati!!.isChecked = true
            rb_english!!.isChecked = false
            rb_hindi!!.isChecked = false
        }else if (str_language.equals("en")){
            rb_english!!.isChecked = true
            rb_gujarati!!.isChecked = false
            rb_hindi!!.isChecked = false
        }else{
            rb_english!!.isChecked = true
            rb_gujarati!!.isChecked = false
            rb_hindi!!.isChecked = false
        }


        btn_submit.setOnClickListener {
            if (ed_name.text.toString().trim().equals("")) {
                Toast.makeText(applicationContext, resources.getString(R.string.entername), Toast.LENGTH_LONG).show()
            } else if (ed_phone_number.text.toString().trim().equals("")) {
                Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
            } else if (ed_phone_number.text.toString().trim().length < 10) {
                Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
            } else if (str_state.equals(resources.getString(R.string.dropstate))) {
                Toast.makeText(applicationContext, resources.getString(R.string.selectstate), Toast.LENGTH_LONG).show()
            } else if (str_city.equals(resources.getString(R.string.dropdistrict))) {
                Toast.makeText(applicationContext, resources.getString(R.string.selectdistrict), Toast.LENGTH_LONG).show()
            } else if (str_tehsil.equals(resources.getString(R.string.droptehsil))) {
                Toast.makeText(applicationContext, resources.getString(R.string.selecttehsil), Toast.LENGTH_LONG).show()
            } else if (str_village.equals(resources.getString(R.string.dropvillage))) {
                Toast.makeText(applicationContext, resources.getString(R.string.selectvillage), Toast.LENGTH_LONG).show()
            } else {
                uploadImage()
            }

        }

        img_click_add.setOnClickListener {
            showPictureDialog()
        }

        img_spinnerstate.setOnClickListener {
            spinner_state.performClick()
        }

        img_spinnercity.setOnClickListener {
            spinner_city.performClick()
        }

        img_spinnertehsil.setOnClickListener {
            spinner_tehsil.performClick()
        }

        img_spinnervillage.setOnClickListener {
            spinner_village.performClick()
        }

        spinner_state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_state = state_.get(position).state
                str_state_id = state_.get(position).id
                if (str_state.equals(resources.getString(R.string.dropstate))) {

                } else {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    } else {
                        city_.clear()
                        hud.show()
                        val json = JSONObject()
                        json.put("state_id", state_.get(position).id)
                        json.put("code", "")
                        json.put("customer_id", "")
                        json.put("language", str_language)
                        Citydata({
                            hud.dismiss();

                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                ).show()
                                return@Citydata
                            } else {
                                cityResponse(it)
                            }
                        }).execute("POST", AppConfig.CITY, json.toString())
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        spinner_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_city = city_.get(position).city
                str_city_id = city_.get(position).id
                if (str_city.equals(resources.getString(R.string.dropdistrict))) {

                } else {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    } else {
                        tehsil_.clear()
                        hud.show()
                        Tehsildata({
                            hud.dismiss();

                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                ).show()
                                return@Tehsildata
                            } else {
                                tehsilResponse(it)
                            }

                        }).execute("GET", AppConfig.TEHSIL + str_city_id + "&code="+str_tehsil_id+"&customer_id="+""+"&language="+str_language, "")
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        spinner_tehsil.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_tehsil = tehsil_.get(position).tehsil
                str_tehsil_id = tehsil_.get(position).id

                if (str_tehsil.equals(resources.getString(R.string.droptehsil))) {

                } else {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    } else {
                        village_.clear()
                        hud.show()
                        Villagedata({
                            hud.dismiss();

                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                ).show()
                                return@Villagedata
                            } else {
                                villageResponse(it)
                            }

                        }).execute("GET", AppConfig.VILLAGE + str_tehsil_id + "&code="+str_village_id+"&customer_id="+""+"&language="+str_language, "")
                    }
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        spinner_village.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_village = village_.get(position).village
                str_village_id = village_.get(position).id

                if (check_keypad.equals("")){
                    ed_name.showKeyboard()
                    check_keypad = "1";
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            Statedata({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Statedata
                } else {
                    stateResponse(it)
                }

            }).execute("GET", AppConfig.STATE+str_state_id+"&customer_id="+""+"&language="+str_language, "")
        }
    }


    private fun uploadImage() {
        hud.show()
        val map = HashMap<String, String>()
        map.put("url", AppConfig.REGISTER)
        if (path.equals("")) {

        } else {
            map.put("image", path)
        }
        map.put("name", ed_name.text.toString().trim())
        map.put("mobile", ed_phone_number.text.toString().trim())
        map.put("language", str_language)
        map.put("token", OtpActivity.token)
        map.put("sid", str_state_id)
        map.put("cid", str_city_id)
        map.put("tid", str_tehsil_id)
        map.put("vid", str_village_id)
        MultiPartRequester(this, map, ALL, this)
    }

    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle(resources.getString(R.string.selectaction))
        val pictureDialogItems = arrayOf(resources.getString(R.string.selectgallery), resources.getString(R.string.selectcamera))
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /* if (resultCode == this.RESULT_CANCELED)
         {
         return
         }*/
        if (requestCode == GALLERY) {
            if (data != null) {
                ALL = requestCode
                val uri = data.data
                val FILE = arrayOf(MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA)
                val cursor = this.getContentResolver().query(uri!!, FILE, null, null, null)
                cursor!!.moveToFirst()
                val columnIndex = cursor!!.getColumnIndex(FILE[0])
                path = cursor!!.getString(columnIndex)
                cursor!!.close()
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                    img_profile!!.setImageBitmap(bitmap)

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, resources.getString(R.string.failed), Toast.LENGTH_SHORT).show()
                }

            }

        } else if (requestCode == CAMERA) {
            ALL = requestCode
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            img_profile!!.setImageBitmap(thumbnail)
            path = saveImage(thumbnail)
        } else {
            ALL = 0
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
        )
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {

            wallpaperDirectory.mkdirs()
        }

        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    private val IMAGE_DIRECTORY = "/Khedut_Bolo"

    override fun onTaskCompleted(response: String, serviceCode: Int) {
        hud.dismiss()
        Log.d("respon", response.toString())

        try {
            val jsonObject = JSONObject(response.toString())

            val status = jsonObject.getString("status")
            val message = jsonObject.getString("message")
            val data = jsonObject.getString("data")

            if (data.equals("[]")) {
                Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, resources.getString(R.string.userregister), Toast.LENGTH_LONG).show()

                val jsonData = JSONObject(data)
                val id = jsonData.getString("id")
                val name = jsonData.getString("name")
                val mobile = jsonData.getString("mobile")
                val language = jsonData.getString("language")
                val image = jsonData.getString("image")
                val sid = jsonData.getString("state_id")
                val cid = jsonData.getString("city_id")
                val tid = jsonData.getString("tehsil_id")
                val vid = jsonData.getString("village_id")
                val state = jsonData.getString("state")
                val city = jsonData.getString("city")
                val tehsil = jsonData.getString("tehsil")
                val village = jsonData.getString("village")

                val sharedProfile = this.getSharedPreferences("profile", 0)
                val editor: SharedPreferences.Editor = sharedProfile.edit()
                editor.putString("id", id)
                editor.putString("name", name)
                editor.putString("mobile", mobile)
                editor.putString("language", language)
                editor.putString("image", image)
                editor.putString("sid", sid)
                editor.putString("cid", cid)
                editor.putString("tid", tid)
                editor.putString("vid", vid)
                editor.putString("state_name", state)
                editor.putString("city_name", city)
                editor.putString("tehsil_name", tehsil)
                editor.putString("village_name", village)
                editor.putString("check_otp", "1")
                editor.apply()

                val sharedPreferences: SharedPreferences = this.getSharedPreferences("language", Context.MODE_PRIVATE)
                val editor1: SharedPreferences.Editor =  sharedPreferences.edit()
                editor1.putString("lan_name",language)
                editor1.apply()

                val intent = Intent(applicationContext, DashBoardActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    class Statedata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun stateResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            var jsonObject: JSONObject? = null

            state_.add(
                State_Model(
                    "", resources.getString(R.string.dropstate)
                )
            )

            city_.add(
                City_Model(
                    "", resources.getString(R.string.dropdistrict)
                )
            )
            val adapter_city = City_adapter(this, city_)
            spinner_city.adapter = adapter_city

            tehsil_.add(
                Tehsil_Model(
                    "", resources.getString(R.string.droptehsil)
                )
            )
            val adapter_tehsil = Tehsil_Adapter(this, tehsil_)
            spinner_tehsil.adapter = adapter_tehsil

            village_.add(
                Village_Model(
                    "", resources.getString(R.string.dropvillage)
                )
            )
            val adapter_village = Village_Adapter(this, village_)
            spinner_village.adapter = adapter_village

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                state_.add(
                    State_Model(
                        jsonObject.getString("STCode"), jsonObject.getString("DTName")
                    )
                )
            }

            val adapter_state = State_Adapter(this, state_)
            spinner_state.adapter = adapter_state

        } else {
//            Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
        }

    }

    class Citydata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cityResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            var jsonObject: JSONObject? = null

            city_.add(
                City_Model(
                    "", resources.getString(R.string.dropdistrict)
                )
            )

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                city_.add(
                    City_Model(
                        jsonObject.getString("DTCode"), jsonObject.getString("DTName")
                    )
                )
            }

            val adapter_city = City_adapter(this, city_)
            spinner_city.adapter = adapter_city

        } else {
        }

    }

    class Tehsildata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun tehsilResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {
            var jsonObject: JSONObject? = null

            tehsil_.add(
                Tehsil_Model(
                    "", resources.getString(R.string.droptehsil)
                )
            )

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                tehsil_.add(
                    Tehsil_Model(
                        jsonObject.getString("SDTCode"), jsonObject.getString("SDTName")
                    )
                )
            }

            val adapter_tehsil = Tehsil_Adapter(this, tehsil_)
            spinner_tehsil.adapter = adapter_tehsil

        } else {
        }

    }

    class Villagedata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun villageResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {
            var jsonObject: JSONObject? = null

            village_.add(
                Village_Model(
                    "", resources.getString(R.string.dropvillage)
                )
            )

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                village_.add(
                    Village_Model(
                        jsonObject.getString("TVCode"), jsonObject.getString("Name")
                    )
                )
            }

            val adapter_village = Village_Adapter(this, village_)
            spinner_village.adapter = adapter_village

        } else {
        }

    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.requestFocus()
        imm.showSoftInput(this, 0)
    }
}
