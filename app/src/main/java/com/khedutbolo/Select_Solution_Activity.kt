package com.khedutbolo

import android.annotation.SuppressLint
import android.content.Context
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.navigation.NavigationView
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.*
import com.khedutbolo.Model.*
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.app_bar_dash_board.*
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

class Select_Solution_Activity : AppCompatActivity() {

    lateinit var rv_solution: RecyclerView
    lateinit var rv_solution1: RecyclerView
    lateinit var rv_filter: RecyclerView
    lateinit var rv_filter1: RecyclerView
    lateinit var btn_clear: Button
    lateinit var btn_apply: Button
    lateinit var hud: KProgressHUD
    lateinit var solution_id: String
    lateinit var subsolution_id: String

    var solution_check: String = ""
    var solution_check1: String = ""
    var subsub_solution_id: String = ""
    val filter_product = ArrayList<Filter_Model>()
    val filter_product1 = ArrayList<Filter_Model1>()

    internal var adapter_solution: Solution_Adapter? = null
    var solution_check3: String = ""
    internal var filter_adapter_solution: Filter_Solution_Adapter? = null
    internal var adapter_filter: Filter_Adapter? = null
    internal var adapter_filter1: Filter_Adapter1? = null
    lateinit var drawerLayout: DrawerLayout
    lateinit var item1: MenuItem
    private lateinit var searchView: SearchView
    lateinit var txt_nodata : LinearLayout
    var solution_name: String = ""

    private var str_id = ""
    var array_solution_id = ArrayList<String>()
    var array_user_id = ArrayList<String>()
    var array_subsub_solution_id = ArrayList<String>()

    companion object{
        val solution_product = ArrayList<Solution_Model>()
        val solution_product1 = ArrayList<Solution_Model1>()
        var filter_company = ArrayList<String>()
        var filter_technical = ArrayList<String>()
        lateinit var tv  : TextView
        lateinit var item  : MenuItem
        lateinit var actionView   : View
        lateinit var m_relative   : RelativeLayout

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select__solution_)

        DashBoardActivity.select_solution = "1"

        val i = intent
        solution_id = i.getStringExtra("solution_id")
        solution_name = i.getStringExtra("solution_name")
        subsolution_id = i.getStringExtra("subsolution_id")

        array_solution_id.add(subsolution_id)
        array_subsub_solution_id.add(solution_id)


            val actionbar = supportActionBar
            actionbar!!.title = resources.getString(R.string.selsolution)
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        array_user_id.add(str_id)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")

        rv_solution = findViewById(R.id.rv_solution) as RecyclerView
        rv_solution1 = findViewById(R.id.rv_solution1) as RecyclerView
        rv_filter = findViewById(R.id.rv_filter) as RecyclerView
        rv_filter1 = findViewById(R.id.rv_filter1) as RecyclerView
        btn_clear = findViewById(R.id.btn_clear) as Button
        btn_apply = findViewById(R.id.btn_apply) as Button
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout
        drawerLayout = findViewById(R.id.parent_drawer_layout)

        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        btn_clear.typeface = typeface_medium
        btn_apply.typeface = typeface_medium

        rv_solution.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        rv_filter.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_filter1.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        if (solution_product.isEmpty()){

        }else{
            solution_product.clear()
        }

        if (solution_product1.isEmpty()){

        }else{
            solution_product.clear()
        }

        if (filter_technical.isEmpty()){

        }else{
            filter_technical.clear()
        }

        if (filter_company.isEmpty()){

        }else{
            filter_company.clear()
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            json.put("id", solution_id)
            json.put("user_id", str_id)
            cropSolution({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                    return@cropSolution
                } else {
                    solutionResponse(it)
                }

            }).execute("POST", AppConfig.SOLUTION, json.toString())
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            FilterList({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                    return@FilterList
                } else {
                    filterResponse(it)
                }

            }).execute("GET", AppConfig.FILTER_LIST+solution_id+"&user_id="+str_id, json.toString())
        }

        rv_solution.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_solution,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, AddtocartActivity::class.java)
                        intent.putExtra("product_id", solution_product.get(position).id)
                        intent.putExtra("technical_id","")
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )

        rv_solution1.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_solution1,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, AddtocartActivity::class.java)
                        intent.putExtra("product_id", solution_product1.get(position).id)
                        intent.putExtra("technical_id","")
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            Cartitem({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Cartitem
                } else {
                    cartitemResponse(it)
                }

            }).execute("GET", AppConfig.CART_COUNT + str_id+"&user_id="+str_id, json.toString())
        }

        btn_apply.setOnClickListener {
            if (!isNetworkAvailable()) {
                Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
            } else {
                if (filter_technical.size == 0 && filter_company.size == 0){
                    Toast.makeText(applicationContext, "Please select atleast one", Toast.LENGTH_LONG).show()
                }else{
                    hud.show()
                    val params = JsonObject()
                    val gson = GsonBuilder().create()
                    val jsonArray = gson.toJsonTree(filter_company)

                    val gson1 = GsonBuilder().create()
                    val jsonArray1 = gson1.toJsonTree(filter_technical)

                    val gson2 = GsonBuilder().create()
                    val jsonArray2 = gson2.toJsonTree(array_solution_id)

                    val gson3 = GsonBuilder().create()
                    val jsonArray3 = gson3.toJsonTree(array_user_id)

                    val gson4 = GsonBuilder().create()
                    val jsonArray4 = gson4.toJsonTree(array_subsub_solution_id)

                    params.add("companies", jsonArray)
                    params.add("technicals", jsonArray1)
                    params.add("solution_id", jsonArray2)
                    params.add("user_id", jsonArray3)
                    params.add("subsubSolution_id", jsonArray4)

                    FilterApply({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                            return@FilterApply
                        } else {
                            rv_solution1.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
                            solution_check1 = ""
                            solution_product1.clear()
                            rv_solution.visibility = View.GONE
                            rv_solution1.visibility = View.VISIBLE
                            if (solution_check3.equals("3")) {
                                if (filter_adapter_solution != null){
                                    filter_adapter_solution!!.notifyDataSetChanged()
                                }
                            } else {
                                solution_check3 = "3"
                            }
                            filterapplyResponse(it)
                            drawerLayout.closeDrawer(GravityCompat.END)
                            item1.setIcon(R.drawable.filter_before)
                        }

                    }).execute("POST", AppConfig.FILTER_APPLY, params.toString())
                }
            }

        }

        btn_clear.setOnClickListener {
            rv_solution.visibility = View.VISIBLE
            rv_solution1.visibility = View.GONE

            if (!isNetworkAvailable()) {
                Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
            } else {
                hud.show()
                val json = JSONObject()
                FilterList({
                    hud.dismiss();

                    if (it == null) {
                        Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                        return@FilterList
                    } else {
                        rv_filter.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                        rv_filter1.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

                        filter_product.clear()
                        filter_product1.clear()

                        adapter_filter!!.notifyDataSetChanged()
                        adapter_filter1!!.notifyDataSetChanged()
                        filterResponse(it)
                        drawerLayout.closeDrawer(GravityCompat.END)
                        item1.setIcon(R.drawable.filter_before)
                    }

                }).execute("GET", AppConfig.FILTER_LIST+solution_id+"&user_id="+str_id, json.toString())
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.solution_menu, menu)

        item = menu!!.findItem(R.id.action_cart)
        actionView = MenuItemCompat.getActionView(item);
        tv = actionView.findViewById(R.id.txtCount);
        tv.setText("0");

        actionView.setOnClickListener{
            val intent = Intent(this, Shopping_Cart_Activity::class.java)
            intent.putExtra("check_product","")
            intent.putExtra("product_id","")
            overridePendingTransition(0, 0)
            startActivity(intent)
        }

//        val searchItem = menu!!.findItem(R.id.action_search)
//        searchView = searchItem.actionView as SearchView
//        searchView.setQueryHint(resources.getString(R.string.search))
//        searchView.setOnQueryTextListener(object :  SearchView.OnQueryTextListener {
//
//            override fun onQueryTextChange(newText: String): Boolean {
//                if (rv_solution.visibility == View.VISIBLE){
//                    adapter_solution!!.filter(newText)
//                }else{
//                    filter_adapter_solution!!.filter(newText)
//                }
//                return false
//            }
//
//            override fun onQueryTextSubmit(query: String): Boolean {
//                return false
//            }
//
//        })

        return true
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_search -> {
                return true
            }
            R.id.action_cart -> {
                val intent = Intent(this, Shopping_Cart_Activity::class.java)
                intent.putExtra("check_product","")
                intent.putExtra("product_id","")
                overridePendingTransition(0, 0)
                startActivity(intent)
                return true
            }
            R.id.action_filter -> {

                val navView: NavigationView = findViewById(R.id.nav_parent_view)
                drawerLayout.setOnTouchListener(object : View.OnTouchListener {
                    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                        drawerLayout.closeDrawer(GravityCompat.END)
                        item.setIcon(R.drawable.filter_before)
                        item1 = item

                        return v?.onTouchEvent(event) ?: true
                    }
                })
                if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                    drawerLayout.closeDrawer(GravityCompat.END)
                    item.setIcon(R.drawable.filter_before)
                    item1 = item
                } else {
                    item.setIcon(R.drawable.filter_after)
                    drawerLayout.openDrawer(GravityCompat.END)
                    item1 = item
                }

                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {

        return super.dispatchTouchEvent(event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    class cropSolution(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun solutionResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (data.equals("[]")){
            rv_solution.visibility = View.GONE
            rv_solution1.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }else{
            val jsonArrayData = jsonObj.getJSONArray("data")
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                rv_solution.visibility = View.VISIBLE
                rv_solution1.visibility = View.GONE
                txt_nodata.visibility = View.GONE

                jsonObject = jsonArrayData.getJSONObject(i)

                if (i == 0) {
                    solution_check = jsonObject.getString("technical_name")
                    solution_product.add(
                        Solution_Model(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "1"
                        )
                    )

                    solution_product.add(
                        Solution_Model(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "0"
                        )
                    )

                } else if (jsonObject.getString("technical_name").equals(solution_check)) {
                    solution_check = jsonObject.getString("technical_name")
                    solution_product.add(
                        Solution_Model(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "0"
                        )
                    )
                } else {
                    solution_check = jsonObject.getString("technical_name")
                    solution_product.add(
                        Solution_Model(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "1"
                        )
                    )

                    solution_product.add(
                        Solution_Model(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "0"
                        )
                    )
                }
            }

            adapter_solution = Solution_Adapter(this, solution_product)
            rv_solution.adapter = adapter_solution
        }

    }

    class FilterList(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun filterResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (data.equals("[]")){
        }else{
            val jsonArrayData = jsonObj.getJSONArray("data")
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                val type = jsonObject.getString("type")
                val data = jsonObject.getString("data")

                if (type.equals("companies")) {
                    val jsonArrayData = jsonObject.getJSONArray("data")

                    if (jsonArrayData.length() > 0) {
                        var jsonObjectData: JSONObject? = null

                        for (i in 0 until jsonArrayData.length()) {
                            jsonObjectData = jsonArrayData.getJSONObject(i)

                            filter_product.add(
                                Filter_Model(jsonObjectData.getString("company_id"),
                                    jsonObjectData.getString("name"))
                            )
                        }
                    }

                    adapter_filter = Filter_Adapter(filter_product)
                    rv_filter.adapter = adapter_filter
                } else if (type.equals("techicals")) {
                    val jsonArrayData = jsonObject.getJSONArray("data")

                    if (jsonArrayData.length() > 0) {
                        var jsonObjectData: JSONObject? = null

                        for (i in 0 until jsonArrayData.length()) {
                            jsonObjectData = jsonArrayData.getJSONObject(i)

                            filter_product1.add(
                                Filter_Model1(jsonObjectData.getString("id"),
                                    jsonObjectData.getString("technical_name"))
                            )
                        }
                    }

                    adapter_filter1 = Filter_Adapter1(filter_product1)
                    rv_filter1.adapter = adapter_filter1
                }

            }
        }
    }

    class FilterApply(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun filterapplyResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            rv_solution1.visibility = View.VISIBLE
            rv_solution.visibility = View.GONE
            txt_nodata.visibility = View.GONE

            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                if (i == 0) {
                    solution_check1 = jsonObject.getString("technical_name")
                    solution_product1.add(
                        Solution_Model1(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "1"
                        )
                    )
//
                    solution_product1.add(
                        Solution_Model1(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile"),
                            jsonObject.getString("category_name"),
                            "0"
                        )
                    )

                } else if (jsonObject.getString("technical_name").equals(solution_check1)) {
                    solution_check1 = jsonObject.getString("technical_name")
                    solution_product1.add(
                        Solution_Model1(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "0"
                        )
                    )
                } else {
                    solution_check1 = jsonObject.getString("technical_name")
                    solution_product1.add(
                        Solution_Model1(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "1"
                        )
                    )

                    solution_product1.add(
                        Solution_Model1(
                            jsonObject.getString("id"),
                            jsonObject.getString("name"),
                            jsonObject.getString("technical_name"),
                            jsonObject.getString("price"),
                            jsonObject.getString("sale_price"),
                            jsonObject.getString("cover"),
                            jsonObject.getString("additional_info"),
                            jsonObject.getString("dealer_name"),
                            jsonObject.getString("mobile")
                            ,
                            jsonObject.getString("category_name"),
                            "0"
                        )
                    )
                }
            }

            filter_adapter_solution = Filter_Solution_Adapter(this, solution_product1)
            rv_solution1.adapter = filter_adapter_solution
        } else {
            rv_solution1.visibility = View.GONE
            rv_solution.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }


    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class Cartitem(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cartitemResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")){
            if (data.equals("[]")){
                tv.setText("0")
            }else{
                if (data.toInt() > 99){
                    var data1 = "99+"
                    tv.setText(data1)
                }else{
                    tv.setText(data)
                }
            }
        }
    }

}
