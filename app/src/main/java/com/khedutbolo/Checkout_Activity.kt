package com.khedutbolo

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import com.google.android.material.textfield.TextInputEditText
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.khedutbolo.Adapter.City_adapter
import com.khedutbolo.Adapter.State_Adapter
import com.khedutbolo.Adapter.Tehsil_Adapter
import com.khedutbolo.Adapter.Village_Adapter
import com.khedutbolo.Model.*
import com.shreyaspatil.EasyUpiPayment.EasyUpiPayment
import com.shreyaspatil.EasyUpiPayment.listener.PaymentStatusListener
import com.shreyaspatil.EasyUpiPayment.model.TransactionDetails
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.roundToInt


class Checkout_Activity : AppCompatActivity(), PaymentStatusListener {

    private var rb_cod: RadioButton? = null
    private var rb_netbanking: RadioButton? = null
    lateinit var card_bank_detail: CardView
    lateinit var txt_payment_method: TextView
    lateinit var txt_title_payment_method: TextView
    lateinit var ed_name: TextInputEditText
    lateinit var ed_mobile: TextInputEditText
    lateinit var ed_email: TextInputEditText
    lateinit var ed_pincode: TextInputEditText
    lateinit var ed_street: TextInputEditText
    lateinit var ed_city: TextInputEditText
    lateinit var ed_address: TextInputEditText
    lateinit var ed_bankname: TextInputEditText
    lateinit var ed_acname: TextInputEditText
    lateinit var ed_ifsccode: TextInputEditText
    lateinit var ed_actype: TextInputEditText
    lateinit var ed_acno: TextInputEditText
    lateinit var ed_total_payment: TextInputEditText
    lateinit var l_payment: LinearLayout
    lateinit var txt_title_order: TextView
    lateinit var txt_title_shipping: TextView
    lateinit var txt_shipping_address: TextView
    lateinit var txt_title_payment: TextView
    lateinit var txt_title_item: TextView
    lateinit var txt_itemname: TextView
    lateinit var txt_title_price: TextView
    lateinit var txt_title_qty: TextView
    lateinit var txt_title_total: TextView
    lateinit var txt_product: TextView
    lateinit var txt_price: TextView
    lateinit var txt_qty: TextView
    lateinit var txt_total_price: TextView
    lateinit var wallet_amount: TextView
    lateinit var txt_wallet_amount: TextView
    lateinit var gst_amount: TextView
    lateinit var txt_gst_amount: TextView
    lateinit var txt_note: TextView
    lateinit var txt_note1: TextView
    lateinit var l_price: LinearLayout
    lateinit var l_wallet: LinearLayout
    lateinit var l_gst: LinearLayout
    lateinit var btn_submit: Button
    lateinit var btn_payment: Button
    lateinit var hud: KProgressHUD
    private var str_id = ""
    private var str_address = ""
    private var str_name = ""
    private var str_mobile = ""
    lateinit var spinner_state: Spinner
    lateinit var img_spinnerstate: ImageView
    lateinit var spinner_city: Spinner
    lateinit var img_spinnercity: ImageView
    lateinit var spinner_tehsil: Spinner
    lateinit var img_spinnertehsil: ImageView
    lateinit var spinner_village: Spinner
    lateinit var img_spinnervillage: ImageView
    val state_ = ArrayList<State_Model>()
    val city_ = ArrayList<City_Model>()
    val tehsil_ = ArrayList<Tehsil_Model>()
    val village_ = ArrayList<Village_Model>()
    private var str_state = ""
    private var str_city = ""
    private var str_tehsil = ""
    private var str_village = ""
    private var str_state_id = ""
    private var str_city_id = ""
    private var str_tehsil_id = ""
    private var str_village_id = ""
    private var total_amount = ""
    private var total_wallet_point = ""
    private var total_wallet_amount = ""
    private var total_gst_price = ""
    private var ref_trns_id = ""
    private var trns_id = ""
    private var app_name = ""
    private var check_keypad = ""
    private var select_payment = "COD"
    lateinit var emailPattern: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout_)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.shippingdetail)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z]+"

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!
        str_name = sharedProfile.getString("name", "")!!
        str_address = sharedProfile.getString("address", "")!!
        str_mobile = sharedProfile.getString("mobile", "")!!
        str_state_id = sharedProfile.getString("sid", "")!!
        str_city_id = sharedProfile.getString("cid", "")!!
        str_tehsil_id = sharedProfile.getString("tid", "")!!
        str_village_id = sharedProfile.getString("vid", "")!!

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val i = intent
        total_amount = i.getStringExtra("total_amount")
        total_wallet_point = i.getStringExtra("total_point")
        total_wallet_amount = i.getStringExtra("total_wallet_amount")
        total_gst_price = i.getStringExtra("gst_price")

        rb_cod = findViewById(R.id.r_cod) as RadioButton
        rb_netbanking = findViewById(R.id.r_netbanking) as RadioButton
        val radioPayment = findViewById(R.id.r_payment) as RadioGroup
        card_bank_detail = findViewById(R.id.card_bank) as CardView
        txt_payment_method = findViewById(R.id.txt_selected_method) as TextView
        ed_name = findViewById(R.id.ed_name) as TextInputEditText
        ed_mobile = findViewById(R.id.ed_mobile) as TextInputEditText
        ed_email = findViewById(R.id.ed_email) as TextInputEditText
        ed_pincode = findViewById(R.id.ed_pincode) as TextInputEditText
        ed_street = findViewById(R.id.ed_street) as TextInputEditText
        ed_city = findViewById(R.id.ed_city) as TextInputEditText
        ed_address = findViewById(R.id.ed_address) as TextInputEditText

        ed_bankname = findViewById(R.id.ed_bankname) as TextInputEditText
        ed_acname = findViewById(R.id.ed_acname) as TextInputEditText
        ed_ifsccode = findViewById(R.id.ed_ifsccode) as TextInputEditText
        ed_actype = findViewById(R.id.ed_actype) as TextInputEditText
        ed_acno = findViewById(R.id.ed_acno) as TextInputEditText
        ed_total_payment = findViewById(R.id.ed_payment_total) as TextInputEditText
        txt_title_payment_method = findViewById(R.id.txt_payment) as TextView
        txt_title_order = findViewById(R.id.txt_order) as TextView
        txt_title_shipping = findViewById(R.id.txt_title_shipping) as TextView
        txt_shipping_address = findViewById(R.id.txt_shipping_address) as TextView
        txt_title_payment = findViewById(R.id.txt_title_payment) as TextView
        txt_title_item = findViewById(R.id.txt_title_item) as TextView
        btn_submit = findViewById(R.id.btn_submit) as Button
        txt_itemname = findViewById(R.id.txt_item) as TextView
        txt_title_price = findViewById(R.id.txt_title_price) as TextView
        txt_title_qty = findViewById(R.id.txt_quntity) as TextView
        txt_title_total = findViewById(R.id.txt_title_total) as TextView
        txt_product = findViewById(R.id.txt_product_name) as TextView
        txt_price = findViewById(R.id.txt_price) as TextView
        txt_qty = findViewById(R.id.txt_qty) as TextView
        txt_total_price = findViewById(R.id.txt_total_price) as TextView
        spinner_state = findViewById(R.id.spinnerstate) as Spinner
        img_spinnerstate = findViewById(R.id.img_spinnerstate) as ImageView
        spinner_city = findViewById(R.id.spinnercity) as Spinner
        img_spinnercity = findViewById(R.id.img_spinnercity) as ImageView
        spinner_tehsil = findViewById(R.id.spinnertehsil) as Spinner
        img_spinnertehsil = findViewById(R.id.img_spinnertehsil) as ImageView
        spinner_village = findViewById(R.id.spinnervillage) as Spinner
        img_spinnervillage = findViewById(R.id.img_spinnervillage) as ImageView
        l_payment = findViewById(R.id.l_payment) as LinearLayout
        btn_payment = findViewById(R.id.btn_payment) as Button
        wallet_amount = findViewById(R.id.wallet_amount) as TextView
        txt_wallet_amount = findViewById(R.id.txt_wallet_amount) as TextView
        l_price = findViewById(R.id.l_price) as LinearLayout
        l_wallet = findViewById(R.id.l_wallet) as LinearLayout
        l_gst = findViewById(R.id.l_gst) as LinearLayout
        gst_amount = findViewById(R.id.txt_gst) as TextView
        txt_gst_amount = findViewById(R.id.txt_gst_amount) as TextView
        txt_note = findViewById(R.id.txt_note) as TextView
        txt_note1 = findViewById(R.id.txt_note1) as TextView

        ed_name.typeface = typeface_medium
        ed_mobile.typeface = typeface_medium
        ed_pincode.typeface = typeface_medium
        ed_street.typeface = typeface_medium
        ed_city.typeface = typeface_medium
        ed_address.typeface = typeface_medium
        txt_title_payment_method.typeface = typeface_medium
        rb_cod!!.typeface = typeface_medium
        rb_netbanking!!.typeface = typeface_medium
        ed_bankname.typeface = typeface_medium
        ed_acname.typeface = typeface_medium
        ed_ifsccode.typeface = typeface_medium
        ed_actype.typeface = typeface_medium
        ed_acno.typeface = typeface_medium
        txt_title_order.typeface = typeface_medium
        txt_title_shipping.typeface = typeface_medium
        txt_shipping_address.typeface = typeface_medium
        txt_title_payment.typeface = typeface_medium
        txt_payment_method.typeface = typeface_medium
        txt_title_item.typeface = typeface_medium
        txt_itemname.typeface = typeface_medium
        txt_title_price.typeface = typeface_medium
        txt_title_qty.typeface = typeface_medium
        txt_title_total.typeface = typeface_medium
        txt_product.typeface = typeface_medium
        txt_price.typeface = typeface_medium
        txt_qty.typeface = typeface_medium
        txt_total_price.typeface = typeface_medium
        wallet_amount.typeface = typeface_medium
        txt_wallet_amount.typeface = typeface_medium
        btn_submit.typeface = typeface_medium
        gst_amount.typeface = typeface_medium
        txt_gst_amount.typeface = typeface_medium
        txt_note.typeface = typeface_medium
        txt_note1.typeface = typeface_medium

        l_price.visibility = View.VISIBLE
        l_wallet.visibility = View.VISIBLE

        total_amount = total_amount.replace(",","")
        total_gst_price = total_gst_price.replace(",","")
        total_wallet_amount = total_wallet_amount.replace(",","")

        val total1 = (total_amount.toFloat())

        txt_price.setText("₹ " + String.format("%.2f", total1))

        val total_display = (total_amount.toFloat() - total_wallet_amount.toFloat())
        txt_total_price.setText("₹ " +String.format("%.2f", total_display))

        txt_wallet_amount.setText("- ₹ " + total_wallet_amount)
        txt_gst_amount.setText("₹ " + total_gst_price)

        ed_total_payment.setText(String.format("%.2f", total_display))
        total_amount = total_display.toString()

        ed_name.setText(str_name)
        ed_mobile.setText(str_mobile)

        ed_total_payment.isLongClickable = false

        ed_street.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0!!.isBlank()) {
                    txt_shipping_address.setText(str_address)
                } else {
                    txt_shipping_address.setText(p0)
                }
            }

            override fun afterTextChanged(p0: Editable?) {

            }
        })

        radioPayment.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                val radio: RadioButton = findViewById(checkedId)
                if (radio.text.equals(resources.getString(R.string.cod))) {
                    l_payment.visibility = View.GONE
                    select_payment = "COD"
                    txt_payment_method.setText(resources.getString(R.string.cod))
                    btn_submit.visibility = View.VISIBLE
                } else if (radio.text.equals(resources.getString(R.string.netbank))) {
                    l_payment.visibility = View.VISIBLE
                    select_payment = "Net Banking"
                    txt_payment_method.setText(resources.getString(R.string.netbank))
                    btn_submit.visibility = View.GONE
                }
            }
        })

        img_spinnerstate.setOnClickListener {
            spinner_state.performClick()
        }

        img_spinnercity.setOnClickListener {
            spinner_city.performClick()
        }

        img_spinnertehsil.setOnClickListener {
            spinner_tehsil.performClick()
        }

        img_spinnervillage.setOnClickListener {
            spinner_village.performClick()
        }

        spinner_state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_state = state_.get(position).state
                str_state_id = state_.get(position).id
                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    city_.clear()
                    hud.show()
                    val json = JSONObject()
                    json.put("state_id", state_.get(position).id)
                    json.put("code", str_city_id)
                    json.put("customer_id", str_id)
                    json.put("language", "")
                    Citydata({
                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@Citydata
                        } else {
                            cityResponse(it)
                        }
                    }).execute("POST", AppConfig.CITY, json.toString())
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        spinner_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_city = city_.get(position).city
                str_city_id = city_.get(position).id
                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    tehsil_.clear()
                    Tehsildata({
                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@Tehsildata
                        } else {
                            tehsilResponse(it)
                        }

                    }).execute("GET", AppConfig.TEHSIL + str_city_id + "&code=" + str_tehsil_id+"&customer_id="+str_id+"&language="+"", "")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        spinner_tehsil.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_tehsil = tehsil_.get(position).tehsil
                str_tehsil_id = tehsil_.get(position).id

                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    village_.clear()
                    Villagedata({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@Villagedata
                        } else {
                            villageResponse(it)
                        }

                    }).execute("GET", AppConfig.VILLAGE + str_tehsil_id + "&code=" + str_village_id+"&customer_id="+str_id+"&language="+"", "")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        spinner_village.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_village = village_.get(position).village
                str_village_id = village_.get(position).id

                if (check_keypad.equals("")) {
                    ed_name.showKeyboard()
                    val count = ed_name.text!!.length
                    ed_name.setSelection(count)
                    check_keypad = "1";
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }

        btn_submit.setOnClickListener {
            if (ed_email.text.toString().trim().equals("")) {

                if (ed_name.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entername), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().length < 10) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().length < 6) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode6), Toast.LENGTH_LONG).show()
                } else if (ed_street.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterfulladdress), Toast.LENGTH_LONG).show()
                } else if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    hud.show()
                    val json = JSONObject()
                    json.put("alias", ed_name.text.toString().trim())
                    json.put("mobile", ed_mobile.text.toString().trim())
                    json.put("mail", ed_email.text.toString().trim())
                    json.put("zip", ed_pincode.text.toString().trim())
                    json.put("street", ed_street.text.toString().trim())
                    json.put("full_address", "")
                    json.put("payment", select_payment)
                    json.put("customer_id", str_id)
                    json.put("state", str_state_id)
                    json.put("city", str_city_id)
                    json.put("tid", str_tehsil_id)
                    json.put("vid", str_village_id)
                    json.put("wallet_point", total_wallet_point)
                    json.put("total_gst", total_gst_price)
                    json.put("user_id", str_id)
                    Order({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@Order
                        } else {
                            orderResponse(it)
                        }

                    }).execute("POST", AppConfig.ORDER, json.toString())
                }

            } else {
                if (ed_name.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entername), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().length < 10) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
                } else if (!ed_email.text.toString().trim() { it <= ' ' }.matches(emailPattern.toRegex())) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entervalidemail), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().length < 6) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode6), Toast.LENGTH_LONG).show()
                } else if (ed_street.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterfulladdress), Toast.LENGTH_LONG).show()
                } else if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    hud.show()
                    val json = JSONObject()
                    json.put("alias", ed_name.text.toString().trim())
                    json.put("mobile", ed_mobile.text.toString().trim())
                    json.put("mail", ed_email.text.toString().trim())
                    json.put("zip", ed_pincode.text.toString().trim())
                    json.put("street", ed_street.text.toString().trim())
                    json.put("full_address", "")
                    json.put("payment", select_payment)
                    json.put("customer_id", str_id)
                    json.put("state", str_state_id)
                    json.put("city", str_city_id)
                    json.put("tid", str_tehsil_id)
                    json.put("vid", str_village_id)
                    json.put("wallet_point", total_wallet_point)
                    json.put("total_gst", total_gst_price)
                    json.put("user_id", str_id)
                    Order({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@Order
                        } else {
                            orderResponse(it)
                        }

                    }).execute("POST", AppConfig.ORDER, json.toString())
                }
            }

        }

        btn_payment.setOnClickListener {
            if (ed_email.text.toString().trim().equals("")) {

                if (ed_name.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entername), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().length < 10) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().length < 6) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode6), Toast.LENGTH_LONG).show()
                } else if (ed_street.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterfulladdress), Toast.LENGTH_LONG).show()
                }else if (ed_total_payment.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enteramount), Toast.LENGTH_LONG).show()
                } else if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    trns_id = System.currentTimeMillis().toString()
                    ref_trns_id = "TR" + System.currentTimeMillis()

                    val easyUpiPayment = EasyUpiPayment.Builder()
                        .with(this@Checkout_Activity)
                        .setPayeeVpa("Q42588968@ybl")
                        .setPayeeName(ed_name.text.toString().trim())
                        .setTransactionId(trns_id)
                        .setTransactionRefId(ref_trns_id)
                        .setDescription("Payment")
                        .setAmount(total_amount)  //twoDigitsF.toString()
                        .build()

                    easyUpiPayment.setPaymentStatusListener(this@Checkout_Activity)

                    easyUpiPayment.startPayment()
                }

            } else {
                if (ed_name.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entername), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
                } else if (ed_mobile.text.toString().trim().length < 10) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
                } else if (!ed_email.text.toString().trim() { it <= ' ' }.matches(emailPattern.toRegex())) {
                    Toast.makeText(applicationContext, resources.getString(R.string.entervalidemail), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode), Toast.LENGTH_LONG).show()
                } else if (ed_pincode.text.toString().trim().length < 6) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterpincode6), Toast.LENGTH_LONG).show()
                } else if (ed_street.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enterfulladdress), Toast.LENGTH_LONG).show()
                } else if (ed_total_payment.text.toString().trim().equals("")) {
                    Toast.makeText(applicationContext, resources.getString(R.string.enteramount), Toast.LENGTH_LONG).show()
                } else if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    trns_id = System.currentTimeMillis().toString()
                    ref_trns_id = "TR" + System.currentTimeMillis()

                    val easyUpiPayment = EasyUpiPayment.Builder()
                        .with(this@Checkout_Activity)
                        .setPayeeVpa("Q42588968@ybl")
                        .setPayeeName(ed_name.text.toString().trim())
                        .setTransactionId(trns_id)
                        .setTransactionRefId(ref_trns_id)
                        .setDescription("Payment")
                        .setAmount(total_amount)  //twoDigitsF.toString()
                        .build()

                    easyUpiPayment.setPaymentStatusListener(this@Checkout_Activity)

                    easyUpiPayment.startPayment()
                }
            }

        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            Statedata({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Statedata
                } else {
                    stateResponse(it)
                }

            }).execute("GET", AppConfig.STATE + str_state_id+"&customer_id="+str_id+"&language="+"", "")
        }

    }

    override fun onTransactionSubmitted() {
    }

    override fun onTransactionCancelled() {
        Toast.makeText(applicationContext, "Cancelled", Toast.LENGTH_LONG).show()
    }

    override fun onTransactionSuccess() {
        btn_submit.visibility = View.VISIBLE
        hud.show()
        val json = JSONObject()
        json.put("device", "Android")
        json.put("amount", total_amount)
        json.put("transaction_id", trns_id)
        json.put("payment_type", "upi")
        json.put("userid", str_id)
        json.put("status", "success")
        json.put("user_id", str_id)
        payment({
            hud.dismiss();

            if (it == null) {
                Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                    .show()
                return@payment
            } else {
                paymentResponse(it)
            }

        }).execute("POST", AppConfig.PAYMENT, json.toString())
    }

    override fun onAppNotFound() {
        Toast.makeText(applicationContext, "App Not Found", Toast.LENGTH_LONG).show()
    }

    override fun onTransactionCompleted(transactionDetails: TransactionDetails?) {
    }

    override fun onTransactionFailed() {
        Toast.makeText(applicationContext, "Failed", Toast.LENGTH_LONG).show()
    }

    class Order(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun orderResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")) {
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage(resources.getString(R.string.ordersuccess))

                .setCancelable(false)
                .setPositiveButton(resources.getString(android.R.string.ok), DialogInterface.OnClickListener { dialog, id ->
                    val intent = Intent(applicationContext, DashBoardActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                    finish()
                })
            val alert = dialogBuilder.create()
            alert.setTitle(resources.getString(R.string.app_name))
            alert.show()

        } else {
            Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
        }

    }

    class Statedata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun stateResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                state_.add(
                    State_Model(
                        jsonObject.getString("STCode"), jsonObject.getString("DTName")
                    )
                )
            }

            val adapter_state = State_Adapter(this, state_)
            spinner_state.adapter = adapter_state

        } else {
        }

    }

    class Citydata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cityResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                city_.add(
                    City_Model(
                        jsonObject.getString("DTCode"), jsonObject.getString("DTName")
                    )
                )
            }

            val adapter_city = City_adapter(this, city_)
            spinner_city.adapter = adapter_city

        } else {
        }

    }

    class Tehsildata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun tehsilResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                tehsil_.add(
                    Tehsil_Model(
                        jsonObject.getString("SDTCode"), jsonObject.getString("SDTName")
                    )
                )
            }

            val adapter_tehsil = Tehsil_Adapter(this, tehsil_)
            spinner_tehsil.adapter = adapter_tehsil

        } else {
        }

    }

    class Villagedata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun villageResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                village_.add(
                    Village_Model(
                        jsonObject.getString("TVCode"), jsonObject.getString("Name")
                    )
                )
            }

            val adapter_village = Village_Adapter(this, village_)
            spinner_village.adapter = adapter_village

        } else {
        }

    }

    class payment(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun paymentResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")) {
            hud.show()

            val json = JSONObject()
            json.put("alias", ed_name.text.toString().trim())
            json.put("mobile", ed_mobile.text.toString().trim())
            json.put("mail", ed_email.text.toString().trim())
            json.put("zip", ed_pincode.text.toString().trim())
            json.put("street", ed_street.text.toString().trim())
            json.put("full_address", "")
            json.put("payment", select_payment)
            json.put("customer_id", str_id)
            json.put("state", str_state_id)
            json.put("city", str_city_id)
            json.put("tid", str_tehsil_id)
            json.put("vid", str_village_id)
            json.put("wallet_point", total_wallet_point)
            json.put("total_gst", total_gst_price)
            json.put("user_id", str_id)

            Order({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(
                        applicationContext,
                        resources.getString(R.string.connectionerror),
                        Toast.LENGTH_LONG
                    ).show()
                    return@Order
                } else {
                    orderResponse(it)
                }

            }).execute("POST", AppConfig.ORDER, json.toString())
        } else {
            Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.requestFocus()
        imm.showSoftInput(this, 0)
    }
}


