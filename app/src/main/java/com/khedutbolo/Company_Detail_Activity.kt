package com.khedutbolo

import android.content.Context
import com.khedutbolo.Adapter.Category_Adapter
import com.khedutbolo.Adapter.Company_Detail_Adapter
import com.khedutbolo.Model.Category_Model
import com.khedutbolo.Model.Company_Detail_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Crop_Adapter
import com.khedutbolo.Model.Crop_Model
import com.khedutbolo.Model.Solution_Model
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Company_Detail_Activity : AppCompatActivity() {

    lateinit var rv_company_detail : RecyclerView
    val company_detail = ArrayList<Company_Detail_Model>()
    lateinit var hud: KProgressHUD
    var category_name: String = ""
    lateinit var company_id: String
    lateinit var txt_nodata : LinearLayout
    var company_name: String = ""
    private var str_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company__detail_)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val i = intent
        company_id = i.getStringExtra("company_id")
        company_name = i.getStringExtra("company_name")

        if (company_name.equals("")){
            val actionbar = supportActionBar
            actionbar!!.title = resources.getString(R.string.companyproduct)
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }else{
            val actionbar = supportActionBar
            actionbar!!.title = company_name
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        rv_company_detail = findViewById(R.id.rv_company_detail) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_company_detail.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            json.put("company_id", company_id)
            json.put("user_id", str_id)
            Company_Product({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                    return@Company_Product
                } else {
                    companyproductResponse(it)
                }

            }).execute("POST", AppConfig.COMPANY_PRODUCT, json.toString())
        }

        rv_company_detail.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_company_detail,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext,AddtocartActivity::class.java)
                        intent.putExtra("product_id",company_detail.get(position).id)
                        intent.putExtra("technical_id","")
                        overridePendingTransition(0,0)
                        startActivity(intent)
                    }
                })
        )
    }

    class Company_Product(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun companyproductResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0){
            rv_company_detail.visibility = View.VISIBLE
            txt_nodata.visibility = View.GONE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                    company_detail.add(Company_Detail_Model(jsonObject.getString("id"), jsonObject.getString("name"),
                        jsonObject.getString("price"),
                        jsonObject.getString("sale_price"),
                        jsonObject.getString("company_name"),
                        jsonObject.getString("category_name"),
                        jsonObject.getString("cover"),"0"))

            }

            val adapter_company_detail = Company_Detail_Adapter(company_detail)
            rv_company_detail.adapter = adapter_company_detail
        }
        else{
            rv_company_detail.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
