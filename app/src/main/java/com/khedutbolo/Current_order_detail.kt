package com.khedutbolo

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Current_Order_Adapter
import com.khedutbolo.Model.Current_Order_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Current_order_detail : AppCompatActivity() {

    lateinit var rv_current: RecyclerView
    lateinit var l_recycler: LinearLayout
    lateinit var btn_cancel: Button
    private var str_id = ""
    lateinit var hud: KProgressHUD
    lateinit var txt_nodata: LinearLayout
    val current_order = ArrayList<Current_Order_Model>()
    private var order_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current_order_detail)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.orderdetail)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val i = intent
        order_id = i.getStringExtra("order_id")

        rv_current = findViewById(R.id.rv_current_order_list) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout
        btn_cancel = findViewById(R.id.btn_cancel_order) as Button

        btn_cancel.typeface = typeface_medium

        rv_current.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            json.put("user_id", str_id)
            json.put("order_id", order_id)
            currentorder({
                hud.dismiss();
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@currentorder
                } else {
                    currentorderResponse(it)
                }

            }).execute("POST", AppConfig.CURRENT_ORDER, json.toString())
        }

        rv_current.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_current,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, ProductDetailActivity::class.java)
                        intent.putExtra("image",current_order.get(position).image)
                        intent.putExtra("product_name",current_order.get(position).name)
                        intent.putExtra("sale_price",current_order.get(position).sale_price)
                        intent.putExtra("price",current_order.get(position).price)
                        intent.putExtra("qty",current_order.get(position).qty)
                        intent.putExtra("size",current_order.get(position).key + current_order.get(position).value)
                        intent.putExtra("shipping_price",current_order.get(position).shipping_price)
                        intent.putExtra("dealer",current_order.get(position).dealer)
                        intent.putExtra("total",current_order.get(position).total)
                        intent.putExtra("gst",current_order.get(position).gst)
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )

        btn_cancel.setOnClickListener{
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage(resources.getString(R.string.areyoudelete))

                .setCancelable(false)
                .setPositiveButton(resources.getString(R.string.areyouyes), DialogInterface.OnClickListener { dialog, id ->
                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    }else{
                        hud.show()
                        val json = JSONObject()
                        cancelorder({
                            hud.dismiss();
                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                                return@cancelorder
                            } else {
                                cancelorderResponse(it)
                            }

                        }).execute("GET", AppConfig.CANCEL_ORDER+order_id+"&user_id="+str_id, "")
                    }
                })
                .setNegativeButton(resources.getString(R.string.areyouno), DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })

            val alert = dialogBuilder.create()
            alert.setTitle(resources.getString(R.string.cancelorder))
            alert.show()

        }
    }

    class currentorder(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun currentorderResponse(response: String?) {

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0){
            rv_current.visibility = View.VISIBLE
            txt_nodata.visibility = View.GONE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {
                jsonObject = jsonArrayData.getJSONObject(i)

                current_order.add(Current_Order_Model(jsonObject.getString("order_id"), jsonObject.getString("product_name"),
                    jsonObject.getString("price"), jsonObject.getString("quantity")
                    , jsonObject.getString("sale_price") ,
                    jsonObject.getString("created_at"), jsonObject.getString("cover"),
                    jsonObject.getString("value")
                    , jsonObject.getString("key"), jsonObject.getString("shipping_price"),
                    jsonObject.getString("Dealer"),
                    jsonObject.getString("gst_price"),
                    jsonObject.getString("total")))
            }

            val adapter_current = Current_Order_Adapter(current_order)
            rv_current.adapter = adapter_current
        }
        else{
            rv_current.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class cancelorder(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cancelorderResponse(response: String?) {

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        Toast.makeText(applicationContext, "Cancel Order Successfully", Toast.LENGTH_LONG).show()

        val intent = Intent(applicationContext,Current_Order_Activity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent)
        overridePendingTransition(0,0)
        finish()
    }
}
