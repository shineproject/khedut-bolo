package com.khedutbolo

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.*
import com.khedutbolo.Model.*
import com.khedutbolo.util.AppConfig
import kotlinx.android.synthetic.main.activity_addtocart.*
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.annotation.IntegerRes
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import androidx.core.widget.NestedScrollView
import androidx.drawerlayout.widget.DrawerLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr


class AddtocartActivity : AppCompatActivity() {

    lateinit var rv_about_product: RecyclerView
    lateinit var rv_similar_product: RecyclerView
    lateinit var spinner_item: Spinner
    lateinit var img_spinner: ImageView
    lateinit var img_minus: ImageView
    lateinit var img_plus: ImageView
    lateinit var txt_value: TextView
    lateinit var btn_buy: Button
    lateinit var add_cart: Button
    lateinit var call_order: Button
    internal var value = 1
    lateinit var txt_product_title: TextView
    lateinit var txt_dealer: TextView
    lateinit var txt_seller: TextView
    lateinit var txt_product_size: TextView
    lateinit var txt_quantity: TextView
    lateinit var txt_selling_price: TextView
    lateinit var txt_total_price: TextView
    lateinit var txt_shipping_price: TextView
    lateinit var sell_price: TextView
    lateinit var total_price: TextView
    lateinit var shipping_price: TextView
    lateinit var txt_title_about: TextView
    lateinit var txt_title_technical: TextView
    lateinit var txt_value_admin: TextView
    lateinit var txt_title_similar: TextView
    lateinit var txt_offer: TextView
    lateinit var txt_offermaintitle: TextView
    lateinit var txt_offerproduct: TextView
    lateinit var txt_offerquantity: TextView
    lateinit var img_offer: ImageView
    lateinit var l_offer: LinearLayout
    lateinit var product_id: String
    var technical_id: String = ""
    lateinit var sell_price_data: String
    lateinit var hud: KProgressHUD
    val similar_product = ArrayList<Similar_Product_Model>()
    private var str_id = ""
    private var str_quantity = "1"
    private var str_total_price = ""
    private var shipping_data = ""
    val productsize = ArrayList<Product_Size_Model>()
    val about_product = ArrayList<About_Product_Model>()
    private var str_size = ""
    lateinit var l_about: LinearLayout
    lateinit var l_similar: LinearLayout
    lateinit var img_product: ImageView
    private var available_qty = 0
    private var check_qty = ""
    private var attribute_id = ""
    private var select_quantity = 0
    private var total_quantity = 0
    private var all_quantity = 0
    internal var adapter_size: Product_Size_Adapter? = null
    internal var adapter_about_product: About_Product_Adapter? = null
    internal var adapter_similar_product: Similar_Product_Adapter? = null
    lateinit var scroll: NestedScrollView
    private var str_total_point = ""
    private var str_total_amount = ""
    private var gst_price = ""
    private var total_payment = ""
    private var select_add = ""
    private var number_call = ""

    lateinit var item  : MenuItem
    lateinit var actionView   : View
    val PERMISSIONS_REQUEST_PHONE_CALL = 1
    companion object{
        lateinit var tv  : TextView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addtocart)

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = resources.getString(R.string.productdetail)
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        DashBoardActivity.add_cart = "1"

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val i = intent
        product_id = i.getStringExtra("product_id")
        technical_id = i.getStringExtra("technical_id")

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_regular = Typeface.createFromAsset(assets, "poppinsregular.otf")

        rv_about_product = findViewById(R.id.rv_about_product) as RecyclerView
        rv_similar_product = findViewById(R.id.rv_similar_product) as RecyclerView
        spinner_item = findViewById(R.id.spinner) as Spinner
        img_spinner = findViewById(R.id.img_spinnerproduct) as ImageView
        img_minus = findViewById(R.id.img_minus) as ImageView
        img_plus = findViewById(R.id.img_plus) as ImageView
        call_order = findViewById(R.id.btn_call_order) as Button
        btn_buy = findViewById(R.id.btn_buynow) as Button
        add_cart = findViewById(R.id.btn_add_cart) as Button
        txt_value = findViewById(R.id.txtvalue) as TextView
        txt_product_title = findViewById(R.id.txt_product) as TextView
        txt_dealer = findViewById(R.id.txt_dealer) as TextView
        txt_seller = findViewById(R.id.txt_seller) as TextView
        txt_product_size = findViewById(R.id.txt_product_size) as TextView
        txt_quantity = findViewById(R.id.txt_quantity) as TextView
        txt_selling_price = findViewById(R.id.txt_selling_price) as TextView
        txt_total_price = findViewById(R.id.txt_total_price) as TextView
        txt_shipping_price = findViewById(R.id.txt_shipping_price) as TextView
        sell_price = findViewById(R.id.sell_price) as TextView
        total_price = findViewById(R.id.total_price) as TextView
        shipping_price = findViewById(R.id.shipping_price) as TextView
        txt_title_about = findViewById(R.id.txt_title_about) as TextView
        txt_title_technical = findViewById(R.id.txt_technical) as TextView
        txt_value_admin = findViewById(R.id.txt_value_admin) as TextView
        txt_title_similar = findViewById(R.id.txt_title_similar) as TextView
        img_product = findViewById(R.id.img_product) as ImageView
        l_about = findViewById(R.id.l_about) as LinearLayout
        l_similar = findViewById(R.id.l_similar) as LinearLayout
        scroll = findViewById(R.id.scroll) as NestedScrollView
        txt_offer = findViewById(R.id.txt_offer) as TextView
        txt_offermaintitle = findViewById(R.id.txt_mainoffertitle) as TextView
        txt_offerproduct = findViewById(R.id.txt_offerproduct) as TextView
        txt_offerquantity = findViewById(R.id.txt_offerquantity) as TextView
        l_offer = findViewById(R.id.l_offer) as LinearLayout
        img_offer = findViewById(R.id.offer_image) as ImageView

        txt_value.typeface = typeface_medium
        txt_product_title.typeface = typeface_medium
        txt_dealer.typeface = typeface_regular
        txt_seller.typeface = typeface_regular
        txt_product_size.typeface = typeface_medium
        txt_quantity.typeface = typeface_medium
        txt_selling_price.typeface = typeface_medium
        txt_total_price.typeface = typeface_medium
        txt_shipping_price.typeface = typeface_medium
        sell_price.typeface = typeface_medium
        total_price.typeface = typeface_medium
        shipping_price.typeface = typeface_medium
        btn_buy.typeface = typeface_medium
        add_cart.typeface = typeface_medium
        call_order.typeface = typeface_medium
        txt_title_about.typeface = typeface_medium
        txt_title_technical.typeface = typeface_medium
        txt_value_admin.typeface = typeface_medium
        txt_title_similar.typeface = typeface_medium
        txt_offer.typeface = typeface_medium
        txt_offermaintitle.typeface = typeface_medium
        txt_offerproduct.typeface = typeface_medium
        txt_offerquantity.typeface = typeface_medium

        rv_about_product.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_similar_product.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            json.put("id", product_id)
            json.put("customer_id", str_id)
            json.put("user_id", str_id)
            productDetail({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@productDetail
                } else {
                    productdetailResponse(it)
                }

            }).execute("POST", AppConfig.PRODUCT_DETAIL, json.toString())
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            json.put("id", product_id)
            json.put("user_id", str_id)
            json.put("technical_id", technical_id)
            Similar_Product({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Similar_Product
                } else {
                    similarproductResponse(it)
                }

            }).execute("POST", AppConfig.SIMILAR_PRODUCT, json.toString())
        }

        spinner_item.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_size = productsize.get(position).value + productsize.get(position).key
                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    hud.show()
                    val json = JSONObject()
                    json.put("customer_id", str_id)
                    json.put("product_id", product_id)
                    json.put("attribute_id", productsize.get(position).id)
                    json.put("user_id", str_id)
                    AttributeDetail({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                                .show()
                            return@AttributeDetail
                        } else {
                            attributeResponse(it)
                        }

                    }).execute("POST", AppConfig.PRODUCT_ATTRIBUTE, json.toString())
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        rv_similar_product.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_similar_product,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        scroll.fullScroll(View.FOCUS_UP);
                        if (!isNetworkAvailable()) {
                            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                        } else {
                            hud.show()
                            val json = JSONObject()
                            json.put("id", similar_product.get(position).id)
                            json.put("customer_id", str_id)
                            json.put("user_id", str_id)
                            productDetail({
                                hud.dismiss();
                                product_id = similar_product.get(position).id
                                productsize.clear()
                                about_product.clear()
                                similar_product.clear()

                                if (adapter_size != null) {
                                    adapter_size!!.notifyDataSetChanged()
                                }
                                if (adapter_about_product != null) {
                                    adapter_about_product!!.notifyDataSetChanged()
                                }
                                if (adapter_similar_product != null) {
                                    adapter_similar_product!!.notifyDataSetChanged()
                                }

                                if (it == null) {
                                    Toast.makeText(
                                        applicationContext,
                                        resources.getString(R.string.connectionerror),
                                        Toast.LENGTH_LONG
                                    ).show()
                                    return@productDetail
                                } else {
                                    productdetailResponse(it)
                                }

                            }).execute("POST", AppConfig.PRODUCT_DETAIL, json.toString())
                        }

                        if (!isNetworkAvailable()) {
                            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                        } else {
                            hud.show()
                            val json = JSONObject()
                            json.put("id", similar_product.get(position).id)
                            json.put("user_id", str_id)
                            Similar_Product({
                                hud.dismiss();

                                if (it == null) {
                                    Toast.makeText(
                                        applicationContext,
                                        resources.getString(R.string.connectionerror),
                                        Toast.LENGTH_LONG
                                    ).show()
                                    return@Similar_Product
                                } else {
                                    similarproductResponse(it)
                                }

                            }).execute("POST", AppConfig.SIMILAR_PRODUCT, json.toString())
                        }

                    }
                })
        )


        img_spinner.setOnClickListener {
            spinner_item.performClick()
        }

        btn_add_cart.setOnClickListener {
            if (!isNetworkAvailable()) {
                Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
            } else {
                if (txt_value.text.toString().trim().equals("0")){
                    Toast.makeText(applicationContext, resources.getString(R.string.productnot), Toast.LENGTH_LONG).show()
                }else{
                    hud.show()
                    val json = JSONObject()
                    json.put("customer_id", str_id)
                    json.put("product_id", product_id)
                    json.put("quantity", str_quantity)
                    json.put("attribute_id", attribute_id)
                    json.put("total_price", str_total_price)
                    json.put("user_id", str_id)
                    Add_Cart({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@Add_Cart
                        } else {
                            addcartResponse(it)
                        }

                    }).execute("POST", AppConfig.ADD_CART, json.toString())
                }

            }
        }

        btn_buy.setOnClickListener {
            if (!isNetworkAvailable()) {
                Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
            } else {
                if (txt_value.text.toString().trim().equals("0")){
                    Toast.makeText(applicationContext, "Product not available", Toast.LENGTH_LONG).show()
                }else{
                    hud.show()
                    val json = JSONObject()
                    Buy_Cart({
                        hud.dismiss();

                        if (it == null) {
                            Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                                .show()
                            return@Buy_Cart
                        } else {
                            buycartResponse(it)
                        }

                    }).execute("GET", AppConfig.BUY_NOW+str_id+"&product_id="+product_id+"&quantity="+str_quantity+"&attribute_id="+attribute_id+"&total_price="+str_total_price+"&user_id="+str_id, json.toString())
                }
            }
        }

        call_order.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CALL_PHONE) !== PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    PERMISSIONS_REQUEST_PHONE_CALL
                )
            } else {
                if (number_call.equals("")){
                    Toast.makeText(applicationContext, "Number not availble", Toast.LENGTH_LONG).show()
                }else{
                    val phone = number_call
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:+91$phone")
                    startActivity(intent)
                }
                //Open call function

            }

        }

        img_minus.setOnClickListener {
            if (value == 1) {
            } else {
                if (check_qty.equals("")){
                    check_qty = "1"
                    value = all_quantity
                }
                if (txt_value.text.toString().trim().equals("0")){
                    Toast.makeText(applicationContext, "Product not available", Toast.LENGTH_LONG).show()
                }else{
                    value--
                    txt_value.setText(value.toString())
                    str_quantity = value.toString()

                    val price_multi = value * sell_price_data.toDouble()
                    val add_shipping = price_multi + shipping_data.toDouble()
                    str_total_price = String.format("%.2f", add_shipping)
                    total_price.setText("₹ " + str_total_price.toString())
                }
            }
        }

        img_plus.setOnClickListener {
            if (total_quantity == 0) {
                Toast.makeText(applicationContext, "Item not available", Toast.LENGTH_LONG).show()
            } else if (value == available_qty) {
                 Toast.makeText(applicationContext, "Product not available", Toast.LENGTH_LONG).show()
            } else {
                if (check_qty.equals("")){
                    check_qty = "1"
                    value = all_quantity
                }
                if (txt_value.text.toString().trim().equals("0")){
                    Toast.makeText(applicationContext, "Product not available", Toast.LENGTH_LONG).show()
                }else{
                    value++
                    txt_value.setText(value.toString())
                    str_quantity = value.toString()

                    val price_multi = value * sell_price_data.toDouble()
                    val add_shipping = price_multi + shipping_data.toDouble()
                    str_total_price = String.format("%.2f", add_shipping)
                    total_price.setText("₹ " + str_total_price.toString())
                }
            }
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            Cartitem({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Cartitem
                } else {
                    cartitemResponse(it)
                }

            }).execute("GET", AppConfig.CART_COUNT + str_id+"&user_id="+str_id, json.toString())
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.product_menu, menu)

        item = menu!!.findItem(R.id.action_cart)
        actionView = MenuItemCompat.getActionView(item);
        tv = actionView.findViewById(R.id.txtCount);
        tv.setText("0");

        actionView.setOnClickListener{
            val intent = Intent(this, Shopping_Cart_Activity::class.java)
            intent.putExtra("check_product","")
            intent.putExtra("product_id","")
            overridePendingTransition(0, 0)
            startActivity(intent)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_cart -> {
                val intent = Intent(this, Shopping_Cart_Activity::class.java)
                intent.putExtra("check_product","1")
                intent.putExtra("product_id",product_id)
                overridePendingTransition(0,0)
                startActivity(intent)
                finish()

                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    class productDetail(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

            var callback = callback

            override fun doInBackground(vararg params: String): String? {
                val url = URL(params[1])
                val httpClient = url.openConnection() as HttpURLConnection
                httpClient.setReadTimeout(10000)
                httpClient.setConnectTimeout(10000)
                httpClient.requestMethod = params[0]

                if (params[0] == "POST") {
                    httpClient.instanceFollowRedirects = false
                    httpClient.doOutput = true
                    httpClient.doInput = true
                    httpClient.useCaches = false
                    httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
                }
                try {
                    if (params[0] == "POST") {
                        httpClient.connect()
                        val os = httpClient.getOutputStream()
                        val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                        writer.write(params[2])
                        writer.flush()
                        writer.close()
                        os.close()
                    }
                    if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                        val stream = BufferedInputStream(httpClient.inputStream)
                        val data: String = readStream(inputStream = stream)
                        return data
                    } else {
                        println("ERROR ${httpClient.responseCode}")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    httpClient.disconnect()
                }

                return null
            }

            fun readStream(inputStream: BufferedInputStream): String {
                val bufferedReader = BufferedReader(InputStreamReader(inputStream))
                val stringBuilder = StringBuilder()
                bufferedReader.forEachLine { stringBuilder.append(it) }
                return stringBuilder.toString()
            }

            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                callback(result)
            }
        }

    fun productdetailResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")
        number_call = jsonObj.getString("mobile")

        if (data.equals("[]")) {
            Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
        } else {
            val jsonData = JSONObject(data)
            val id = jsonData.getString("id")
            val name = jsonData.getString("name")
            val company_name = jsonData.getString("company_name")
            val mobile = jsonData.getString("mobile")
            val status = jsonData.getString("status")
            val weight = jsonData.getString("weight")
            shipping_data = jsonData.getString("shipping_price")
            val cover = jsonData.getString("cover")
            val offer = jsonData.getString("offer")

            Glide.with(this)
                .load(cover)
                .error(R.mipmap.width_product)
                .into(img_product)

            txt_product_title.setText(name)
            txt_dealer.setText(resources.getString(R.string.by) + company_name)
            shipping_price.setText("₹ " + shipping_data)

            val jsonArrayAdditional = jsonData.getJSONArray("additional_info")

            if (jsonArrayAdditional.length() > 0) {
                l_about.visibility = View.VISIBLE
                txt_title_about.visibility = View.VISIBLE
                var jsonObject: JSONObject? = null

                for (i in 0 until jsonArrayAdditional.length()) {

                    jsonObject = jsonArrayAdditional.getJSONObject(i)
                    about_product.add(About_Product_Model(jsonObject.getString("key"), jsonObject.getString("value")))

                }

                adapter_about_product = About_Product_Adapter(about_product)
                rv_about_product.adapter = adapter_about_product

            } else {
                l_about.visibility = View.GONE
                txt_title_about.visibility = View.GONE
            }

            val jsonArrayData = jsonData.getJSONArray("unit")
            try {
                if (jsonArrayData.length() > 0) {
                    var jsonObject: JSONObject? = null

                    for (i in 0 until jsonArrayData.length()) {

                        jsonObject = jsonArrayData.getJSONObject(i)
                        productsize.add(Product_Size_Model(jsonObject.getString("key"), jsonObject.getString("value"),
                            jsonObject.getString("quantity"),
                            jsonObject.getString("price"), jsonObject.getString("sale_price"),
                            jsonObject.getString("available_quantity"), jsonObject.getString("id"),
                            jsonObject.getString("user_quantity")))

                        if (i == 0){
                            select_quantity = Integer.valueOf(jsonObject.getString("quantity"))
                            available_qty = Integer.valueOf(jsonObject.getString("available_quantity"))
                            total_quantity = select_quantity + available_qty

                            attribute_id = jsonObject.getString("id")

                            all_quantity = Integer.valueOf(jsonObject.getString("user_quantity"))

                            if (all_quantity.toString().equals("0")){
                                value = 1
                                txt_value.setText(value.toString())
                            }else{
                                value = all_quantity
                                txt_value.setText(value.toString())
                            }

                            str_quantity = value.toString()

                            sell_price.setText("₹ " + jsonObject.getString("sale_price"))
                            sell_price_data = jsonObject.getString("sale_price")
                            str_total_price = sell_price_data


                            val price_multi = value * sell_price_data.toDouble()
                            val add_shipping = price_multi + shipping_data.toDouble()
                            str_total_price = String.format("%.2f", add_shipping)
                            if (available_qty.toString().equals("0")){
                                value = 0
                                total_price.setText("₹ " + "0.00")
                                txt_value.setText(value.toString())
                            }else{
                                total_price.setText("₹ " + str_total_price.toString())
                            }
                        }

                    }

                    adapter_size = Product_Size_Adapter(this, productsize)
                    spinner_item.adapter = adapter_size

                } else {
                    Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
                }
            }catch (e: Exception) {
                e.printStackTrace()
            }

            if (offer.equals("null")){
                l_offer.visibility = View.GONE
            }else{
                l_offer.visibility = View.VISIBLE

                val jsonOffer = JSONObject(offer)
                val offer_title = jsonOffer.getString("offer_title")
                val offer_product = jsonOffer.getString("offer_product")
                val offer_quantity = jsonOffer.getString("offer_quantity")
                val offer_product_image = jsonOffer.getString("offer_product_image")

                txt_offermaintitle.setText(offer_title)
                txt_offerproduct.setText(offer_product)
                txt_offerquantity.setText(resources.getString(R.string.str_quant)+offer_quantity)

                Glide.with(this)
                    .load(offer_product_image)
                    .error(R.mipmap.test_product)
                    .into(img_offer)

            }
        }
    }

    class Similar_Product(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun similarproductResponse(response: String?) {

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {
            l_similar.visibility = View.VISIBLE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                similar_product.add(
                    Similar_Product_Model(
                        jsonObject.getString("id"), jsonObject.getString("name"), jsonObject.getString("price"),
                        jsonObject.getString("company_id"), jsonObject.getString("product_dealer"),
                        jsonObject.getString("cover"), jsonObject.getString("sale_price"), jsonObject.getString("technical_name")
                    )
                )

            }

            adapter_similar_product = Similar_Product_Adapter(similar_product)
            rv_similar_product.adapter = adapter_similar_product



        } else {
            l_similar.visibility = View.GONE
        }
    }

    class Add_Cart(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun addcartResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        Toast.makeText(applicationContext, resources.getString(R.string.productadd), Toast.LENGTH_LONG).show()

        if (data.equals("[]")){
            tv.setText("0")
            DashBoardActivity.tv.setText("0")
            if (DashBoardActivity.select_solution.equals("")){

            }else{
                Select_Solution_Activity.tv.setText("0")
            }

            if (DashBoardActivity.trending.equals("")){

            }else{
                Trending_Product_Activity.tv.setText("0")
            }
        }else{
            if (data.toInt() > 99){
                var data1 = "99+"
                tv.setText(data1)
                DashBoardActivity.tv.setText(data1)
                if (DashBoardActivity.select_solution.equals("")){

                }else{
                    Select_Solution_Activity.tv.setText(data1)
                }

                if (DashBoardActivity.trending.equals("")){

                }else{
                    Trending_Product_Activity.tv.setText(data1)
                }
            }else{
                tv.setText(data)
                DashBoardActivity.tv.setText(data)
                if (DashBoardActivity.select_solution.equals("")){

                }else{
                    Select_Solution_Activity.tv.setText(data)
                }

                if (DashBoardActivity.trending.equals("")){

                }else{
                    Trending_Product_Activity.tv.setText(data)
                }
            }

        }
    }

    class Buy_Cart(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun buycartResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (data.length > 0) {
            val jsonData = JSONObject(data)
            str_total_point = jsonData.getString("total_point")
            str_total_amount = jsonData.getString("total_wallet")
            total_payment = jsonData.getString("total_order_price")
            gst_price = jsonData.getString("gst_price")
        }

        val intent = Intent(applicationContext, Checkout_Activity::class.java)
        intent.putExtra("total_amount", total_payment)
        intent.putExtra("total_point", str_total_point)
        intent.putExtra("total_wallet_amount", str_total_amount)
        intent.putExtra("gst_price", gst_price)
        overridePendingTransition(0, 0)
        startActivity(intent)
    }

    class AttributeDetail(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun attributeResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        try{
            if (data.equals("[]")){
                Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
            }else{
                select_add = "1"
                val jsonData = JSONObject(data)

                val id = jsonData.getString("id")
                val product_id = jsonData.getString("product_id")
                val key = jsonData.getString("key")
                val value1 = jsonData.getString("value")
                val price = jsonData.getString("price")
                val sale_price = jsonData.getString("sale_price")
                val quantity = jsonData.getString("quantity")
                val available_quantity = jsonData.getString("available_quantity")
                val user_quantity = jsonData.getString("user_quantity")

                attribute_id = id

                all_quantity = Integer.valueOf(jsonData.getString("user_quantity"))

                if (all_quantity.toString().equals("0")){
                    value = 1
                    txt_value.setText(value.toString())
                }else{
                    value = all_quantity
                    txt_value.setText(value.toString())
                }

                str_quantity = value.toString()

                select_quantity = Integer.valueOf(quantity)
                available_qty = Integer.valueOf(available_quantity)
                total_quantity = select_quantity + available_qty

                if (select_add.equals("1")){
                    sell_price.setText("₹ " + sale_price)
                    total_price.setText("₹ " + sale_price)
                    sell_price_data = sale_price


                    str_total_price = sell_price_data

                    val price_multi = value * sell_price_data.toDouble()
                    val add_shipping = price_multi + shipping_data.toDouble()
                    str_total_price = String.format("%.2f", add_shipping)
                    if (available_qty.toString().equals("0")){
                        value = 0
                        total_price.setText("₹ " + "0.00")
                        txt_value.setText(value.toString())
                    }else{
                        total_price.setText("₹ " + str_total_price.toString())
                    }
                }
            }
        }catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class Cartitem(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cartitemResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")){
            if (data.equals("[]")){
                tv.setText("0")
                DashBoardActivity.tv.setText("0")
                if (DashBoardActivity.select_solution.equals("")){

                }else{
                    Select_Solution_Activity.tv.setText("0")
                }

                if (DashBoardActivity.trending.equals("")){

                }else{
                    Trending_Product_Activity.tv.setText("0")
                }
            }else{
                if (data.toInt() > 99){
                    var data1 = "99+"
                    tv.setText(data1)
                    DashBoardActivity.tv.setText(data1)
                    if (DashBoardActivity.select_solution.equals("")){

                    }else{
                        Select_Solution_Activity.tv.setText(data1)
                    }

                    if (DashBoardActivity.trending.equals("")){

                    }else{
                        Trending_Product_Activity.tv.setText(data1)
                    }
                }else{
                    tv.setText(data)
                    DashBoardActivity.tv.setText(data)
                    if (DashBoardActivity.select_solution.equals("")){

                    }else{
                        Select_Solution_Activity.tv.setText(data)
                    }

                    if (DashBoardActivity.trending.equals("")){

                    }else{
                        Trending_Product_Activity.tv.setText(data)
                    }
                }

            }
        }
    }
}
