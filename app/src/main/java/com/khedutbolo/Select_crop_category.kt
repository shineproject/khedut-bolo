package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Crop_Adapter
import com.khedutbolo.Adapter.Crop_Cat_Adapter
import com.khedutbolo.Model.Crop_Cat_Model
import com.khedutbolo.Model.Crop_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Select_crop_category : AppCompatActivity() {

    lateinit var rv_crop : RecyclerView
    lateinit var txt_nodata : LinearLayout
    lateinit var hud: KProgressHUD
    lateinit var solution_id: String
    val crop = ArrayList<Crop_Cat_Model>()
    var solution_name: String = ""
    private var str_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_crop_category)

        val i = intent
        solution_id = i.getStringExtra("solution_id")
        solution_name = i.getStringExtra("solution_name")

        if (solution_name.equals("")){
            val actionbar = supportActionBar
            actionbar!!.title = resources.getString(R.string.selectcrop)
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }else{
            val actionbar = supportActionBar
            actionbar!!.title = solution_name
            actionbar.setDisplayHomeAsUpEnabled(true)
            actionbar.setDisplayHomeAsUpEnabled(true)
        }

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)


        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        rv_crop = findViewById(R.id.rv_crop_category) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_crop.layoutManager = GridLayoutManager(this, 2)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            json.put("id", solution_id)
            json.put("user_id", str_id)
            Crop_Solution({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(
                        applicationContext,
                        resources.getString(R.string.connectionerror),
                        Toast.LENGTH_LONG
                    ).show()
                    return@Crop_Solution
                } else {
                    cropsolutionResponse(it)
                }

            }).execute("POST", AppConfig.CROP_CATEGORY, json.toString())
        }

        rv_crop.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_crop,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext,Select_Solution_Activity::class.java)
                        intent.putExtra("solution_id",crop.get(position).id)
                        intent.putExtra("solution_name",crop.get(position).name)
                        intent.putExtra("crop_name",solution_name)
                        intent.putExtra("subsolution_id",solution_id)
                        overridePendingTransition(0,0)
                        startActivity(intent)
                    }
                })
        )
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    class Crop_Solution(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cropsolutionResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0){
            rv_crop.visibility = View.VISIBLE
            txt_nodata.visibility = View.GONE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                crop.add(Crop_Cat_Model(jsonObject.getString("image"), jsonObject.getString("name"), jsonObject.getString("id")))

            }

            val adapter_crop = Crop_Cat_Adapter(crop)
            rv_crop.adapter = adapter_crop
        }
        else{
            rv_crop.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }


    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
