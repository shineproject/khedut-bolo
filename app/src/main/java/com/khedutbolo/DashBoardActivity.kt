package com.khedutbolo

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import androidx.core.widget.NestedScrollView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Companyadapter
import com.khedutbolo.Adapter.CustomAdapter
import com.khedutbolo.Adapter.TrendingAdapter
import com.khedutbolo.Adapter.ViewPagerAdapter
import com.khedutbolo.Model.*
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList


class DashBoardActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    internal lateinit var viewpager: ViewPager
    lateinit var rv_product: RecyclerView
    lateinit var rv_tranding: RecyclerView
    lateinit var rv_company: RecyclerView
    lateinit var img_insects: ImageView
    lateinit var img_disease: ImageView
    lateinit var img_condemnation: ImageView
    lateinit var txt_trending_more: TextView
    lateinit var txt_company_all: TextView
    lateinit var txt_title_solution: TextView
    lateinit var txt_insects: TextView
    lateinit var txt_disease: TextView
    lateinit var txt_condemnation: TextView
    lateinit var txt_title_product: TextView
    lateinit var txt_title_trending: TextView
    lateinit var txt_title_company: TextView
    lateinit var sliderItems: MutableList<SliderItem>
    internal var firstCurrentItem = 0
    var active = 0
    internal var itemPosition = 0
    lateinit var hud: KProgressHUD
    lateinit var txt_insects_id: TextView
    lateinit var txt_disease_id: TextView
    lateinit var txt_condemnation_id: TextView
    lateinit var close_image: ImageView
    lateinit var Edit_search: EditText
    private var str_id = ""
    lateinit var l_product: LinearLayout
    lateinit var l_trending: LinearLayout
    lateinit var l_company: LinearLayout
    lateinit var l_row: LinearLayout
    lateinit var l_solution: LinearLayout

    val trending = ArrayList<TrandingModel>()
    val company = ArrayList<CompanyModel>()
    private var check_category = ""
    private var check_company = ""
    private var check_trending = ""
    internal var adapter_product: CustomAdapter? = null
    private lateinit var searchView: SearchView
    lateinit var r_offer: LinearLayout

    private var currentPage = 0
    private var NUM_PAGES = 0
    var company_all_check = ""
    var trending_more_check = ""
    lateinit var scroll: NestedScrollView
    private var str_language = ""
    val PERMISSIONS_REQUEST_PHONE_CALL = 1
    private var number_call = ""

    companion object {
        var array_trending = ""
        var array_company = ""
        var select_solution = ""
        var trending = ""
        var add_cart = ""
        val productdata = ArrayList<ProductModel>()
        lateinit var tv: TextView
        lateinit var item: MenuItem
        lateinit var actionView: View

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dash_board)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = resources.getString(R.string.app_name)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.CALL_PHONE) !== PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                    arrayOf(Manifest.permission.CALL_PHONE),
                    PERMISSIONS_REQUEST_PHONE_CALL
                )
            } else {
                if (number_call.equals("")){
                    Snackbar.make(view, "Number not availble", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show()
                }else{
                    val phone = number_call
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:+91$phone")
                    startActivity(intent)
                }
                //Open call function

            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val sharedPreferences: SharedPreferences = this.getSharedPreferences("language", Context.MODE_PRIVATE)
        str_language = sharedPreferences.getString("lan_name", "")!!

        if (str_language.equals("")){

        }else{
            val locale = Locale(str_language)
            val res = resources
            val dm = res.displayMetrics
            val conf = res.configuration
            conf.locale = locale
            res.updateConfiguration(conf, dm)
        }


        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_semibold = Typeface.createFromAsset(assets, "poppinssemiBold.otf")

        viewpager = findViewById(R.id.viewPager) as ViewPager
        rv_product = findViewById(R.id.rv_product_list) as RecyclerView
        rv_tranding = findViewById(R.id.rv_tranding) as RecyclerView
        rv_company = findViewById(R.id.rv_company_list) as RecyclerView
        img_insects = findViewById(R.id.img_insects) as ImageView
        img_disease = findViewById(R.id.img_disease) as ImageView
        img_condemnation = findViewById(R.id.img_condemnation) as ImageView
        txt_trending_more = findViewById(R.id.txt_trending_more) as TextView
        txt_company_all = findViewById(R.id.txt_company_all) as TextView
        txt_title_solution = findViewById(R.id.txt_solution) as TextView
        txt_insects = findViewById(R.id.txt_insects) as TextView
        txt_disease = findViewById(R.id.txt_disease) as TextView
        txt_condemnation = findViewById(R.id.txt_condemnation) as TextView
        txt_title_product = findViewById(R.id.txt_title_product) as TextView
        txt_title_trending = findViewById(R.id.txt_title_tranding) as TextView
        txt_title_company = findViewById(R.id.txt_title_companylist) as TextView
        txt_insects_id = findViewById(R.id.txt_insects_id) as TextView
        txt_disease_id = findViewById(R.id.txt_disease_id) as TextView
        txt_condemnation_id = findViewById(R.id.txt_condemnation_id) as TextView
        l_product = findViewById(R.id.l_product) as LinearLayout
        l_trending = findViewById(R.id.l_trending) as LinearLayout
        l_company = findViewById(R.id.l_company) as LinearLayout
        l_row = findViewById(R.id.l_row) as LinearLayout
        r_offer = findViewById(R.id.r_offer) as LinearLayout
        l_solution = findViewById(R.id.r_solution) as LinearLayout
        scroll = findViewById(R.id.scroll) as NestedScrollView


        txt_trending_more.typeface = typeface_medium
        txt_company_all.typeface = typeface_medium
        txt_title_solution.typeface = typeface_medium
        txt_insects.typeface = typeface_medium
        txt_disease.typeface = typeface_medium
        txt_condemnation.typeface = typeface_medium
        txt_title_product.typeface = typeface_medium
        txt_title_trending.typeface = typeface_medium
        txt_title_company.typeface = typeface_medium

        sliderItems = ArrayList()

        rv_product.layoutManager = GridLayoutManager(this, 2)
        rv_tranding.layoutManager = GridLayoutManager(this, 2)
        rv_company.layoutManager = GridLayoutManager(this, 2)

        if (productdata.isEmpty()) {

        } else {
            productdata.clear()
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            val json = JSONObject()
            json.put("name", "check")
            json.put("user_id", str_id)
            Solution({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Solution
                } else {
                    cropResponse(it)
                }

            }).execute("POST", AppConfig.CROP, json.toString())
        }

        if (!isNetworkAvailable()) {
        } else {
            val json = JSONObject()
            json.put("user_id", str_id)
            Offer({
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Offer
                } else {
                    offerResponse(it)
                }

            }).execute("POST", AppConfig.OFFER, json.toString())
        }

        if (!isNetworkAvailable()) {
        } else {
            val json = JSONObject()
            product({
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@product
                } else {
                    productResponse(it)
                }

            }).execute("GET", AppConfig.PRODUCT_CATEGORY+str_id, json.toString())
        }

        if (!isNetworkAvailable()) {
        } else {
            hud.show()
            val json = JSONObject()
            Cartitem({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Cartitem
                } else {
                    cartitemResponse(it)
                }

            }).execute("GET", AppConfig.CART_COUNT + str_id+"&user_id="+str_id, json.toString())
        }

        rv_product.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_product,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, SubCategoryActivity::class.java)
                        intent.putExtra("category_id", productdata.get(position).id)
                        intent.putExtra("category_name", productdata.get(position).name)
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )

        rv_tranding.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_tranding,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, AddtocartActivity::class.java)
                        intent.putExtra("product_id", trending.get(position).id)
                        intent.putExtra("technical_id","")
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )

        rv_company.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_company,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, Company_Detail_Activity::class.java)
                        intent.putExtra("company_id", company.get(position).id)
                        intent.putExtra("company_name", company.get(position).name)
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )

        img_insects.setOnClickListener {
            val intent = Intent(this@DashBoardActivity, Select_Crop_Activity::class.java)
            intent.putExtra("crop_id", txt_insects_id.text.toString())
            intent.putExtra("crop_name", txt_insects.text.toString())
            startActivity(intent)
        }

        img_disease.setOnClickListener {
            val intent = Intent(this@DashBoardActivity, Select_Crop_Activity::class.java)
            intent.putExtra("crop_id", txt_disease_id.text.toString())
            intent.putExtra("crop_name", txt_disease.text.toString())
            startActivity(intent)
        }

        img_condemnation.setOnClickListener {
            val intent = Intent(this@DashBoardActivity, Select_Crop_Activity::class.java)
            intent.putExtra("crop_id", txt_condemnation_id.text.toString())
            intent.putExtra("crop_name", txt_condemnation.text.toString())
            startActivity(intent)
        }

        txt_trending_more.setOnClickListener {
            if (trending.isEmpty()) {
                Toast.makeText(applicationContext, "No data found", Toast.LENGTH_LONG).show()
            } else {
                startActivity(Intent(this, Trending_Product_Activity::class.java))
                overridePendingTransition(0, 0)
            }

        }

        txt_company_all.setOnClickListener {
            if (company.isEmpty()) {
                Toast.makeText(applicationContext, "No data found", Toast.LENGTH_LONG).show()
            } else {
                startActivity(Intent(this, Company_List_Activity::class.java))
                overridePendingTransition(0, 0)
            }

        }

    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.dash_board, menu)

        item = menu!!.findItem(R.id.action_cart)
        actionView = MenuItemCompat.getActionView(item);
        tv = actionView.findViewById(R.id.txtCount);
        tv.setText("0");

        actionView.setOnClickListener {
            val intent = Intent(this, Shopping_Cart_Activity::class.java)
            intent.putExtra("check_product", "")
            intent.putExtra("product_id", "")
            overridePendingTransition(0, 0)
            startActivity(intent)
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_notification -> {
                startActivity(Intent(this, Notification_Activity::class.java))
                overridePendingTransition(0, 0)
                return true
            }
            R.id.action_search -> {
                    startActivity(Intent(this, Searchactivity::class.java))
                    overridePendingTransition(0, 0)
                return true
            }
            R.id.action_cart -> {
                val intent = Intent(this, Shopping_Cart_Activity::class.java)
                intent.putExtra("check_product", "")
                intent.putExtra("product_id", "")
                overridePendingTransition(0, 0)
                startActivity(intent)
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_profile -> {
                startActivity(Intent(this, Profile_Activity::class.java))
                overridePendingTransition(0, 0)
                // Handle the camera action
            }
            R.id.nav_current_order -> {
                startActivity(Intent(this, Current_Order_Activity::class.java))
                overridePendingTransition(0, 0)
            }
            R.id.nav_complete_order -> {
                startActivity(Intent(this, Complete_Order_Activity::class.java))
                overridePendingTransition(0, 0)
            }
            R.id.nav_cancel_order -> {
                startActivity(Intent(this, Cancel_Order_Activity::class.java))
                overridePendingTransition(0, 0)

            }
            R.id.nav_wallet -> {
                startActivity(Intent(this, WalletActivity::class.java))
                overridePendingTransition(0, 0)

            }
            R.id.nav_change_language -> {
                startActivity(Intent(this, Change_Language_Activity::class.java))
                overridePendingTransition(0, 0)
            }
            R.id.nav_share_app -> {
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Khedut Bolo")
                shareIntent.putExtra(
                    Intent.EXTRA_TEXT,
                    "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID
                )
                startActivity(Intent.createChooser(shareIntent, "Share via"))

            }
            R.id.nav_logout -> {
                val dialogBuilder = AlertDialog.Builder(this)
                dialogBuilder.setMessage(resources.getString(R.string.areyoulogout))

                    .setCancelable(false)
                    .setPositiveButton(resources.getString(R.string.areyouyes), DialogInterface.OnClickListener { dialog, id ->
                        if (!isNetworkAvailable()) {
                            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                        } else {
                            hud.show()
                            val json = JSONObject()
                            json.put("customer_id", str_id)
                            json.put("token", "")
                            logout({
                                hud.dismiss();

                                if (it == null) {
                                    Toast.makeText(
                                        applicationContext,
                                        resources.getString(R.string.connectionerror),
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                    return@logout
                                } else {
                                    logoutResponse(it)
                                }

                            }).execute("POST", AppConfig.LOGOUT, json.toString())
                        }
                    })
                    .setNegativeButton(resources.getString(R.string.areyouno), DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                    })

                val alert = dialogBuilder.create()
                alert.setTitle(resources.getString(R.string.menu_logout))
                alert.show()

            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    class Solution(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cropResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {
                jsonObject = jsonArrayData.getJSONObject(i)

                if (i == 0) {
                    txt_insects_id.setText(jsonObject.getString("id"))
                    txt_insects.setText(jsonObject.getString("name"))
                    Glide.with(this)
                        .load(jsonObject.getString("image"))
                        .error(R.mipmap.ic_launcher_round)
                        .into(img_insects)
                } else if (i == 1) {
                    txt_disease_id.setText(jsonObject.getString("id"))
                    txt_disease.setText(jsonObject.getString("name"))
                    Glide.with(this)
                        .load(jsonObject.getString("image"))
                        .error(R.mipmap.ic_launcher_round)
                        .into(img_disease)
                } else if (i == 2) {
                    txt_condemnation_id.setText(jsonObject.getString("id"))
                    txt_condemnation.setText(jsonObject.getString("name"))
                    Glide.with(this)
                        .load(jsonObject.getString("image"))
                        .error(R.mipmap.ic_launcher_round)
                        .into(img_condemnation)
                }
            }
        } else {
            Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
        }
    }

    class Offer(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun offerResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")
        number_call = jsonObj.getString("mobile")

        if (jsonArrayData.length() > 0) {
            r_offer.visibility = View.VISIBLE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {
                jsonObject = jsonArrayData.getJSONObject(i)

                sliderItems.add(
                    SliderItem(
                        jsonObject.getString("id"), jsonObject.getString("cover")
                        , jsonObject.getString("offer"), jsonObject.getString("description")
                    )
                )
            }

            val dotsIndicator = findViewById(R.id.spring_dots_indicator) as SpringDotsIndicator
            val adapter = ViewPagerAdapter(this, sliderItems)
            viewpager.adapter = adapter
            dotsIndicator.setViewPager(viewpager)

            NUM_PAGES = sliderItems!!.size

            // Auto start of viewpager
            val handler = Handler()
            val Update = Runnable {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0
                }
                viewpager!!.setCurrentItem(currentPage++, true)
            }
            val swipeTimer = Timer()
            swipeTimer.schedule(object : TimerTask() {
                override fun run() {
                    handler.post(Update)
                }
            }, 3000, 3000)

        } else {
            r_offer.visibility = View.GONE
        }
    }

    class product(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun productResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (status.equals("111")){

        }else{
            try {
                if (jsonArrayData.length() > 0) {

                    var jsonObject: JSONObject? = null

                    for (i in 0 until jsonArrayData.length()) {
                        jsonObject = jsonArrayData.getJSONObject(i)

                        val type = jsonObject.getString("type")
                        val data = jsonObject.getString("data")
                        val priority = jsonObject.getString("priority")

                        if (i == 0) {
                            check_category = priority
                            txt_title_product.setText(resources.getString(R.string.product_category))

                            val jsonArrayData = jsonObject.getJSONArray("data")

                            if (jsonArrayData.length() > 0) {

                                var jsonObjectData: JSONObject? = null

                                for (i in 0 until jsonArrayData.length()) {
                                    jsonObjectData = jsonArrayData.getJSONObject(i)

                                    if (type.equals("Product Categories / Manage Products")) {
                                        productdata.add(
                                            ProductModel(
                                                jsonObjectData.getString("id"),
                                                jsonObjectData.getString("name")
                                                ,
                                                jsonObjectData.getString("cover"),
                                                jsonObjectData.getString("price"),
                                                jsonObjectData.getString("sale_price")
                                                ,
                                                jsonObjectData.getString("company_name")
                                            )
                                        )
                                    } else if (type.equals("Company List")) {
                                        company.add(
                                            CompanyModel(
                                                jsonObjectData.getString("id"),
                                                jsonObjectData.getString("name")
                                                ,
                                                jsonObjectData.getString("cover"),
                                                jsonObjectData.getString("price"),
                                                jsonObjectData.getString("sale_price")
                                                ,
                                                jsonObjectData.getString("company_name")
                                            )
                                        )
                                    } else if (type.equals("Trending Products")) {
                                        trending.add(
                                            TrandingModel(
                                                jsonObjectData.getString("id"),
                                                jsonObjectData.getString("name")
                                                ,
                                                jsonObjectData.getString("cover"),
                                                jsonObjectData.getString("price"),
                                                jsonObjectData.getString("sale_price")
                                                ,
                                                jsonObjectData.getString("company_name")
                                            )
                                        )
                                    }
                                }
                            }

                            if (type.equals("Product Categories / Manage Products")) {
                                adapter_product = CustomAdapter(this, productdata)
                                rv_product.adapter = adapter_product
                            } else if (type.equals("Company List")) {
                                val adapter_company = Companyadapter(company)
                                rv_company.adapter = adapter_company
                            } else if (type.equals("Trending Products")) {
                                val adapter_trending = TrendingAdapter(trending)
                                rv_tranding.adapter = adapter_trending
                            }

                        } else if (i == 1) {
                            check_company = priority
                            txt_title_company.setText(resources.getString(R.string.company_list))

                            array_company = jsonObject.getString("data")

                            val jsonArraycompany = jsonObject.getJSONArray("data")

                            if (data.equals("[]")) {
                                company_all_check = "0"
                            } else {
                                company_all_check = ""
                            }

                            if (jsonArraycompany.length() > 0) {

                                var jsonObjectData: JSONObject? = null

                                for (i in 0 until jsonArraycompany.length()) {
                                    jsonObjectData = jsonArraycompany.getJSONObject(i)

                                    if (type.equals("Product Categories / Manage Products")) {
                                        productdata.add(
                                            ProductModel(
                                                jsonObjectData.getString("id"),
                                                jsonObjectData.getString("name")
                                                ,
                                                jsonObjectData.getString("cover"),
                                                jsonObjectData.getString("price"),
                                                jsonObjectData.getString("sale_price")
                                                ,
                                                jsonObjectData.getString("company_name")
                                            )
                                        )
                                    } else if (type.equals("Company List")) {
                                        if (i < 6) {
                                            company.add(
                                                CompanyModel(
                                                    jsonObjectData.getString("id"),
                                                    jsonObjectData.getString("name")
                                                    ,
                                                    jsonObjectData.getString("cover"),
                                                    jsonObjectData.getString("price"),
                                                    jsonObjectData.getString("sale_price")
                                                    ,
                                                    jsonObjectData.getString("company_name")
                                                )
                                            )
                                        }
                                    } else if (type.equals("Trending Products")) {
                                        trending.add(
                                            TrandingModel(
                                                jsonObjectData.getString("id"),
                                                jsonObjectData.getString("name")
                                                ,
                                                jsonObjectData.getString("cover"),
                                                jsonObjectData.getString("price"),
                                                jsonObjectData.getString("sale_price")
                                                ,
                                                jsonObjectData.getString("company_name")
                                            )
                                        )
                                    }
                                }
                            }

                            if (type.equals("Product Categories / Manage Products")) {
                                adapter_product = CustomAdapter(this, productdata)
                                rv_product.adapter = adapter_product
                            } else if (type.equals("Company List")) {
                                val adapter_company = Companyadapter(company)
                                rv_company.adapter = adapter_company
                            } else if (type.equals("Trending Products")) {
                                val adapter_trending = TrendingAdapter(trending)
                                rv_tranding.adapter = adapter_trending
                            }

                        } else if (i == 2) {
                            check_trending = priority
                            txt_title_trending.setText(resources.getString(R.string.trading_production))

                            array_trending = jsonObject.getString("data")
                            val jsonArrayData = jsonObject.getJSONArray("data")

                            if (data.equals("[]")) {
                                trending_more_check = "0"
                            } else {
                                trending_more_check = ""
                            }

                            if (jsonArrayData.length() > 0) {

                                var jsonObjectData: JSONObject? = null

                                for (i in 0 until jsonArrayData.length()) {
                                    jsonObjectData = jsonArrayData.getJSONObject(i)

                                    if (type.equals("Product Categories / Manage Products")) {
                                        productdata.add(
                                            ProductModel(
                                                jsonObjectData.getString("id"),
                                                jsonObjectData.getString("name")
                                                ,
                                                jsonObjectData.getString("cover"),
                                                jsonObjectData.getString("price"),
                                                jsonObjectData.getString("sale_price")
                                                ,
                                                jsonObjectData.getString("company_name")
                                            )
                                        )
                                    } else if (type.equals("Company List")) {
                                        company.add(
                                            CompanyModel(
                                                jsonObjectData.getString("id"),
                                                jsonObjectData.getString("name")
                                                ,
                                                jsonObjectData.getString("cover"),
                                                jsonObjectData.getString("price"),
                                                jsonObjectData.getString("sale_price")
                                                ,
                                                jsonObjectData.getString("company_name")
                                            )
                                        )
                                    } else if (type.equals("Trending Products")) {
                                        if (i < 6) {
                                            trending.add(
                                                TrandingModel(
                                                    jsonObjectData.getString("id"),
                                                    jsonObjectData.getString("name")
                                                    ,
                                                    jsonObjectData.getString("cover"),
                                                    jsonObjectData.getString("price"),
                                                    jsonObjectData.getString("sale_price")
                                                    ,
                                                    jsonObjectData.getString("company_name")
                                                )
                                            )
                                        }
                                    }
                                }
                            }

                            if (type.equals("Product Categories / Manage Products")) {
                                adapter_product = CustomAdapter(this, productdata)
                                rv_product.adapter = adapter_product
                            } else if (type.equals("Company List")) {
                                val adapter_company = Companyadapter(company)
                                rv_company.adapter = adapter_company
                            } else if (type.equals("Trending Products")) {
                                val adapter_trending = TrendingAdapter(trending)
                                rv_tranding.adapter = adapter_trending
                            }
                        }
                    }

                    if (trending_more_check.equals("0")) {
                        txt_trending_more.visibility = View.GONE
                        l_trending.visibility = View.GONE
                    } else {
                        txt_trending_more.visibility = View.VISIBLE
                        l_trending.visibility = View.VISIBLE
                    }

                    if (company_all_check.equals("0")) {
                        txt_company_all.visibility = View.GONE
                        l_company.visibility = View.GONE
                    } else {
                        txt_company_all.visibility = View.VISIBLE
                        l_company.visibility = View.VISIBLE
                    }

                    l_row.removeView(l_product)
                    l_row.removeView(l_company)
                    l_row.removeView(l_trending)

                    if (check_category.equals("1") && check_company.equals("2") && check_trending.equals("3")) {
                        l_row.addView(l_product)
                        l_row.addView(l_company)
                        l_row.addView(l_trending)
                    } else if (check_category.equals("2") && check_company.equals("3") && check_trending.equals("1")) {
                        l_row.addView(l_trending)
                        l_row.addView(l_product)
                        l_row.addView(l_company)
                    } else if (check_category.equals("3") && check_company.equals("1") && check_trending.equals("2")) {
                        l_row.addView(l_company)
                        l_row.addView(l_trending)
                        l_row.addView(l_product)
                    } else if (check_company.equals("1") && check_category.equals("2") && check_trending.equals("3")) {
                        l_row.addView(l_company)
                        l_row.addView(l_product)
                        l_row.addView(l_trending)
                    } else if (check_company.equals("2") && check_category.equals("3") && check_trending.equals("1")) {
                        l_row.addView(l_trending)
                        l_row.addView(l_company)
                        l_row.addView(l_product)
                    } else if (check_company.equals("3") && check_category.equals("1") && check_trending.equals("2")) {
                        l_row.addView(l_product)
                        l_row.addView(l_trending)
                        l_row.addView(l_company)
                    } else if (check_trending.equals("1") && check_category.equals("2") && check_company.equals("3")) {
                        l_row.addView(l_trending)
                        l_row.addView(l_product)
                        l_row.addView(l_company)
                    } else if (check_trending.equals("2") && check_category.equals("3") && check_company.equals("1")) {
                        l_row.addView(l_company)
                        l_row.addView(l_trending)
                        l_row.addView(l_product)
                    } else if (check_trending.equals("3") && check_category.equals("1") && check_company.equals("2")) {
                        l_row.addView(l_product)
                        l_row.addView(l_company)
                        l_row.addView(l_trending)
                    }

                    l_row.invalidate();


                } else {
                    Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    class logout(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun logoutResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")

        if (status.equals("200")) {
            val sharedProfile = this.getSharedPreferences("profile", 0)
            val editor: SharedPreferences.Editor = sharedProfile.edit()
            editor.putString("id", null)
            editor.putString("name", null)
            editor.putString("mobile", null)
            editor.putString("address", null)
            editor.putString("language", null)
            editor.putString("image", null)
            editor.putString("check_otp", null)
            editor.apply()

            val intent = Intent(this@DashBoardActivity, Select_Language_Activity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            overridePendingTransition(0, 0)
            startActivity(intent)
            finish()
        }
    }

    @SuppressLint("MissingPermission")
    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    class Cartitem(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cartitemResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")){
            if (data.equals("[]")) {
                tv.setText("0")
            } else {
                if (data.toInt() > 99) {
                    var data1 = "99+"
                    tv.setText(data1)
                } else {
                    tv.setText(data)
                }
            }
        }
    }

}

