package com.khedutbolo.util

class AppConfig {


        companion object {

                //                                        const val URL = "http://192.168.1.14/api/v1/"
                const val URL = "http://khedutbolo.com/api/v1/"
                //                        const val IMAGE_PATH = "http://192.168.1.12/"
//        const val IMAGE_PATH1 = "http://192.168.1.12/storage/"
//        const val IMAGE_PATH = "http://khedutbolo.shineinfosoft.in/"
//        const val IMAGE_PATH1 = "http://khedutbolo.shineinfosoft.in/storage/"

                const val SENDOTP = URL + "sendOTP"
                const val REGISTER = URL + "register_customer"
                const val UPDATE_PROFILE = URL + "update/profile" //--Done
                const val CROP = URL + "crop"   //--Done
                const val CROP_SOLUTION = URL + "cropSolution"  //--Done
                const val SOLUTION = URL + "solution"   //--Done
                const val OFFER = URL + "offer"  //--Done
                const val CURRENT_ORDER = URL + "currentOrder"   //--Done
                const val SHOW_CANCEL_ORDER = URL + "showCancelOrder"  //--Done
                const val NOTIFICATION = URL + "notification"  //--Done
                const val COMPLETE_ORDER = URL + "completeOrder"  //--Done
                const val PRODUCT_CATEGORY = URL + "product_categories?user_id=" //--Done
                const val PRODUCT_DETAIL = URL + "productDetail"   //--Done
                const val SIMILAR_PRODUCT = URL + "similarProducts"   //--Done
                const val SHOW_CART = URL + "showCart?customer_id="  //--Done
                const val COMPANY_PRODUCT = URL + "companyProducts"  //--Done
                const val FILTER_LIST = URL + "ctfilter?crop_id="  //--Done
                const val ADD_CART = URL + "addCart"  //--Done
                const val ORDER = URL + "order"  //--Done
                const val STATE = URL + "state?code="
                const val CITY = URL + "cities"
                const val TEHSIL = URL + "tehsil?city_id="
                const val VILLAGE = URL + "villages?tehsil_id="
                const val FILTER_APPLY = URL + "filter"  //--Done
                const val SELECT_CATEGORY = URL + "selectedCategory"  //--Done
                const val PAYMENT = URL + "payment"  //--Done
                const val DELETE = URL + "deleteProductCart" //--Done
                const val LOGOUT = URL + "log_in_out"
                const val SUB_CATEGORY = URL + "get_subCategory?id=" //--Done
                const val PRODUCT_ATTRIBUTE = URL + "productAttribute"  //--Done
                const val CART_COUNT = URL + "count-product?customer_id=" //--Done
                const val CURRENT_LIST = URL + "get-order?user_id="  //--Done
                const val CANCEL_ORDER = URL + "cancelOrder?order_id="  //--Done
                const val WALLET_TRANSACTION = URL + "get_wallet?customer_id="
                const val SEARCH = URL + "search?value="  //--Done
                const val OFFER_DETAIL = URL + "offer/products"  //--Done
                const val BUY_NOW = URL + "buy-now?customer_id="  //--Done
                const val CHANGE_LANGUAGE = URL + "lang_change?user_id="  //--Done
                const val CROP_CATEGORY = URL + "cropSubSolution"  //--Done
        }

}