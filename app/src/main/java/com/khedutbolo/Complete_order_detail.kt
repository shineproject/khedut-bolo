package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Complete_Order_Adapter
import com.khedutbolo.Model.Complete_Order_Model
import com.khedutbolo.Model.Current_Order_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Complete_order_detail : AppCompatActivity() {

    lateinit var rv_complete : RecyclerView
    private var str_id = ""
    lateinit var hud: KProgressHUD
    lateinit var txt_nodata: LinearLayout
    val complete_order = ArrayList<Complete_Order_Model>()
    private var order_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_order_detail)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.orderdetail)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val i = intent
        order_id = i.getStringExtra("order_id")

        rv_complete = findViewById(R.id.rv_complete_order_list) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_complete.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            json.put("user_id", str_id)
            json.put("order_id", order_id)
            completeorder({
                hud.dismiss();
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                    return@completeorder
                } else {
                    completeorderResponse(it)
                }

            }).execute("POST", AppConfig.COMPLETE_ORDER, json.toString())
        }

        rv_complete.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_complete,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, ProductDetailActivity::class.java)
                        intent.putExtra("image",complete_order.get(position).image)
                        intent.putExtra("product_name",complete_order.get(position).name)
                        intent.putExtra("sale_price",complete_order.get(position).sale_price)
                        intent.putExtra("price",complete_order.get(position).price)
                        intent.putExtra("qty",complete_order.get(position).qty)
                        intent.putExtra("size",complete_order.get(position).key + complete_order.get(position).value)
                        intent.putExtra("shipping_price",complete_order.get(position).shipping_price)
                        intent.putExtra("dealer",complete_order.get(position).dealer)
                        intent.putExtra("total",complete_order.get(position).total)
                        intent.putExtra("gst",complete_order.get(position).gst)
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    class completeorder(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun completeorderResponse(response: String?) {


        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0){
            rv_complete.visibility = View.VISIBLE
            txt_nodata.visibility = View.GONE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {
                jsonObject = jsonArrayData.getJSONObject(i)

                complete_order.add(Complete_Order_Model(jsonObject.getString("order_id"), jsonObject.getString("product_name"), jsonObject.getString("price"), jsonObject.getString("quantity")
                    , jsonObject.getString("sale_price") , jsonObject.getString("created_at"),
                    jsonObject.getString("cover"),
                    jsonObject.getString("value")
                    , jsonObject.getString("key"), jsonObject.getString("shipping_price"),
                    jsonObject.getString("Dealer"),
                    jsonObject.getString("gst_price"),
                    jsonObject.getString("total")))
            }

            val adapter_complete = Complete_Order_Adapter(complete_order)
            rv_complete.adapter = adapter_complete
        }
        else{
            rv_complete.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
