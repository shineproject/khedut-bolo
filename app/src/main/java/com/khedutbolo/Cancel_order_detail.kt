package com.khedutbolo

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.Cancel_Order_Adapter
import com.khedutbolo.Model.Cancel_Order_Model
import com.khedutbolo.Model.Complete_Order_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Cancel_order_detail : AppCompatActivity() {

    lateinit var rv_cancel : RecyclerView
    private var str_id = ""
    lateinit var hud: KProgressHUD
    lateinit var txt_nodata: LinearLayout
    val cancel_order = ArrayList<Cancel_Order_Model>()
    private var order_id = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cancel_order_detail)

        val actionbar = supportActionBar
        //set actionbar title
        actionbar!!.title = resources.getString(R.string.orderdetail)
        //set back button
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!

        val i = intent
        order_id = i.getStringExtra("order_id")

        rv_cancel = findViewById(R.id.rv_cancel_order_list) as RecyclerView
        txt_nodata = findViewById(R.id.txt_nodata)as LinearLayout

        rv_cancel.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        }else{
            hud.show()
            val json = JSONObject()
            json.put("user_id", str_id)
            json.put("order_id", order_id)
            cancelorder({
                hud.dismiss();
                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG).show()
                    return@cancelorder
                } else {
                    cancelorderResponse(it)
                }

            }).execute("POST", AppConfig.SHOW_CANCEL_ORDER, json.toString())
        }

        rv_cancel.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_cancel,
                object : RecyclerItemClickListenr.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        val intent = Intent(applicationContext, ProductDetailActivity::class.java)
                        intent.putExtra("image",cancel_order.get(position).image)
                        intent.putExtra("product_name",cancel_order.get(position).name)
                        intent.putExtra("sale_price",cancel_order.get(position).sale_price)
                        intent.putExtra("price",cancel_order.get(position).price)
                        intent.putExtra("qty",cancel_order.get(position).qty)
                        intent.putExtra("size",cancel_order.get(position).key + cancel_order.get(position).value)
                        intent.putExtra("shipping_price",cancel_order.get(position).shipping_price)
                        intent.putExtra("dealer",cancel_order.get(position).dealer)
                        intent.putExtra("total",cancel_order.get(position).total)
                        intent.putExtra("gst",cancel_order.get(position).gst)
                        overridePendingTransition(0, 0)
                        startActivity(intent)
                    }
                })
        )
    }

    class cancelorder(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cancelorderResponse(response: String?) {

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0){
            rv_cancel.visibility = View.VISIBLE
            txt_nodata.visibility = View.GONE
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {
                jsonObject = jsonArrayData.getJSONObject(i)

                cancel_order.add(Cancel_Order_Model(jsonObject.getString("order_id"), jsonObject.getString("product_name"), jsonObject.getString("price"), jsonObject.getString("quantity")
                    , jsonObject.getString("sale_price")
                    , jsonObject.getString("created_at"), jsonObject.getString("cover"),
                    jsonObject.getString("value")
                    , jsonObject.getString("key"), jsonObject.getString("shipping_price"),
                    jsonObject.getString("Dealer"),
                    jsonObject.getString("gst_price"),
                    jsonObject.getString("total")))
            }

            val adapter_cancel = Cancel_Order_Adapter(cancel_order)
            rv_cancel.adapter = adapter_cancel
        }else{
            rv_cancel.visibility = View.GONE
            txt_nodata.visibility = View.VISIBLE
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}
