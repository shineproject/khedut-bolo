package com.khedutbolo

import android.graphics.Typeface
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.util.AppConfig
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.CountDownTimer
import android.view.View
import android.widget.*
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.goodiebag.pinview.Pinview
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.khedutbolo.Service.AppSignatureHelper
import com.khedutbolo.Service.MySMSBroadcastReceiver
import java.lang.String.format
import com.khedutbolo.Service.MySMSBroadcastReceiver.OTPReceiveListener

class Check_OTP_Activity : AppCompatActivity(), OTPReceiveListener {

    lateinit var btn_verify_otp: Button
    lateinit var pin_view: Pinview
    lateinit var txt_ver_code: TextView
    lateinit var txt_ver_mobile_no: TextView
    lateinit var btn_resend_otp: Button
    lateinit var txt_time: TextView
    private var otp = ""
    private var mobile = ""
    private var register_check = ""
    private var str_language = ""
    private var str_id = ""
    lateinit var hud: KProgressHUD
    companion object {
        val OTP_REGEX: String? = "[0-9]{1,6}"
    }
    private var select_message = ""
    private var smsReceiver: MySMSBroadcastReceiver? = null

    @SuppressLint("ServiceCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check__otp_)

        val appSignatureHelper = AppSignatureHelper(applicationContext)
        appSignatureHelper.appSignatures;

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.verifycode)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_semibold = Typeface.createFromAsset(assets, "poppinssemiBold.otf")

        startSMSListener()

        val i = intent
        otp = i.getStringExtra("otp")
        mobile = i.getStringExtra("mobile")
        register_check = i.getStringExtra("register_check")
        str_id = i.getStringExtra("str_id")
        str_language = i.getStringExtra("language")

        btn_verify_otp = findViewById(R.id.btn_verify_otp) as Button
        pin_view = findViewById(R.id.pinview1) as Pinview
        txt_ver_code = findViewById(R.id.txt_verification_code) as TextView
        txt_ver_mobile_no = findViewById(R.id.txt_ver_mobile_no) as TextView
        txt_time = findViewById(R.id.txt_time) as TextView
        btn_resend_otp = findViewById(R.id.btn_resend_otp) as Button

        btn_verify_otp.typeface = typeface_medium
        txt_ver_code.typeface = typeface_semibold
        txt_ver_mobile_no.typeface = typeface_medium
        txt_time.typeface = typeface_medium
        btn_resend_otp.typeface = typeface_medium

        txt_ver_mobile_no.setText(resources.getString(R.string.pleaseselectmobile) + mobile)

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        starttime()

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

        pin_view.showKeyboard()
        pin_view.isLongClickable = false

        pin_view.setOnClickListener{
            pin_view.showCursor(true)
        }

        btn_resend_otp.setOnClickListener {
            pin_view.setValue("")
            if (mobile.toString().trim().equals("")) {
                Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
            } else {
                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                }else{
                    hud.show()
                    val json = JSONObject()
                    json.put("mobile", mobile)
                    HttpTask({
                        hud.dismiss();
                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            ).show()
                            return@HttpTask
                        } else {
                            otpResponse(it)
                        }

                    }).execute("POST", AppConfig.SENDOTP, json.toString())
                }
            }
        }

        btn_verify_otp.setOnClickListener {
            if (otp.equals(pin_view.value)){

                if (register_check.equals("true")){
                    hideKeyboard()
                    val sharedProfile = this.getSharedPreferences("profile", 0)
                    val editor: SharedPreferences.Editor =  sharedProfile.edit()
                    editor.putString("check_otp","1")
                    editor.apply()

                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    }else{
                        hud.show()
                        val json = JSONObject()
                        json.put("customer_id", str_id)
                        json.put("token", OtpActivity.token)
                        json.put("lang", str_language)
                        logout({
                            hud.dismiss();
                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                                return@logout
                            } else {
                                logoutResponse(it)
                            }

                        }).execute("POST", AppConfig.LOGOUT, json.toString())
                    }

                }else{
                    hideKeyboard()
                    val intent = Intent(applicationContext,RegistrationActivity::class.java)
                    intent.putExtra("mobile_no",mobile)
                    intent.putExtra("language",str_language)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    overridePendingTransition(0,0)
                    finish()
                }

            }
            else {
                hideKeyboard()
                Toast.makeText(applicationContext, resources.getString(R.string.otpnotmatch), Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun startSMSListener() {
        try {
            smsReceiver = MySMSBroadcastReceiver()
            smsReceiver!!.initOTPListener(this)

            val intentFilter = IntentFilter()
            intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
            this.registerReceiver(smsReceiver, intentFilter)

            val client = SmsRetriever.getClient(this)

            val task = client.startSmsRetriever()
            task.addOnSuccessListener {
                // API successfully started
            }

            task.addOnFailureListener {
                // Fail to start API
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    class HttpTask(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun otpResponse(response: String?) {

        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val data = jsonObj.getString("data")

        if (status.equals("200")){
            if (data.length > 0) {
                startSMSListener()
                val jsonData = JSONObject(data)
                val is_register = jsonData.getString("is_register")
                val mobile = jsonData.getString("mobile")
                otp = jsonData.getString("otp")

                register_check = is_register

                starttime()

            } else {
                Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
        }


    }

    class logout(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {
        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun logoutResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val language = jsonObj.getString("data")

        if (status.equals("200")) {
            val sharedPreferences: SharedPreferences = this.getSharedPreferences("language", Context.MODE_PRIVATE)
            val editor1: SharedPreferences.Editor =  sharedPreferences.edit()
            editor1.putString("lan_name",language)
            editor1.apply()

            val intent = Intent(applicationContext,DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            overridePendingTransition(0,0)
            finish()

        }
    }

    fun starttime(){
        object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                btn_resend_otp.visibility = View.GONE
                btn_verify_otp.visibility = View.VISIBLE

                val seconduntil = millisUntilFinished / 1000
                val second = seconduntil % 60
                val mins = seconduntil / 60

                var yy = format("%02d:%02d", mins, second);
                txt_time.setText("seconds remaining: " + yy)
            }

            override fun onFinish() {
                btn_resend_otp.visibility = View.VISIBLE
                btn_verify_otp.visibility = View.GONE
               txt_time.setText("seconds remaining: " +"00:00")
            }
        }.start()
    }

    override fun onSupportNavigateUp(): Boolean {
        hideKeyboard()
        onBackPressed()
        return true
    }

    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.requestFocus()
        imm.showSoftInput(this, 0)
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    override fun onOTPReceived(otp: String) {
        pin_view.setValue(otp)
        if (otp.equals(pin_view.value)){

            if (register_check.equals("true")){
                hideKeyboard()
                val sharedProfile = this.getSharedPreferences("profile", 0)
                val editor: SharedPreferences.Editor =  sharedProfile.edit()
                editor.putString("check_otp","1")
                editor.apply()

                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                }else{
                    hud.show()
                    val json = JSONObject()
                    json.put("customer_id", str_id)
                    json.put("token", OtpActivity.token)
                    json.put("lang", str_language)
                    logout({
                        hud.dismiss();
                        if (it == null) {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.connectionerror),
                                Toast.LENGTH_LONG
                            )
                                .show()
                            return@logout
                        } else {
                            logoutResponse(it)
                        }

                    }).execute("POST", AppConfig.LOGOUT, json.toString())
                }

            }else{
                hideKeyboard()
                val intent = Intent(applicationContext,RegistrationActivity::class.java)
                intent.putExtra("mobile_no",mobile)
                intent.putExtra("language",str_language)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                overridePendingTransition(0,0)
                finish()
            }

        }
        else {
            hideKeyboard()
            Toast.makeText(applicationContext, resources.getString(R.string.otpnotmatch), Toast.LENGTH_LONG).show()
        }
        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver!!)
        }
    }

    override fun onOTPTimeOut() {
        showToast("OTP Time out")
    }


    override fun onDestroy() {
        super.onDestroy()
        if (smsReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver!!)
        }
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}
