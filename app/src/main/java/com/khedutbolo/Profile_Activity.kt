package com.khedutbolo

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.ImageDecoder
import android.graphics.Typeface
import android.media.MediaScannerConnection
import android.net.ConnectivityManager
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.textfield.TextInputEditText
import com.kaopiz.kprogresshud.KProgressHUD
import com.khedutbolo.Adapter.City_adapter
import com.khedutbolo.Adapter.State_Adapter
import com.khedutbolo.Adapter.Tehsil_Adapter
import com.khedutbolo.Adapter.Village_Adapter
import com.khedutbolo.Model.City_Model
import com.khedutbolo.Model.State_Model
import com.khedutbolo.Model.Tehsil_Model
import com.khedutbolo.Model.Village_Model
import com.khedutbolo.Service.AsyncTaskCompleteListener
import com.khedutbolo.Service.MultiPartRequester
import com.khedutbolo.util.AppConfig
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.security.AccessController.getContext
import java.util.*
import kotlin.collections.HashMap

class Profile_Activity : AppCompatActivity(), AsyncTaskCompleteListener {

    lateinit var l_scroll_display : LinearLayout
    lateinit var l_scroll_edit : LinearLayout
    lateinit var scroll_edit : ScrollView
    lateinit var scroll_display : ScrollView
    lateinit var btn_save : Button
    lateinit var btn_edit : Button
    lateinit var btn_ok : Button
    lateinit var ed_name_edit : TextInputEditText
    lateinit var ed_mobile_edit : TextInputEditText
    lateinit var ed_address_edit : TextInputEditText
    lateinit var txt_select : TextView
    private var rb_english : RadioButton? = null
    private var rb_hindi : RadioButton? = null
    private var rb_gujarati : RadioButton? = null
    lateinit var txt_name : TextView
    lateinit var txt_mobile : TextView
    lateinit var txt_address : TextView
    lateinit var txt_state : TextView
    lateinit var txt_city : TextView
    lateinit var txt_tehsil : TextView
    lateinit var txt_village : TextView
    lateinit var txt_select_language : TextView
    lateinit var txt_language : TextView
    lateinit var img_profile : CircleImageView
    lateinit var img_profile_edit : CircleImageView
    lateinit var img_profile_edit_icon : CircleImageView
    private var str_id = ""
    private var str_name = ""
    private var str_mobile = ""
    private var str_address = ""
    private var str_image = ""
    private var str_language = ""
    private var state_name = ""
    private var city_name = ""
    private var tehsil_name = ""
    private var village_name = ""
    lateinit var hud: KProgressHUD
    private val GALLERY = 1
    private val CAMERA = 2
    private var ALL = 3
    private var path = ""
    lateinit var spinner_state: Spinner
    lateinit var img_spinnerstate: ImageView
    lateinit var spinner_city: Spinner
    lateinit var img_spinnercity: ImageView
    lateinit var spinner_tehsil: Spinner
    lateinit var img_spinnertehsil: ImageView
    lateinit var spinner_village: Spinner
    lateinit var img_spinnervillage: ImageView
    val state_ = ArrayList<State_Model>()
    val city_ = ArrayList<City_Model>()
    val tehsil_ = ArrayList<Tehsil_Model>()
    val village_ = ArrayList<Village_Model>()
    private var str_state = ""
    private var str_city = ""
    private var str_tehsil = ""
    private var str_village = ""
    private var str_state_id = ""
    private var str_city_id = ""
    private var str_tehsil_id = ""
    private var str_village_id = ""
    private var check_keypad = ""
    private var str_language_check = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_)

        val typeface_medium = Typeface.createFromAsset(assets, "poppinsmedium.otf")
        val typeface_regular = Typeface.createFromAsset(assets, "poppinsregular.otf")

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.myprofile)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        val policy = StrictMode.ThreadPolicy.Builder()
            .permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure()
        }

        val sharedPreferences: SharedPreferences = this.getSharedPreferences("language", Context.MODE_PRIVATE)
        str_language_check = sharedPreferences.getString("lan_name", "")!!

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.drawable.load)

        Glide.with(this)
            .load(R.drawable.load)
            .into(imageView)

        hud = KProgressHUD.create(this)
            .setCustomView(imageView)
            .setBackgroundColor(Color.TRANSPARENT)
            .setDimAmount(0.5f)

        val sharedProfile: SharedPreferences = this.getSharedPreferences("profile", Context.MODE_PRIVATE)
        str_id = sharedProfile.getString("id", "")!!
        str_name = sharedProfile.getString("name", "")!!
        str_mobile = sharedProfile.getString("mobile", "")!!
        str_address = sharedProfile.getString("address", "")!!
        str_image = sharedProfile.getString("image", "")!!
        str_language = sharedProfile.getString("language", "")!!
        state_name = sharedProfile.getString("state_name", "")!!
        city_name = sharedProfile.getString("city_name", "")!!
        tehsil_name = sharedProfile.getString("tehsil_name", "")!!
        village_name = sharedProfile.getString("village_name", "")!!
        str_state_id = sharedProfile.getString("sid", "")!!
        str_city_id = sharedProfile.getString("cid", "")!!
        str_tehsil_id = sharedProfile.getString("tid", "")!!
        str_village_id = sharedProfile.getString("vid", "")!!

        l_scroll_display = findViewById(R.id.l_scroll_display) as LinearLayout
        l_scroll_edit = findViewById(R.id.l_scroll_edit) as LinearLayout
        scroll_edit = findViewById(R.id.edit_scroll) as ScrollView
        scroll_display = findViewById(R.id.display_scroll) as ScrollView
        btn_save = findViewById(R.id.btn_save) as Button
        btn_edit = findViewById(R.id.btn_edit) as Button
        btn_ok = findViewById(R.id.btn_ok) as Button
        ed_name_edit = findViewById(R.id.ed_name) as TextInputEditText
        ed_mobile_edit = findViewById(R.id.ed_mobile_no) as TextInputEditText
        ed_address_edit = findViewById(R.id.ed_address) as TextInputEditText
        txt_select = findViewById(R.id.txt_select_edit) as TextView
        rb_english = findViewById(R.id.r_english) as RadioButton
        rb_hindi = findViewById(R.id.r_hindi) as RadioButton
        rb_gujarati = findViewById(R.id.r_gujarati) as RadioButton
        txt_name = findViewById(R.id.txt_name) as TextInputEditText
        txt_mobile = findViewById(R.id.txt_mobile_no) as TextInputEditText
        txt_address = findViewById(R.id.txt_address) as TextInputEditText
        txt_state = findViewById(R.id.txt_state) as TextInputEditText
        txt_city = findViewById(R.id.txt_city) as TextInputEditText
        txt_tehsil = findViewById(R.id.txt_tehsil) as TextInputEditText
        txt_village = findViewById(R.id.txt_village) as TextInputEditText
        txt_select_language = findViewById(R.id.txt_select) as TextView
        txt_language = findViewById(R.id.txt_language) as TextView
        img_profile = findViewById(R.id.img_profile) as CircleImageView
        img_profile_edit = findViewById(R.id.img_profile_edit) as CircleImageView
        img_profile_edit_icon = findViewById(R.id.img_profile_edit_icon) as CircleImageView
        spinner_state = findViewById(R.id.spinnerstate) as Spinner
        img_spinnerstate = findViewById(R.id.img_spinnerstate) as ImageView
        spinner_city = findViewById(R.id.spinnercity) as Spinner
        img_spinnercity = findViewById(R.id.img_spinnercity) as ImageView
        spinner_tehsil = findViewById(R.id.spinnertehsil) as Spinner
        img_spinnertehsil = findViewById(R.id.img_spinnertehsil) as ImageView
        spinner_village = findViewById(R.id.spinnervillage) as Spinner
        img_spinnervillage = findViewById(R.id.img_spinnervillage) as ImageView

        btn_save.typeface = typeface_regular
        btn_edit.typeface = typeface_regular
        btn_ok.typeface = typeface_regular
        ed_name_edit.typeface = typeface_medium
        ed_mobile_edit.typeface = typeface_medium
        ed_address_edit.typeface = typeface_medium
        txt_select.typeface = typeface_medium
        rb_english!!.typeface = typeface_medium
        rb_hindi!!.typeface = typeface_medium
        rb_gujarati!!.typeface = typeface_medium
        txt_name.typeface = typeface_medium
        txt_mobile.typeface = typeface_medium
        txt_address.typeface = typeface_medium
        txt_state.typeface = typeface_medium
        txt_city.typeface = typeface_medium
        txt_tehsil.typeface = typeface_medium
        txt_village.typeface = typeface_medium
        txt_select_language.typeface = typeface_medium
        txt_language.typeface = typeface_medium

        ed_mobile_edit.isLongClickable = false

        if (str_language_check.equals("hi")){
            txt_language.setText("हिंदी")
        }else if (str_language_check.equals("gu")){
            txt_language.setText("ગુજરાતી")
        }else if (str_language_check.equals("en")){
            txt_language.setText("English")
        }else{
            txt_language.setText("English")
        }

        img_profile_edit_icon.setOnClickListener{
            showPictureDialog()
        }

        btn_save.setOnClickListener {
            if (ed_name_edit.text.toString().trim().equals("")){
                Toast.makeText(applicationContext, resources.getString(R.string.entername), Toast.LENGTH_LONG).show()
            } else if (ed_mobile_edit.text.toString().trim().equals("")) {
                Toast.makeText(applicationContext, resources.getString(R.string.entermobileno), Toast.LENGTH_LONG).show()
            } else if (ed_mobile_edit.text.toString().trim().length < 10) {
                Toast.makeText(applicationContext, resources.getString(R.string.entermobileno10), Toast.LENGTH_LONG).show()
            } else{
                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()

                }else{
                    uploadImage()
                }
            }
        }

        img_spinnerstate.setOnClickListener {
            spinner_state.performClick()
        }

        img_spinnercity.setOnClickListener {
            spinner_city.performClick()
        }

        img_spinnertehsil.setOnClickListener {
            spinner_tehsil.performClick()
        }

        img_spinnervillage.setOnClickListener {
            spinner_village.performClick()
        }

        spinner_state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_state = state_.get(position).state
                str_state_id = state_.get(position).id
                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    } else {
                        city_.clear()
                        hud.show()
                        val json = JSONObject()
                        json.put("state_id", state_.get(position).id)
                        json.put("code", str_city_id)
                        json.put("customer_id", str_id)
                        json.put("language", "")
                        Citydata({
                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                ).show()
                                return@Citydata
                            } else {
                                cityResponse(it)
                            }
                        }).execute("POST", AppConfig.CITY, json.toString())
                    }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        spinner_city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_city = city_.get(position).city
                str_city_id = city_.get(position).id
                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    } else {
                        tehsil_.clear()
                        Tehsildata({
                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                ).show()
                                return@Tehsildata
                            } else {
                                tehsilResponse(it)
                            }

                        }).execute("GET", AppConfig.TEHSIL + str_city_id + "&code="+str_tehsil_id+"&customer_id="+str_id+"&language="+"", "")
                    }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        spinner_tehsil.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_tehsil = tehsil_.get(position).tehsil
                str_tehsil_id = tehsil_.get(position).id

                    if (!isNetworkAvailable()) {
                        Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                    } else {
                        village_.clear()
                        Villagedata({
                            hud.dismiss();

                            if (it == null) {
                                Toast.makeText(
                                    applicationContext,
                                    resources.getString(R.string.connectionerror),
                                    Toast.LENGTH_LONG
                                ).show()
                                return@Villagedata
                            } else {
                                villageResponse(it)
                            }

                        }).execute("GET", AppConfig.VILLAGE + str_tehsil_id + "&code="+str_village_id+"&customer_id="+str_id+"&language="+"", "")
                    }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        spinner_village.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                str_village = village_.get(position).village
                str_village_id = village_.get(position).id

                if (check_keypad.equals("")){
                    ed_name_edit.showKeyboard()
                    val count = ed_name_edit.text!!.length
                    ed_name_edit.setSelection(count)
                    check_keypad = "1";
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }

        if (!isNetworkAvailable()) {
            Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
        } else {
            hud.show()
            Statedata({
                hud.dismiss();

                if (it == null) {
                    Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                        .show()
                    return@Statedata
                } else {
                    stateResponse(it)
                }

            }).execute("GET", AppConfig.STATE+str_state_id+"&customer_id="+str_id+"&language="+"", "")
        }

        btn_edit.setOnClickListener {
                l_scroll_edit.visibility = View.VISIBLE
                scroll_edit.visibility = View.VISIBLE
                l_scroll_display.visibility = View.GONE
                scroll_display.visibility = View.GONE
        }

        btn_ok.setOnClickListener {
            onBackPressed()
        }

        txt_name.setText(str_name)
        txt_mobile.setText(str_mobile)
        txt_address.setText(str_address)
        txt_state.setText(state_name)
        txt_city.setText(city_name)
        txt_tehsil.setText(tehsil_name)
        txt_village.setText(village_name)
        if (str_image.equals("null")){
            Glide.with(this)
            .load(R.mipmap.user_icon)
            .into(img_profile)
        }else{
            Glide.with(this)
            .load(str_image)
            .error(R.mipmap.user_icon)
            .into(img_profile)
        }

        ed_name_edit.setText(str_name)
        ed_mobile_edit.setText(str_mobile)
        ed_address_edit.setText(str_address)
        if (str_image.equals("null")){
            Glide.with(this)
                .load(R.mipmap.user_icon)
                .into(img_profile_edit)
        }else{
                    Glide.with(this)
                        .load(str_image)
                        .error(R.mipmap.user_icon)
                        .into(img_profile_edit)
        }

        txt_name.isLongClickable = false
        txt_mobile.isLongClickable = false
        txt_address.isLongClickable = false
        txt_state.isLongClickable = false
        txt_city.isLongClickable = false
        txt_tehsil.isLongClickable = false
        txt_village.isLongClickable = false

    }

    private fun uploadImage() {
        hud.show()
        val map = HashMap<String, String>()
        map.put("url", AppConfig.UPDATE_PROFILE)
        if (path.equals("")){

        }else{
            map.put("image", path)
        }

        map.put("id", str_id)
        map.put("name", ed_name_edit.text.toString().trim())
        map.put("mobile", ed_mobile_edit.text.toString().trim())
        map.put("language", str_language_check)
        map.put("sid", str_state_id)
        map.put("cid", str_city_id)
        map.put("tid", str_tehsil_id)
        map.put("vid", str_village_id)
        MultiPartRequester(this, map, ALL, this)
    }

    override fun onSupportNavigateUp(): Boolean {
            onBackPressed()
        return true
    }

    private fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle(resources.getString(R.string.selectaction))
        val pictureDialogItems = arrayOf(resources.getString(R.string.selectgallery), resources.getString(R.string.selectcamera))
        pictureDialog.setItems(pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    public override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                ALL = requestCode
                val uri = data.data
                val FILE = arrayOf(MediaStore.Images.Media.DATA, MediaStore.Video.Media.DATA)
                val cursor = this.getContentResolver().query(uri!!, FILE, null, null, null)
                cursor!!.moveToFirst()
                val columnIndex = cursor.getColumnIndex(FILE[0])
                path = cursor.getString(columnIndex)
                cursor.close()

                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                    img_profile_edit!!.setImageBitmap(bitmap)

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, resources.getString(R.string.failed), Toast.LENGTH_SHORT).show()
                }
            }
        }
        else if (requestCode == CAMERA)
        {
            ALL = requestCode
            if (resultCode == 0){
            }else{
                val thumbnail = data!!.extras!!.get("data") as Bitmap
                img_profile_edit!!.setImageBitmap(thumbnail)
                path = saveImage(thumbnail)
            }
        }
        else{
            ALL = 0
        }
    }

    class Statedata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun stateResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                state_.add(
                    State_Model(
                        jsonObject.getString("STCode"), jsonObject.getString("DTName")
                    )
                )
            }

            val adapter_state = State_Adapter(this, state_)
            spinner_state.adapter = adapter_state

        } else {
        }

    }

    class Citydata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun cityResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {

            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                city_.add(
                    City_Model(
                        jsonObject.getString("DTCode"), jsonObject.getString("DTName")
                    )
                )
            }

            val adapter_city = City_adapter(this, city_)
            spinner_city.adapter = adapter_city

        } else {
        }

    }

    class Tehsildata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun tehsilResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                tehsil_.add(
                    Tehsil_Model(
                        jsonObject.getString("SDTCode"), jsonObject.getString("SDTName")
                    )
                )
            }

            val adapter_tehsil = Tehsil_Adapter(this, tehsil_)
            spinner_tehsil.adapter = adapter_tehsil

        } else {
        }

    }

    class Villagedata(callback: (String?) -> Unit) : AsyncTask<String, Unit, String>() {

        var callback = callback

        override fun doInBackground(vararg params: String): String? {
            val url = URL(params[1])
            val httpClient = url.openConnection() as HttpURLConnection
            httpClient.setReadTimeout(10000)
            httpClient.setConnectTimeout(10000)
            httpClient.requestMethod = params[0]

            if (params[0] == "POST") {
                httpClient.instanceFollowRedirects = false
                httpClient.doOutput = true
                httpClient.doInput = true
                httpClient.useCaches = false
                httpClient.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            }
            try {
                if (params[0] == "POST") {
                    httpClient.connect()
                    val os = httpClient.getOutputStream()
                    val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
                    writer.write(params[2])
                    writer.flush()
                    writer.close()
                    os.close()
                }
                if (httpClient.responseCode == HttpURLConnection.HTTP_OK) {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } else {
                    println("ERROR ${httpClient.responseCode}")
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                httpClient.disconnect()
            }

            return null
        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            callback(result)
        }
    }

    fun villageResponse(response: String?) {
        val jsonObj = JSONObject(response)
        val status = jsonObj.getString("status")
        val message = jsonObj.getString("message")
        val jsonArrayData = jsonObj.getJSONArray("data")

        if (jsonArrayData.length() > 0) {
            var jsonObject: JSONObject? = null

            for (i in 0 until jsonArrayData.length()) {

                jsonObject = jsonArrayData.getJSONObject(i)

                village_.add(
                    Village_Model(
                        jsonObject.getString("TVCode"), jsonObject.getString("Name")
                    )
                )
            }

            val adapter_village = Village_Adapter(this, village_)
            spinner_village.adapter = adapter_village

        } else {
        }

    }

    fun saveImage(myBitmap: Bitmap):String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)

        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY)
        Log.d("fee",wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {
            Log.d("heel",wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                .getTimeInMillis()).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    companion object {
        private val IMAGE_DIRECTORY = "/Khedut_Bolo"
    }

    override fun onTaskCompleted(response: String, serviceCode: Int) {

        Log.d("respon", response.toString())

        try
        {
            val jsonObject = JSONObject(response.toString())

            val status = jsonObject.getString("status")
            val message = jsonObject.getString("message")
            val data = jsonObject.getString("data")

            if (data.equals("[]")){
                hud.dismiss()
                Toast.makeText(applicationContext, resources.getString(R.string.nodata), Toast.LENGTH_LONG).show()
            }else{
                hud.dismiss()
                val jsonData = JSONObject(data)
                val id = jsonData.getString("id")
                val name = jsonData.getString("name")
                val mobile = jsonData.getString("mobile")
                val language = jsonData.getString("language")
                val image = jsonData.getString("image")
                val state_id = jsonData.getString("state_id")
                val city_id = jsonData.getString("city_id")
                val tehsil_id = jsonData.getString("tehsil_id")
                val village_id = jsonData.getString("village_id")
                val state_name = jsonData.getString("state")
                val city_name = jsonData.getString("city")
                val tehsil_name = jsonData.getString("tehsil")
                val village_name = jsonData.getString("village")

                val sharedProfile = this.getSharedPreferences("profile", 0)
                val editor: SharedPreferences.Editor =  sharedProfile.edit()
                editor.putString("id",id)
                editor.putString("name",name)
                editor.putString("mobile",mobile)
                editor.putString("language",language)
                editor.putString("image",image)
                editor.putString("sid", state_id)
                editor.putString("cid", city_id)
                editor.putString("tid", tehsil_id)
                editor.putString("vid", village_id)
                editor.putString("state_name", state_name)
                editor.putString("city_name", city_name)
                editor.putString("tehsil_name", tehsil_name)
                editor.putString("village_name", village_name)
                editor.apply()

                l_scroll_edit.visibility = View.GONE
                scroll_edit.visibility = View.GONE
                l_scroll_display.visibility = View.VISIBLE
                scroll_display.visibility = View.VISIBLE

                txt_name.setText(name)
                txt_mobile.setText(mobile)
                txt_state.setText(state_name)
                txt_city.setText(city_name)
                txt_tehsil.setText(tehsil_name)
                txt_village.setText(village_name)
                if (image.equals("null")){
                    Glide.with(this)
                        .load(R.mipmap.user_icon)
                        .into(img_profile)

                    Glide.with(this)
                        .load(R.mipmap.user_icon)
                        .into(img_profile_edit)
                }else{
                    Glide.with(this)
                        .load(image)
                        .error(R.mipmap.user_icon)
                        .into(img_profile)

                    Glide.with(this)
                        .load(image)
                        .error(R.mipmap.user_icon)
                        .into(img_profile_edit)
                }

                if (!isNetworkAvailable()) {
                    Toast.makeText(applicationContext, resources.getString(R.string.nointernet), Toast.LENGTH_LONG).show()
                } else {
                    Statedata({
                        hud.dismiss()
                        if (it == null) {
                            Toast.makeText(applicationContext, resources.getString(R.string.connectionerror), Toast.LENGTH_LONG)
                                .show()
                            return@Statedata
                        } else {
                            stateResponse(it)
                        }

                    }).execute("GET", AppConfig.STATE+str_state_id, "")
                }
            }
        }
        catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun View.showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        this.requestFocus()
        imm.showSoftInput(this, 0)
    }

}
