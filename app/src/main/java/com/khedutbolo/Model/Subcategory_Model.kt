package com.khedutbolo.Model

data class Subcategory_Model(val id: String, val name: String, val image: String)