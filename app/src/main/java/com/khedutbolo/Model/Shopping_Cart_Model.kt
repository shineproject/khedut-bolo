package com.khedutbolo.Model

data class Shopping_Cart_Model(val id: String, val name: String, val price: String, val quantity: String, val product_size: String,
                               val attribute_id: String, val sale_price: String, val total_price: String,
                               val cover: String, val dealer_name: String, val gst: String, val gst_price: String)