package com.khedutbolo.Model

data class Company_Detail_Model(val id: String, val name: String, val price: String, val sale_price: String, val company_name: String
                                , val category_name: String, val cover: String,
                                val s: String)