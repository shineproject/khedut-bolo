package com.khedutbolo.Model

data class Trending_Product_Model(val id: String, val name: String, val image: String, val price: String, val sale_price: String, val company_name: String)
