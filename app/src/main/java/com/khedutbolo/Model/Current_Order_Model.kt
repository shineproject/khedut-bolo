package com.khedutbolo.Model

data class Current_Order_Model(val order_id: String, val name: String, val price: String, val qty: String,
                               val sale_price: String, val date: String, val image: String, val value: String, val key: String,
                               val shipping_price: String, val dealer: String, val gst: String, val total: String)