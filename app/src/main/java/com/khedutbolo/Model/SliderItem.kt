package com.khedutbolo.Model

data class SliderItem(val id: String, val image: String, val offer_type: String, val desc: String)