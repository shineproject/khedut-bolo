package com.khedutbolo.Model

data class Similar_Product_Model(val id: String, val name: String, val price: String, val company_id: String,
                                 val product_dealer: String, val cover: String, val sale_price: String, val technical_name: String)