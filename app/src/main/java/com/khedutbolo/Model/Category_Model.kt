package com.khedutbolo.Model

data class Category_Model( val id: String,
                           val name: String,
                           val price: String,
                           val cover: String,
                           val additional_info: String,
                           val dealer_name: String,
                           val mobile: String,
                           val category_name: String,
                           val s: String,
                           val sale_price: String,
                            val technical_name: String)