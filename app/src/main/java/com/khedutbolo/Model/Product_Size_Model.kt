package com.khedutbolo.Model

data class Product_Size_Model(val key: String, val value: String, val quantity: String, val price: String
                              , val sale_price: String, val available_quantity: String, val id: String, val user_quantity: String)