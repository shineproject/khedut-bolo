package com.khedutbolo.Model

data class Wallet_Model(val id: String, var customer_id: String, var order_id: String, var amount: String,
                        var points: String, var creditdebit: String, var created_at: String, var title: String)