package com.khedutbolo.Model

data class Offer_Model(val id: String, val name: String, val price: String, val sale_price: String, val img: String)