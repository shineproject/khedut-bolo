package com.khedutbolo.Model

data class Sub_Technical_Model( val id: String,
                                val name: String,
                                val technical_name: String,
                                val price: String,
                                val sale_price: String,
                                val cover: String,
                                val additional_info: String
                                ,
                                val dealer_name: String,
                                val mobile: String,
                                val category_name: String,
                                val s: String,
                                val technical_id: String)