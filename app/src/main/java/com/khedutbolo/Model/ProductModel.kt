package com.khedutbolo.Model

data class ProductModel(val id: String, val name: String, val image: String, val price: String, val sale_price: String, val company_name: String)