package com.khedutbolo

import com.khedutbolo.Adapter.Crop_Adapter
import com.khedutbolo.Adapter.Crop_Solution_Adapter
import com.khedutbolo.Model.Crop_Model
import com.khedutbolo.Model.Crop_Solution_Model
import com.khedutbolo.RecyclerView_View.RecyclerItemClickListenr
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Select_Crop_Solution_Activity : AppCompatActivity() {

    lateinit var rv_crop_solution : RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select__crop__solution_)

        val actionbar = supportActionBar
        actionbar!!.title = resources.getString(R.string.selectcropandsol)
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)

        rv_crop_solution = findViewById(R.id.rv_crop_solution) as RecyclerView

        rv_crop_solution.layoutManager = GridLayoutManager(this, 2)

        val crop_solution = ArrayList<Crop_Solution_Model>()

        //adding some dummy data to the list
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))
        crop_solution.add(Crop_Solution_Model(R.mipmap.test_product, "Name"))


        val adapter_crop_solution = Crop_Solution_Adapter(crop_solution)
        rv_crop_solution.adapter = adapter_crop_solution

        rv_crop_solution.addOnItemTouchListener(
            RecyclerItemClickListenr(
                this,
                rv_crop_solution,
                object : RecyclerItemClickListenr.OnItemClickListener {

                    override fun onItemClick(view: View, position: Int) {
                        //do your work here..
                        startActivity(Intent(applicationContext, Select_Solution_Activity::class.java))
                        overridePendingTransition(0, 0)
                    }


                })
        )
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
