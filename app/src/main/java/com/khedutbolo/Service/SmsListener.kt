package com.khedutbolo.Service

interface SmsListener {
    fun messageReceived(messageText: String)
}